//
// eco32test.v -- test bench for ECO32
//


`include "src/eco32/eco32.v"
`include "src/clk_rst/clk_rst.v"
`include "src/cpu/cpu.v"
`include "src/cpu/cpu_bus.v"
`include "src/cpu/cpu_core.v"
`include "src/ram/ram.v"
`include "src/rom/rom.v"
`include "src/tmr/tmr.v"
`include "src/dsp/dsp.v"
`include "src/kbd/kbd.v"
`include "src/ser/ser.v"


`timescale 1ns/1ns


module eco32test(clk_in, rst_in_n);
    input clk_in;		// clock, input, 50 MHz
    input rst_in_n;		// reset, input, active low

  // create an instance of ECO32
  eco32 eco32_1(
    .clk_in(clk_in),
    .rst_in_n(rst_in_n)
  );

endmodule
