`include "../../src/mmu.v"
`include "../../src/instr_cache.v"
`include "../../src/data_cache.v"
`include "../../src/tlb.v"
`include "../../src/ramctrl.v" 
`include "../../src/ram.v" 

`timescale 1ns/10ps
`default_nettype none


module test;

	reg clk = 0;
	reg rst;

    //mmu-cpu
    reg [2:0]   cpu_data_size;  
    reg         mmu_data_we;   
    wire         mmu_data_hit;  
    reg [31:0]  mmu_data_din;  
    wire [31:0]  cpu_data_din;  

    wire         mmu_instr_hit; 
    wire [31:0]  cpu_instr_din; 

    reg [2:0]   mmu_fnc;       
    reg [31:0]  mmu_instr_virt;
    reg [31:0]  mmu_data_virt;

    wire         mmu_data_tlb_kmissed;
    wire         mmu_data_tlb_invalid;  
    wire         mmu_data_tlb_umissed; 
    wire         mmu_data_tlb_wrtprot; 
    wire         mmu_data_tlb_badaddr; 

    wire         mmu_instr_tlb_kmissed;
    wire         mmu_instr_tlb_umissed;
    wire         mmu_instr_tlb_invalid;
    wire         mmu_instr_tlb_wrtprot;
    wire         mmu_instr_tlb_badaddr;


    //sregs-mmu
    reg [31:0]  sregs_tlb_index; 
    wire [31:0]  sregs_tlb_index_new;
    reg [31:0]  sregs_tlb_entry_hi; 
    wire [31:0]  sregs_tlb_entry_hi_new;
    reg [31:0]  sregs_tlb_entry_lo;    
    wire [31:0]  sregs_tlb_entry_lo_new;

	// memctrl-communication
	wire [25:0] mem_data_addr; //memctrl-mmu
	wire [63:0] mem_data_dout; //memctrl-mmu
	wire [63:0] mem_data_din; //memctrl-mmu
	wire        mem_data_stb; //memctrl-mmu
	wire        mem_data_we; //memctrl-mmu
	wire        mem_data_ack; //memctrl-mmu
	wire		mem_data_timeout;

	wire        mem_instr_stb; //memctrl-mmu
	wire [63:0] mem_instr_dout; //memctrl-mmu
	wire [25:0] mem_instr_addr; //memctrl-mmu
	wire        mem_instr_ack; //memctrl-mmu
	wire 		mem_instr_timeout;

	// hold / flush - wires
    reg         cpu_hold_tf;
    reg         cpu_flush_tf;
    reg         cpu_hold_df;
    reg         cpu_flush_df;
    reg         cpu_hold_data_tf;
    reg         cpu_flush_data_tf;
    reg         cpu_flush_data_df;


	// io-controller
	wire io_stb;
	wire io_we;
	wire [31:0] io_addr;
	reg [31:0] io_data_in;
	wire [31:0] io_data_out;
	reg io_ack;
//	wire io_timeout;


	/* Make a regular pulsing clock. */
	always #1 clk = !clk;


	initial begin
		$dumpfile("dump.vcd");
		$dumpvars(0,test);

		#1 $readmemh("ram.dat", ramctrl_1.ram_1.mem);

		#10 rst <= 1;
		// init
		cpu_hold_tf <= 0;
		cpu_flush_tf <= 0;
		cpu_hold_df <= 0;
		cpu_flush_df <= 0;
		cpu_hold_data_tf <= 0;
		cpu_flush_data_tf <= 0;
    	cpu_flush_data_df <= 0;
	
		io_ack <= 0;


		#2500 rst <= 0;

		/* fill TLB with test adresses with write-index */
		#10 sregs_tlb_entry_hi <= 32'h00111048; sregs_tlb_entry_lo <= 32'h00000043; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd0;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h00222222; sregs_tlb_entry_lo <= 32'h0FAFE222; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd1;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h00333333; sregs_tlb_entry_lo <= 32'h0FAFD333; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd2;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h00444444; sregs_tlb_entry_lo <= 32'h0FAFC444; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd3;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h00555555; sregs_tlb_entry_lo <= 32'h0FAFB555; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd4;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h00666666; sregs_tlb_entry_lo <= 32'h0FAFA666; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd5;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h00777777; sregs_tlb_entry_lo <= 32'h0FAF9777; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd6;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h00888888; sregs_tlb_entry_lo <= 32'h0FAF8888; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd7;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h00999999; sregs_tlb_entry_lo <= 32'h0FAF7999; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd8;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h00AAA8FC; sregs_tlb_entry_lo <= 32'h047118FC; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd9;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h00BBB8FC; sregs_tlb_entry_lo <= 32'h008158FC; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd10;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h00CCCCCC; sregs_tlb_entry_lo <= 32'h0FAF4CCC; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd11;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h00DDDDDD; sregs_tlb_entry_lo <= 32'h0FAF3DDD; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd12;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h00EEEEEE; sregs_tlb_entry_lo <= 32'h0FAF2EEE; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd13;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h0CABDAED; sregs_tlb_entry_lo <= 32'h02244AED; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd14;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h00FFFFFF; sregs_tlb_entry_lo <= 32'h0FAF1FFF; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd15;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h01BBBBBB; sregs_tlb_entry_lo <= 32'h0FAF0BBB; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd16;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h02BBBBBB; sregs_tlb_entry_lo <= 32'h0FABFBBB; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd17;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h03BBBBBB; sregs_tlb_entry_lo <= 32'h0FABEBBB; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd18;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h04BBBBBB; sregs_tlb_entry_lo <= 32'h0FABDBBB; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd19;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h05BBBBBB; sregs_tlb_entry_lo <= 32'h0FABCBBB; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd20;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h06BBBBBB; sregs_tlb_entry_lo <= 32'h0FABABBB; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd21;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h07BBBBBB; sregs_tlb_entry_lo <= 32'h0FAB9BBB; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd22;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h08BBBBBB; sregs_tlb_entry_lo <= 32'h0FAB8BBB; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd23;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h09BBBBBB; sregs_tlb_entry_lo <= 32'h0FACFBBB; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd24;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h0ABBBBBB; sregs_tlb_entry_lo <= 32'h0FACCBBB; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd25;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h0BBBBBBB; sregs_tlb_entry_lo <= 32'h0FACEBBB; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd26;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h0CBBBBBB; sregs_tlb_entry_lo <= 32'h0FADEBBB; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd27;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h0DBBBBBB; sregs_tlb_entry_lo <= 32'h0FADFBBB; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd28;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h0EBBBBBB; sregs_tlb_entry_lo <= 32'h0FADABBB; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd29;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h0FBBBBBB; sregs_tlb_entry_lo <= 32'h0FAD2BBB; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd30;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;
		#10 sregs_tlb_entry_hi <= 32'h0FBABABA; sregs_tlb_entry_lo <= 32'h0FAD5ABA; mmu_fnc <= 3'b101; sregs_tlb_index <= 5'd31;
		#4 mmu_fnc <= 3'b001; mmu_data_virt <= 32'h0;

/*******************************************    start testing    *******************************************/


		/* test address errors */

	
		/* test direct-mapped space */
		#10 mmu_instr_virt <= 32'hC000_006C;
		#14 mmu_instr_virt <= 32'hC000_0010;
		#2  mmu_instr_virt <= 32'hC000_0014;

		/* test page-mapped space */
		#14 mmu_instr_virt <= 32'h0011_1048;

		/* test ROM addresses */
		

		/* test I/O addresses */
		#100 mmu_data_virt <= 32'hE011_1048;
		#6 io_ack <= 1;

		#10000 $finish;
	end



    mmu mmu_1(
        .clk(clk), .rst(rst),
        .fnc(mmu_fnc), 

        .hold_data_stage_1(cpu_hold_data_tf),
    	.flush_data_stage_1(cpu_flush_data_tf),
    	.enable_data_stage_2(cpu_flush_data_df),

        .data_size(cpu_data_size), 
        .data_we(mmu_data_we),     .data_hit(mmu_data_hit), 
        .data_din(mmu_data_din),   .data_dout(cpu_data_din),
        .data_virt(mmu_data_virt),

        .instr_hit(mmu_instr_hit), 
        .instr_dout(cpu_instr_din),    
        .instr_virt(mmu_instr_virt),

        .tlb_index(sregs_tlb_index),       .tlb_index_new(sregs_tlb_index_new),	      
        .tlb_entry_hi(sregs_tlb_entry_hi), .tlb_entry_hi_new(sregs_tlb_entry_hi_new),
        .tlb_entry_lo(sregs_tlb_entry_lo), .tlb_entry_lo_new(sregs_tlb_entry_lo_new),

        .instr_tlb_kmissed(mmu_instr_tlb_kmissed), .data_tlb_kmissed(mmu_data_tlb_kmissed),
        .instr_tlb_umissed(mmu_instr_tlb_umissed), .data_tlb_umissed(mmu_data_tlb_umissed),
        .instr_tlb_invalid(mmu_instr_tlb_invalid), .data_tlb_invalid(mmu_data_tlb_invalid),
        .instr_tlb_wrtprot(mmu_instr_tlb_wrtprot), .data_tlb_wrtprot(mmu_data_tlb_wrtprot),
        .instr_tlb_badaddr(mmu_instr_tlb_badaddr), .data_tlb_badaddr(mmu_data_tlb_badaddr),

        .data_bus_stb(mem_data_stb),   .data_bus_we(mem_data_we),   
        .data_bus_addr(mem_data_addr), .data_bus_din(mem_data_dout),   
        .data_bus_dout(mem_data_din),  .data_bus_ack(mem_data_ack), .data_bus_timeout(mem_data_timeout),

        .instr_bus_stb(mem_instr_stb),   .instr_bus_din(mem_instr_dout), 
        .instr_bus_addr(mem_instr_addr), .instr_bus_ack(mem_instr_ack), .instr_bus_timeout(mem_instr_timeout),
		// hold / flush - wires
        .hold_instr_stage_1(cpu_hold_tf),
        .flush_instr_stage_1(cpu_flush_tf),
        .hold_instr_stage_2(cpu_hold_df),
        .flush_instr_stage_2(cpu_flush_df),
		// io-controller
		.io_stb(io_stb), .io_we(io_we), .io_addr(io_addr), .io_data_in(io_data_out),
		.io_data_out(io_data_in), .io_ack(io_ack) //	, .io_timeout()
		
    );

	ramctrl ramctrl_1(
		.clk(clk),
		.rst(rst),
		.inst_stb(mem_instr_stb),
		.inst_addr(mem_instr_addr),
		.inst_dout(mem_instr_dout),
		.inst_ack(mem_instr_ack),
		.inst_timeout(mem_instr_timeout),

		.data_stb(mem_data_stb),
		.data_we(mem_data_we),
		.data_addr(mem_data_addr),
		.data_din(mem_data_din),
		.data_dout(mem_data_dout),
		.data_ack(mem_data_ack),
		.data_timeout(mem_data_timeout)
	);

endmodule
