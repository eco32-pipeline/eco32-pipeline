
`include "../../src/data_cache.v"
`include "../../src/ramctrl.v" 
`include "../../src/ram.v"

`timescale 1ns/10ps
`default_nettype none


module test;

	reg clk = 0;
	reg rst;
	reg en;
	reg [2:0] size;
	reg [31:12] tag_in;
	reg [11:0] offset;
	reg we;
	wire hit;
	reg [31:0] din;
	wire [31:0] dout;

	// memctrl-communication
	wire [25:0] mem_data_addr; //memctrl-mmu
	wire [63:0] mem_data_dout; //memctrl-mmu
	wire [63:0] mem_data_din; //memctrl-mmu
	wire        mem_data_stb; //memctrl-mmu
	wire        mem_data_we; //memctrl-mmu
	wire        mem_data_ack; //memctrl-mmu
	wire		mem_data_timeout;

	wire        mem_instr_stb; //memctrl-mmu
	wire [63:0] mem_instr_dout; //memctrl-mmu
	wire [25:0] mem_instr_addr; //memctrl-mmu
	wire        mem_instr_ack; //memctrl-mmu
	wire 		mem_instr_timeout;


	/* Make a regular pulsing clock. */
	always #1 clk = !clk;


	initial begin
		$dumpfile("dump.vcd");
		$dumpvars(0,test);

		#1 $readmemh("ram.dat", ramctrl_1.ram_1.mem); en <= 0;

		#10 rst <= 1;		
		#2500 rst <= 0; 

		#10 en <= 1; size <= 3'b010; we <= 0;
		/*1st miss reading 2 words*/
		offset <= { 9'b0_0000_0000, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0000, 3'b100 }; #2 tag_in <= 20'h0;
				
		#12
		/*2nd miss reading 4 halfwords*/
		size <= 3'b101;
		offset <= { 9'b0_0000_0001, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0001, 3'b010 }; #2 tag_in <= 20'h0;
		#12
		offset <= { 9'b0_0000_0001, 3'b100 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0001, 3'b110 }; #2 tag_in <= 20'h0;
		/*3rd miss reading 8 bytes*/
		size <= 3'b000;
		offset <= { 9'b0_0000_0010, 3'b000 }; #2 tag_in <= 20'h0; size <= 3'b100;
		offset <= { 9'b0_0000_0010, 3'b001 }; #2 tag_in <= 20'h0;
		#12
		size <= 3'b000;
		offset <= { 9'b0_0000_0010, 3'b010 }; #2 tag_in <= 20'h0; size <= 3'b100;
		offset <= { 9'b0_0000_0010, 3'b011 }; #2 tag_in <= 20'h0; size <= 3'b000;
		offset <= { 9'b0_0000_0010, 3'b100 }; #2 tag_in <= 20'h0; size <= 3'b100;
		offset <= { 9'b0_0000_0010, 3'b101 }; #2 tag_in <= 20'h0; size <= 3'b000;
		offset <= { 9'b0_0000_0010, 3'b110 }; #2 tag_in <= 20'h0; size <= 3'b100;
		offset <= { 9'b0_0000_0010, 3'b111 }; #2 tag_in <= 20'h0;
		
		/*4th miss*/
		size <= 3'b010;
		offset <= { 9'b0_0000_0011, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0011, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_0100, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0100, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_0101, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0101, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_0110, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0110, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_0111, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0111, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_1000, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1000, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_1001, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1001, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_1010, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1010, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_1011, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1011, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_1100, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1100, 3'b100 }; #2 tag_in <= 20'h0;



		#100
		offset <= { 9'b0_0000_0000, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0000, 3'b100 }; #2 tag_in <= 20'h0;
			
		offset <= { 9'b0_0000_0001, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0001, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_0010, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0010, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_0011, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0011, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_0100, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0100, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_0101, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0101, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_0110, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0110, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_0111, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0111, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_1000, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1000, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_1001, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1001, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_1010, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1010, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_1011, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1011, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_1100, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1100, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_1101, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1101, 3'b100 }; #2 tag_in <= 20'h0;



#100
		offset <= { 9'b0_0000_0000, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0000, 3'b100 }; #2 tag_in <= 20'h01;
				
		#12
		offset <= { 9'b0_0000_0001, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0001, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_0010, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0010, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_0011, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0011, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_0100, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0100, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_0101, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0101, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_0110, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0110, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_0111, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0111, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_1000, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1000, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_1001, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1001, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_1010, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1010, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_1011, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1011, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_1100, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1100, 3'b100 }; #2 tag_in <= 20'h01;



		#100
		offset <= { 9'b0_0000_0000, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0000, 3'b100 }; #2 tag_in <= 20'h01;
	
		offset <= { 9'b0_0000_0001, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0001, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_0010, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0010, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_0011, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0011, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_0100, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0100, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_0101, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0101, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_0110, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0110, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_0111, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0111, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_1000, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1000, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_1001, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1001, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_1010, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1010, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_1011, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1011, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_1100, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1100, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_1101, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1101, 3'b100 }; #2 tag_in <= 20'h01;



		#10000 $finish;
	end


	dcache dcache_1(
		.clk(clk),
		.rst(rst),
		.en(en),
		.size(size),
		.tag_in(tag_in),
		.offset(offset),
		.we(we),
		.hit(hit),
		.din(din),
		.dout(dout),
	// memctrl-communication
		.bus_stb(mem_data_stb),
		.bus_we(mem_data_we),
		.bus_addr(mem_data_addr),
		.bus_din(mem_data_dout),
		.bus_dout(mem_data_din),
		.bus_ack(mem_data_ack),
		.bus_timeout(mem_data_timeout)
	);


	ramctrl ramctrl_1(
		.clk(clk),
		.rst(rst),
		/*icache wires*/
		.inst_stb(mem_instr_stb),
		.inst_addr(mem_instr_addr),
		.inst_dout(mem_instr_dout),
		.inst_ack(mem_instr_ack),
		.inst_timeout(mem_instr_timeout),
		/*dcache wires*/
		.data_stb(mem_data_stb),
		.data_we(mem_data_we),
		.data_addr(mem_data_addr),
		.data_din(mem_data_din),
		.data_dout(mem_data_dout),
		.data_ack(mem_data_ack),
		.data_timeout(mem_data_timeout)
	);

endmodule
