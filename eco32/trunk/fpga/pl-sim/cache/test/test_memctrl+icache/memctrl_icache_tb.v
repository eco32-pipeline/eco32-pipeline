
`include "../../src/instr_cache.v"
`include "../../src/ramctrl.v" 
`include "../../src/ram.v" 



`timescale 1ns/10ps
`default_nettype none


module test;

	reg clk = 0;
	reg rst;
	reg en;
	reg [31:12] tag_in;
	reg [11:0] offset;
	wire hit;
	wire [31:0] dout;

	// memctrl-communication
	wire [25:0] mem_data_addr; //memctrl-mmu
	wire [63:0] mem_data_dout; //memctrl-mmu
	wire [63:0] mem_data_din; //memctrl-mmu
	wire        mem_data_stb; //memctrl-mmu
	wire        mem_data_we; //memctrl-mmu
	wire        mem_data_ack; //memctrl-mmu
	wire		mem_data_timeout;

	wire        mem_instr_stb; //memctrl-mmu
	wire [63:0] mem_instr_dout; //memctrl-mmu
	wire [25:0] mem_instr_addr; //memctrl-mmu
	wire        mem_instr_ack; //memctrl-mmu
	wire 		mem_instr_timeout;


	reg hold_tf = 1'b0;
	reg flush_tf = 1'b0;
	reg hold_df = 1'b0;
	reg flush_df = 1'b0;

	/* Make a regular pulsing clock. */
	always #1 clk = !clk;


	initial begin
		$dumpfile("dump.vcd");
		$dumpvars(0,test);

		#1 $readmemh("ram.dat", ramctrl_1.ram_1.mem); en <= 0;

		#10 rst <= 1;		
		#2500 rst <= 0;

		#10 en <= 1;

		offset <= { 9'b0_0000_0000, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0000, 3'b100 }; #2 tag_in <= 20'h0;
				
		#12
		offset <= { 9'b0_0000_0001, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0001, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_0010, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0010, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_0011, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0011, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_0100, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0100, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_0101, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0101, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_0110, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0110, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_0111, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0111, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_1000, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1000, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_1001, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1001, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_1010, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1010, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_1011, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1011, 3'b100 }; #2 tag_in <= 20'h0;

		#12
		offset <= { 9'b0_0000_1100, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1100, 3'b100 }; #2 tag_in <= 20'h0;



		#100
		offset <= { 9'b0_0000_0000, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0000, 3'b100 }; #2 tag_in <= 20'h0;
			
		offset <= { 9'b0_0000_0001, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0001, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_0010, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0010, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_0011, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0011, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_0100, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0100, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_0101, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0101, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_0110, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0110, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_0111, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_0111, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_1000, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1000, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_1001, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1001, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_1010, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1010, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_1011, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1011, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_1100, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1100, 3'b100 }; #2 tag_in <= 20'h0;

		offset <= { 9'b0_0000_1101, 3'b000 }; #2 tag_in <= 20'h0;
		offset <= { 9'b0_0000_1101, 3'b100 }; #2 tag_in <= 20'h0;



#100
		offset <= { 9'b0_0000_0000, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0000, 3'b100 }; #2 tag_in <= 20'h01;
				
		#12
		offset <= { 9'b0_0000_0001, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0001, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_0010, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0010, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_0011, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0011, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_0100, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0100, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_0101, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0101, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_0110, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0110, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_0111, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0111, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_1000, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1000, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_1001, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1001, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_1010, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1010, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_1011, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1011, 3'b100 }; #2 tag_in <= 20'h01;

		#12
		offset <= { 9'b0_0000_1100, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1100, 3'b100 }; #2 tag_in <= 20'h01;



		#100
		offset <= { 9'b0_0000_0000, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0000, 3'b100 }; #2 tag_in <= 20'h01;
	
		offset <= { 9'b0_0000_0001, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0001, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_0010, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0010, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_0011, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0011, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_0100, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0100, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_0101, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0101, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_0110, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0110, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_0111, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_0111, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_1000, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1000, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_1001, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1001, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_1010, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1010, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_1011, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1011, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_1100, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1100, 3'b100 }; #2 tag_in <= 20'h01;

		offset <= { 9'b0_0000_1101, 3'b000 }; #2 tag_in <= 20'h01;
		offset <= { 9'b0_0000_1101, 3'b100 }; #2 tag_in <= 20'h01;


/* ---------------- NEU -------------------- */
		#100		
		offset <= { 9'b0_0000_0000, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_0000, 3'b100 }; #2 tag_in <= 20'h02;
				
		#12
		offset <= { 9'b0_0000_0001, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_0001, 3'b100 }; #2 tag_in <= 20'h02;

		#12
		offset <= { 9'b0_0000_0010, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_0010, 3'b100 }; #2 tag_in <= 20'h02;

		#12
		offset <= { 9'b0_0000_0011, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_0011, 3'b100 }; #2 tag_in <= 20'h02;

		#12
		offset <= { 9'b0_0000_0100, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_0100, 3'b100 }; #2 tag_in <= 20'h02;

		#12
		offset <= { 9'b0_0000_0101, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_0101, 3'b100 }; #2 tag_in <= 20'h02;

		#12
		offset <= { 9'b0_0000_0110, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_0110, 3'b100 }; #2 tag_in <= 20'h02;

		#12
		offset <= { 9'b0_0000_0111, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_0111, 3'b100 }; #2 tag_in <= 20'h02;

		#12
		offset <= { 9'b0_0000_1000, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_1000, 3'b100 }; #2 tag_in <= 20'h02;

		#12
		offset <= { 9'b0_0000_1001, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_1001, 3'b100 }; #2 tag_in <= 20'h02;

		#12
		offset <= { 9'b0_0000_1010, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_1010, 3'b100 }; #2 tag_in <= 20'h02;

		#12
		offset <= { 9'b0_0000_1011, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_1011, 3'b100 }; #2 tag_in <= 20'h02;

		#12
		offset <= { 9'b0_0000_1100, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_1100, 3'b100 }; #2 tag_in <= 20'h02;



		#100
		offset <= { 9'b0_0000_0000, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_0000, 3'b100 }; #2 tag_in <= 20'h02;
			
		offset <= { 9'b0_0000_0001, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_0001, 3'b100 }; #2 tag_in <= 20'h02;

		offset <= { 9'b0_0000_0010, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_0010, 3'b100 }; #2 tag_in <= 20'h02;

		offset <= { 9'b0_0000_0011, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_0011, 3'b100 }; #2 tag_in <= 20'h02;

		offset <= { 9'b0_0000_0100, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_0100, 3'b100 }; #2 tag_in <= 20'h02;

		offset <= { 9'b0_0000_0101, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_0101, 3'b100 }; #2 tag_in <= 20'h02;

		offset <= { 9'b0_0000_0110, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_0110, 3'b100 }; #2 tag_in <= 20'h02;

		offset <= { 9'b0_0000_0111, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_0111, 3'b100 }; #2 tag_in <= 20'h02;

		offset <= { 9'b0_0000_1000, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_1000, 3'b100 }; #2 tag_in <= 20'h02;

		offset <= { 9'b0_0000_1001, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_1001, 3'b100 }; #2 tag_in <= 20'h02;

		offset <= { 9'b0_0000_1010, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_1010, 3'b100 }; #2 tag_in <= 20'h02;

		offset <= { 9'b0_0000_1011, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_1011, 3'b100 }; #2 tag_in <= 20'h02;

		offset <= { 9'b0_0000_1100, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_1100, 3'b100 }; #2 tag_in <= 20'h02;

		offset <= { 9'b0_0000_1101, 3'b000 }; #2 tag_in <= 20'h02;
		offset <= { 9'b0_0000_1101, 3'b100 }; #2 tag_in <= 20'h02;





		#10000 $finish;
	end


	icache icache_1(
		.clk(clk),
		.rst(rst),
		.tag_in(tag_in),
		.offset(offset),
		.hit(hit),
		.dout(dout),
	// hold / flush - wires
		.hold_tf(hold_tf),
		.flush_tf(flush_tf),
		.hold_df(hold_df),
		.flush_df(flush_df),

	// memctrl-communication
		.bus_stb(mem_instr_stb),
		.bus_addr(mem_instr_addr),
		.bus_din(mem_instr_dout),
		.bus_ack(mem_instr_ack),
		.bus_timeout(mem_instr_timeout)
	);


	ramctrl ramctrl_1(
		.clk(clk),
		.rst(rst),
		.inst_stb(mem_instr_stb),
		.inst_addr(mem_instr_addr),
		.inst_dout(mem_instr_dout),
		.inst_ack(mem_instr_ack),
		.inst_timeout(mem_instr_timeout),

		.data_stb(mem_data_stb),
		.data_we(mem_data_we),
		.data_addr(mem_data_addr),
		.data_din(mem_data_din),
		.data_dout(mem_data_dout),
		.data_ack(mem_data_ack),
		.data_timeout(mem_data_timeout)
	);

endmodule
