`timescale 1ns/10ps

//--------------------------------------------------------------
// mmu -- the memory management unit
//--------------------------------------------------------------

module mmu(clk, rst, 
			data_size, data_we, data_hit, data_din, data_dout,
			instr_hit, instr_dout, 

			fnc,
			instr_virt, data_virt,
			tlb_index, tlb_index_new,
			tlb_entry_hi, tlb_entry_hi_new,
			tlb_entry_lo, tlb_entry_lo_new,
			instr_tlb_kmissed, data_tlb_kmissed,
			instr_tlb_umissed, data_tlb_umissed,
			instr_tlb_invalid, data_tlb_invalid,
			instr_tlb_wrtprot, data_tlb_wrtprot,
			instr_tlb_badaddr, data_tlb_badaddr,
			instr_bus_timeout_exc, data_bus_timeout_exc,
			/* hold / flush wires instr cache*/
			hold_instr_stage_1, flush_instr_stage_1,
			hold_instr_stage_2, flush_instr_stage_2,
			/* hold / flush wires data cache*/
			hold_data_stage_1, flush_data_stage_1,
			enable_data_stage_2,
			/* I/O - Controller */
			io_stb, io_we, io_addr, 
			io_data_in, io_data_out, io_ack, io_timeout,
			
			data_bus_stb, data_bus_we, data_bus_addr, data_bus_din, data_bus_dout, data_bus_ack, data_bus_timeout,
			instr_bus_stb, instr_bus_din, instr_bus_addr, instr_bus_ack, instr_bus_timeout
);

	/*data_cache bus wires*/
	output data_bus_stb;
	output data_bus_we;
	output [25:0]data_bus_addr;
	output [63:0]data_bus_dout;
	input data_bus_ack;
	input [63:0]data_bus_din;
	input data_bus_timeout;

	/*instr_cache bus wires*/
	output instr_bus_stb;
	input [63:0]instr_bus_din;
	output [25:0]instr_bus_addr;
	input instr_bus_ack;
	input instr_bus_timeout;

    input clk;
    input rst;

	// data_cache
	input [2:0] data_size;
	input hold_data_stage_1;
	input flush_data_stage_1;
	input enable_data_stage_2;
	input data_we;
	output data_hit;
	input [31:0] data_din;
	output [31:0] data_dout;
	output instr_hit;
	output [31:0] instr_dout;

	
	// MMU
	wire instr_map;
    input [2:0] fnc;
	// fnc = 000    no operation, hold output
	//       001    map virt to phys address
	//       010    tbs
	//       011    tbwr
	//       100    tbri
	//       101    tbwi
	//       110    undefined
	//       111    undefined
	input [31:0] instr_virt;
	input [31:0] data_virt;
	wire [31:0] instr_phys;		// physical tag send to cache
	wire [31:0] data_phys;		// physical tag send to cache

    input [31:0] tlb_index;
    output [31:0] tlb_index_new;
    input [31:0] tlb_entry_hi;
    output [31:0] tlb_entry_hi_new;
    input [31:0] tlb_entry_lo;
    output [31:0] tlb_entry_lo_new;

	/* Errors */
    output instr_tlb_kmissed;
    output instr_tlb_umissed;
    output instr_tlb_invalid;
    output instr_tlb_wrtprot;
    output instr_tlb_badaddr;
    output instr_bus_timeout_exc;

    output data_tlb_kmissed;
    output data_tlb_umissed;
    output data_tlb_invalid;
    output data_tlb_wrtprot;
    output data_tlb_badaddr;
    output data_bus_timeout_exc;

	/* hold / flush */
    input hold_instr_stage_1;
    input hold_instr_stage_2;
    input flush_instr_stage_1;
    input flush_instr_stage_2;

	/* I/O - Controller */
	output reg io_stb;
	output reg io_we;
	output reg [31:0] io_addr;
	output reg [31:0] io_data_in;
	input [31:0] io_data_out;
	input io_ack;
	input io_timeout;


	// intern
	reg [31:0] io_output;

	wire data_map;
	wire tbs;
	wire tbwr;
	wire tbri;
	wire tbwi;

	reg [19:0] instr_page;
	reg [11:0] instr_offset;
	reg [19:0] data_page;
	reg [11:0] data_offset;

	wire [19:0] data_tlb_page;
 	wire [19:0] instr_tlb_page;
	wire [4:0] tlb_found;
  
	wire instr_tlb_wbit;
	wire instr_tlb_vbit;
	wire data_tlb_wbit;
	wire data_tlb_vbit;

	wire [4:0] rw_index;
	wire [19:0] r_page;
	wire [19:0] r_frame;
	wire w_enable;
	wire [19:0] w_page;
	wire [19:0] w_frame;
  
	wire instr_direct;
	wire data_direct;
	reg instr_direct_delayed;
	reg data_direct_delayed;
	wire [17:0] instr_frame;  
	wire [17:0] data_frame;
  
	reg [4:0] random_index;
  
	reg instr_tlb_miss_delayed;
	reg data_tlb_miss_delayed;
  
	wire instr_tlb_enable;
	wire [19:0] instr_tlb_frame;
	wire instr_tlb_miss;
  
	wire data_tlb_enable;
	wire [19:0] data_tlb_frame;
	wire data_tlb_miss;

	wire d_fnc;
	
	reg iexc_startup;
	
	wire instr_cache_hit;
	wire data_cache_hit;
	wire [31:0] instr_cache_dout;
	wire [31:0] data_cache_dout;

	wire instr_flush_df;
	wire data_flush_df;
	reg old_instr_flush_tf;
	reg old_instr_flush_df;
	reg old_instr_hold_df;
	reg [31:0] last_dout;
	reg instr_io_indicator;
	reg data_io_indicator;
	wire instr_cache_flush_df;
	reg last_instr_hit;
	reg last_hit;
	reg old_data_flush_tf;

	assign instr_flush_df = flush_instr_stage_2 || old_instr_flush_tf;
	assign instr_cache_flush_df = instr_phys[29] || instr_flush_df;
	assign data_flush_df  = data_phys[29] || enable_data_stage_2;
	
	assign instr_dout = ( old_instr_flush_df || old_instr_hold_df || ~last_instr_hit ) ? last_dout : 
						  instr_io_indicator ? io_output : instr_cache_dout;

	assign data_dout = ;

	dcache d_cache(
			.clk(clk),
			.rst(rst),
			.hold_tf(hold_data_stage_1),
			.flush_tf(flush_data_stage_1),
			.flush_df(data_flush_df),
			.size(data_size),
			.tag_in(data_phys[31:12]),
			.offset(data_virt[11:0]),
			.we(data_we),
			.hit(data_cache_hit),
			.din(data_din),
			.dout(data_cache_dout),
			.bus_stb(data_bus_stb),
			.bus_we(data_bus_we),
			.bus_addr(data_bus_addr),
			.bus_din(data_bus_din),
			.bus_dout(data_bus_dout),
			.bus_ack(data_bus_ack),
			.bus_timeout(data_bus_timeout)
	);


	icache i_cache(
				.clk(clk),
				.rst(rst),
				// tag_fetch (stage 1)
				.hold_tf(hold_instr_stage_1),
				.flush_tf(flush_instr_stage_1),
				// data_fetch (stage 2)
				.hold_df(hold_instr_stage_2),
				.flush_df(instr_cache_flush_df),

				.dout(instr_cache_dout),
				.tag_in(instr_phys[31:12]),
				.offset(instr_virt[11:0]),
				.hit(instr_cache_hit),
				.bus_stb(instr_bus_stb),
				.bus_din(instr_bus_din),
				.bus_addr(instr_bus_addr),
				.bus_ack(instr_bus_ack),
				.bus_timeout(instr_bus_timeout)
	);


	assign d_fnc = ( hold_data_stage_1 | flush_data_stage_1 ) ? 3'b000 : fnc;
	
	assign instr_map = ~( hold_instr_stage_1 | flush_instr_stage_1 );
	// decode function - Instruction
	assign data_map = (d_fnc == 3'b001) ? 1 : 0;
	assign tbs      = (d_fnc == 3'b010) ? 1 : 0;
	assign tbwr     = (d_fnc == 3'b011) ? 1 : 0;
	assign tbri     = (d_fnc == 3'b100) ? 1 : 0;
	assign tbwi     = (d_fnc == 3'b101) ? 1 : 0;

	// latch virtual address
	always @(posedge clk) begin
		if(data_map == 1) begin
			data_page <= data_virt[31:12];
			data_offset <= data_virt[11:0];
		end

		if(instr_map == 1) begin
			instr_page <= instr_virt[31:12];
			instr_offset <= instr_virt[11:0];
		end
	end

	// create tlb instances
	assign data_tlb_page = (tbs == 1) ? tlb_entry_hi[31:12] : data_virt[31:12];
	assign instr_tlb_page = instr_virt[31:12];

	assign instr_tlb_enable = instr_map;
	assign data_tlb_enable = data_map;
  
	assign instr_tlb_wbit = instr_tlb_frame[1];
	assign instr_tlb_vbit = instr_tlb_frame[0];
	assign data_tlb_wbit = data_tlb_frame[1];
	assign data_tlb_vbit = data_tlb_frame[0];
  
	assign rw_index = (tbwr == 1) ? random_index : tlb_index[4:0];
	assign tlb_index_new = { data_tlb_miss, 26'b0, tlb_found };
	assign tlb_entry_hi_new = { ((tbri == 1) ? r_page : data_page),
                              tlb_entry_hi[11:0] };
	assign tlb_entry_lo_new = { tlb_entry_lo[31:30], r_frame[19:2],
                              tlb_entry_lo[11:2], r_frame[1:0] };
	assign w_enable = tbwr | tbwi;
	assign w_page = tlb_entry_hi[31:12];
	assign w_frame = { tlb_entry_lo[29:12], tlb_entry_lo[1:0] };

	tlb instr_tlb(
				.page_in(instr_tlb_page),
				.miss(instr_tlb_miss),
				.clk(clk),
				.enable(instr_tlb_enable),
				.frame_out(instr_tlb_frame),
				.rw_index(rw_index),
				.w_enable(w_enable),
				.w_page(w_page),
				.w_frame(w_frame)
	);

	tlb data_tlb(
				.page_in(data_tlb_page),
				.miss(data_tlb_miss),
				.found(tlb_found),
				.clk(clk),
				.enable(data_tlb_enable),
				.frame_out(data_tlb_frame),
				.rw_index(rw_index),
				.r_page(r_page),
				.r_frame(r_frame),
				.w_enable(w_enable),
				.w_page(w_page),
				.w_frame(w_frame)
	);


	// construct physical address - instr
	assign instr_direct = (instr_virt[31:30] == 2'b11) ? 1 : 0;
	assign instr_frame = (instr_direct_delayed == 1) ? instr_page[17:0] : instr_tlb_frame[19:2];
	assign instr_phys = { 2'b00, instr_frame, instr_offset };

	// construct physical address - data
	assign data_direct = (data_virt[31:30] == 2'b11) ? 1 : 0;
	assign data_frame = (data_direct_delayed == 1) ? data_page[17:0] : data_tlb_frame[19:2];
	assign data_phys = old_data_flush_tf ? 32'h0 : { 2'b00, data_frame, data_offset };

	assign instr_hit = ( instr_phys[29] && ~data_phys[29] && io_ack ) || instr_cache_hit || (last_hit && instr_flush_df);
	assign data_hit = ( data_phys[29] && io_ack ) ? 1'b1 : data_cache_hit;


	always @(posedge clk) begin

		if( rst == 1 ) begin
			io_output <= 32'h0;
			io_stb <= 1'b0;
		end else begin
			if( io_ack ) begin
				io_output <= io_data_out;
			end
			if( io_ack || io_timeout ) begin
				io_stb <= 0;
			end else begin
				if( (data_phys[29] == 1) && ~data_flush_df && (~last_hit || data_flush_df)) begin
					io_stb <= 1;
					io_we <= data_we;
					io_addr <= data_phys;
					io_data_in <= (data_we == 1) ? data_din : 32'hx;
				end
				else if( (instr_phys[29] == 1) && ~hold_instr_stage_2 && ~instr_flush_df && (~last_hit || instr_flush_df)) begin
					io_stb <= 1;
					io_we <= 0;
					io_addr <= instr_phys;
					io_data_in <= 32'hx;		// egal, weil io_we == 0;
				end
			end
		end
	end
	
	/*mux dout*/
	always @(posedge clk) begin
		if(hold_instr_stage_2 || instr_flush_df)begin
			if(io_ack)begin
				last_hit <= 1;
			end
		end else begin
			last_hit <= 0;
		end
	
		old_instr_flush_df <= instr_flush_df;
		old_instr_hold_df <= hold_instr_stage_2;
		instr_io_indicator <= instr_phys[29];
		data_io_indicator <= data_phys[29];
		last_instr_hit <= instr_hit;
		
		if ( ~hold_instr_stage_1 ) begin
			old_instr_flush_tf <= flush_instr_stage_1;
		end
		
		if( ~hold_instr_stage_2 )begin
			if( instr_flush_df | ~instr_hit) begin
				last_dout <= 32'h0;
			end else begin
				last_dout <= instr_dout;
			end
		end
	end

	// generate "random" index
	always @(posedge clk) begin
		if (rst == 1) begin
			// the index register is counting down
			// so we must start at topmost index
			random_index <= 5'd31;
		end else begin
			// decrement index register "randomly"
			// (whenever there is a mapping operation)
			// skip "fixed" entries (0..3)
			if (data_map == 1) begin
				if (random_index == 5'd4) begin
					random_index <= 5'd31;
				end else begin
					random_index <= random_index - 1;
				end
			end
    	end
	end


	// generate TLB exceptions
	always @(posedge clk) begin
		iexc_startup <= rst;
		old_data_flush_tf <= flush_data_stage_1;
		if (instr_map == 1) begin
			instr_direct_delayed <= instr_direct;
			instr_tlb_miss_delayed <= instr_tlb_miss;
		end
		if (data_map == 1) begin
			data_direct_delayed <= data_direct;
			data_tlb_miss_delayed <= data_tlb_miss;
		end
	end
  
  
	assign instr_tlb_kmissed = instr_tlb_miss  & ~instr_direct & instr_virt[31];
	assign instr_tlb_umissed = instr_tlb_miss  & ~instr_direct & ~instr_virt[31];
	assign instr_tlb_invalid = ~instr_tlb_vbit & ~instr_direct_delayed & ~iexc_startup;
	assign instr_tlb_wrtprot = ~instr_tlb_wbit & ~instr_direct_delayed & ~iexc_startup;
	assign instr_tlb_badaddr = ~(instr_virt[1:0] == 2'b00);	
	assign instr_bus_timeout_exc = instr_bus_timeout || (io_timeout && instr_phys[29] && ~data_phys[29]);
  
	assign data_tlb_kmissed = data_tlb_miss  & ~data_direct & data_virt[31];
	assign data_tlb_umissed = data_tlb_miss  & ~data_direct & ~data_virt[31];
	assign data_tlb_invalid = ~data_tlb_vbit & ~data_direct_delayed;
	assign data_tlb_wrtprot = ~data_tlb_wbit & ~data_direct_delayed;
	assign data_tlb_badaddr = ( ~( (data_virt[1:0] == 2'b00 & data_size[1]) | 
								   (data_virt[0]   == 1'b0  & data_size[1:0] == 2'b01) |
								  hold_data_stage_1 | flush_data_stage_1 ));
	assign data_bus_timeout_exc = data_bus_timeout || (io_timeout && data_phys[29]);
								

endmodule
