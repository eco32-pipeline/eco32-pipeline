//--------------------------------------------------------------
// ioctrl -- The IO-controller
//--------------------------------------------------------------

`timescale 1ns/10ps

module ioctrl (
	       clk, rst,
	       stb, we, addr, data_in, data_out, ack,
	       irq, timeout,
	       io_we, io_dout, io_addr, 
	       kbd_ack, kbd_dout, kbd_irq, kbd_stb,
	       ser1_ack, ser1_dout, ser1_irq_r, ser1_irq_t, ser1_stb,
	       ser2_ack, ser2_dout, ser2_irq_r, ser2_irq_t, ser2_stb,
	       tmr2_dout, tmr2_ack, tmr2_irq, tmr2_stb,
	       tmr1_dout, tmr1_ack, tmr1_irq, tmr1_stb,
	       dsk_dout, dsk_ack, dsk_irq, dsk_stb,
	       dsk_dout, dsk_ack, dsk_stb,
	       fms_dout, fms_ack, fms_stb,
	       bio_dout, bio_ack, bio_stb
	       );


        input              clk;
        input              rst;

        // MMU interface
        input      stb;
        input      we;
        input      [31:0]  addr;
        input      [31:0]  data_in;

        output     [31:0]  data_out;
        output 	           ack;
        output             timeout;

        // CPU interface
        output     [15:0]  irq;

        // General IO-interface
        output 	           io_we;
        output     [31:0]  io_dout;
        output     [31:0]  io_addr;

        // rom
        output 	           rom_stb; 	// rom strobe
        input      [31:0]  rom_dout;	// rom data output
        input 	           rom_ack;		// rom acknowledge

        // Kbd interface
        input 	           kbd_ack;
        input       [7:0]  kbd_dout;
        input 	           kbd_irq;

        output 	           kbd_stb;


        // Ser1 interface
        input 	           ser1_ack;
        input       [7:0]  ser1_dout;
        input 	           ser1_irq_r;
        input 	           ser1_irq_t;

        output 	           ser1_stb;
        //output ser1_i; //Nur für Simulation; wird hartkodiert auf 0

        // Ser2 interface
        input 	           ser2_ack;
        input       [7:0]  ser2_dout;
        input 	           ser2_irq_r;
        input 	           ser2_irq_t;

        output 	           ser2_stb;
        //output ser2_i; //Nur für Simulation; wird hartkodiert auf 1

        // Tmr2 interface
        input      [31:0]  tmr2_dout;
        input 	           tmr2_ack;
        input 	           tmr2_irq;

        output 	           tmr2_stb;

        // Tmr1 interface
        input      [31:0]  tmr1_dout;
        input 	           tmr1_ack;
        input 	           tmr1_irq;

        output 	           tmr1_stb;

        // dsp
        output 	           dsp_stb;		// dsp strobe
        input      [15:0]  dsp_dout;	// dsp data output
        input 	           dsp_ack;		// dsp acknowledge

        // DSK interface
        input      [31:0]  dsk_dout; 
        input 	           dsk_ack;
        input 	           dsk_irq;

        output 	           dsk_stb;

        // Fms interface
        input      [31:0]  fms_dout;
        input 	           fms_ack;

        output 	           fms_stb;

        // Dac interface

        // BIO interface
        input      [31:0]  bio_dout;	
        input 	           bio_ack;

        output 	           bio_stb;


    reg  [7:0]  timeout_count;


    // bus timeout detector
    assign timeout = (bus_count == 8'h00) ? 1 : 0;
    always @(posedge clk) begin
        if (stb == 1 && ack == 0) begin
            // bus is waiting
            timeout_count <= timeout_count - 1;
        end else begin
            // bus is not waiting
            timeout_count <= 8'hFF;
        end
    end
   
   

   assign io_dout[31:0] = data_in[31:0];
   assign io_addr[31:0] = addr[31:0];
   assign io_we = we;


   //--------------------------------------
   // bus interrupt request assignments
   //--------------------------------------

   assign irq[15] = tmr2_irq;
   assign irq[14] = tmr1_irq;
   assign irq[13] = 0;
   assign irq[12] = 0;
   assign irq[11] = 0;
   assign irq[10] = 0;
   assign irq[ 9] = 0;
   assign irq[ 8] = dsk_irq;
   assign irq[ 7] = 0;
   assign irq[ 6] = 0;
   assign irq[ 5] = 0;
   assign irq[ 4] = kbd_irq;
   assign irq[ 3] = ser2_irq_r;
   assign irq[ 2] = ser2_irq_t;
   assign irq[ 1] = ser1_irq_r;
   assign irq[ 0] = ser1_irq_t;
   
   //--------------------------------------
   // address decoder
   //--------------------------------------

   // ROM: architectural limit  = 256 MB
   //      implementation limit =   2 MB
   assign rom_stb =
		   (stb == 1 && addr[31:28] == 4'b0010
                    && addr[27:21] == 7'b0000000) ? 1 : 0;

   // I/O: architectural limit  = 256 MB
   assign tmr1_stb =
		    (stb == 1 && addr[27:20] == 8'h00
                     && addr[19:12] == 8'h00) ? 1 : 0;
   assign tmr2_stb =
		    (stb == 1 && addr[27:20] == 8'h00
                     && addr[19:12] == 8'h01) ? 1 : 0;
   assign dsp_stb =
		   (stb == 1 && addr[27:20] == 8'h01) ? 1 : 0;
   assign kbd_stb =
		   (stb == 1 && addr[27:20] == 8'h02) ? 1 : 0;
   assign ser1_stb =
		    (stb == 1 && addr[27:20] == 8'h03
                     && addr[19:12] == 8'h00) ? 1 : 0;
   assign ser2_stb =
		    (stb == 1 && addr[27:20] == 8'h03
                     && addr[19:12] == 8'h01) ? 1 : 0;
   assign dsk_stb =
		   (stb == 1 && addr[27:20] == 8'h04) ? 1 : 0;
   assign fms_stb =
		   (stb == 1 && addr[27:20] == 8'h05
                    && addr[19:12] == 8'h00) ? 1 : 0;
   assign bio_stb =
		   (stb == 1 && addr[27:20] == 8'h10
                    && addr[19:12] == 8'h00) ? 1 : 0;

   //--------------------------------------
   // data and acknowledge multiplexers
   //--------------------------------------

   assign data_out[31:0] =
			  (rom_stb == 1)  ? rom_dout[31:0] :
			  (tmr1_stb == 1) ? tmr1_dout[31:0] :
			  (tmr2_stb == 1) ? tmr2_dout[31:0] :
			  (dsp_stb == 1)  ? { 16'h0000, dsp_dout[15:0] } :
			  (kbd_stb == 1)  ? { 24'h000000, kbd_dout[7:0] } :
			  (ser1_stb == 1) ? { 24'h000000, ser1_dout[7:0] } :
			  (ser2_stb == 1) ? { 24'h000000, ser2_dout[7:0] } :
			  (dsk_stb == 1)  ? dsk_dout[31:0] :
			  (fms_stb == 1)  ? fms_dout[31:0] :
			  (bio_stb == 1)  ? bio_dout[31:0] :
			  32'h00000000;

   assign ack =
	       (rom_stb == 1)  ? rom_ack :
	       (tmr1_stb == 1) ? tmr1_ack :
	       (tmr2_stb == 1) ? tmr2_ack :
	       (dsp_stb == 1)  ? dsp_ack :
	       (kbd_stb == 1)  ? kbd_ack :
	       (ser1_stb == 1) ? ser1_ack :
	       (ser2_stb == 1) ? ser2_ack :
	       (dsk_stb == 1)  ? dsk_ack :
	       (fms_stb == 1)  ? fms_ack :
	       (bio_stb == 1)  ? bio_ack :
	       0;
   
endmodule
