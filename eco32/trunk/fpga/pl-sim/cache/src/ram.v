//
// ram.v -- simulate external RAM
//          8M x 32 bit = 32 MB
//


`timescale 1ns/10ps


module ram(clk, rst,
           stb, we, addr,
           data_in, data_out, ack);
    input clk;
    input rst;
    input stb;
    input we;
    input [24:2] addr;
    input [31:0] data_in;
    output reg [31:0] data_out;
    output ack;

  reg [31:0] mem[0:8388607];
  reg rd_done;

  always @(posedge clk) begin
    if (stb) begin
      if (we) begin
        // write cycle
        mem[addr] <= data_in;
      end else begin
        // read cycle
        data_out <= mem[addr];
      end
    end
  end

  always @(posedge clk) begin
    if (rst) begin
      rd_done <= 0;
    end else begin
      if (stb & ~we) begin
        // a read needs two clock cycles
        rd_done <= ~rd_done;
      end
    end
  end

  assign ack = (stb & we) | rd_done;

endmodule
