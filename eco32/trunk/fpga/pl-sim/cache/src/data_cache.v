/************************/
/* Pipelined Data Cache */
/************************/

`timescale 1ns/10ps

`define HALF_EXT 16'b0
`define BYTE_EXT 24'b0

module dcache(clk, rst, size,
				hold_tf, flush_tf, flush_df,
				tag_in, offset, we, hit, 
				din, dout,
				bus_stb, bus_we, bus_addr, bus_din, bus_dout, bus_ack, bus_timeout);

	/* input and output wires/ regs */
	input clk;
	input rst;
	input hold_tf;
	input flush_tf;
	input flush_df;
	input [2:0] size;
	input we;

	input [31:12] tag_in;
	input [11:0] offset;

	output [31:0] dout;
	input [31:0] din;
	output hit;

	/* wires to decide in which bank the hit occurs */
	wire hit_a; 
	wire hit_b;

	/* bus wires for communication with memctrl */
	output reg bus_stb;
	input [63:0] bus_din;
	output reg[63:0] bus_dout;
	output reg[25:0] bus_addr;
	output reg bus_we;
	input bus_ack;
	input bus_timeout;

	/* Signals to store for 2nd pipeline stage */
	reg [2:0]old_size;
	reg [11:0]old_offset;
	reg old_we;
	reg old_hit;
	reg [31:0]old_din;
	
	/*3rd stage: writebuffer and output muxing*/
	reg buffer_we;
	reg [2:0]mux_size;
	reg [11:0]mux_offset;
	reg [28:12]old_tag;

	/* indices to adress tags and cachelines*/
	wire [10:0] index_a;
	wire [10:0] index_b;
	wire [9:0] index_tag_a;
	wire [9:0] index_tag_b;
	wire [9:0]  index_miss;

	wire [63:0] mux_data;
	wire [31:0] word;
	wire [31:0] halfword;
	wire [15:0] byte;

	/* wires to mux the output for dout and bufferline*/
	wire [31:0]dout_word_a;
	wire [31:0]dout_word_b;
	wire [15:0]dout_hw_a;
	wire [15:0]dout_hw_b;
	wire [7:0]dout_byte_a;
	wire [7:0]dout_byte_b;
	wire [31:0]dout_ext_a;
	wire [31:0]dout_ext_b;

	wire [31:0] dout_a;
	wire [31:0] dout_b;
	
	/* wires to decide if and on which index a bufferline already exists */
	wire [1:0] found;
	wire [21:0] ad0, ad1, ad2, ad3;
	wire [3:0] match;
	wire buffer_hit;


	/* memory and tag_ram of the cache */
	reg [7:0] mem_0[0:2047];
	reg [7:0] mem_1[0:2047];
	reg [7:0] mem_2[0:2047];
	reg [7:0] mem_3[0:2047];

	reg [29:12] tag[0:1023];

	reg 	    lru[0:511];		

	reg [7:0] out_a_0, out_b_0, out_a_1, out_b_1, 
			  out_a_2, out_b_2, out_a_3, out_b_3;


	reg [29:12] tag_a_match;
	reg [29:12] tag_b_match;

	reg [63:0] bufferLine [0:3];
	reg bufferValid[0:3];			/* valid-Bit for buffer entries */
	reg [25:0] bufferAddr [0:3];
	reg [1:0] buffer_rd_idx;		/* if rd_idx == wr_idx --> buffer empty */
	reg [1:0] buffer_wr_idx;		/* if rd_idx == wr_idx+1 --> buffer full */
	reg [1:0] buffer_wr_old;
	
	wire wr_tag;
	reg startup;
	reg old_startup;
	reg old_rst;
	wire common_hit;
	reg buffer_working;
	reg strobe_enable;
	reg old_ack;
	reg [29:12] prev_tag;
	
	/* counter for tag_rst_idx*/
	reg [9:0]count = 10'b0;
	
	wire [29:12]tag_data;
	
		/* zwei we-leitungen pro mem
		(fuer jeden port eine) 		*/
	wire we_a_0, we_a_1, we_a_2, we_a_3, we_b_0, we_b_1, we_b_2, we_b_3;


	/* input-leitungen, auf die die schreibdaten
		von CPU oder MemCtrl gelegt werden */
	wire [63:0] data_in;


	wire write_a_0, write_a_1, write_a_2, write_a_3,
		 write_b_0, write_b_1, write_b_2, write_b_3; 

	
	wire flush_data;
	
	reg last_flush_tf;
	
	assign flush_data = flush_df | last_flush_tf;


/* index_tag adressiert beide speicherbänke, index_a und index_b die cacheline*/

	assign index_miss = lru[old_offset[11:3]] ? { 1'b0, old_offset[11:3] } : { 1'b1, old_offset[11:3] };

	assign index_a = (~common_hit && bus_ack && ~buffer_working) ? { index_miss, 1'b0 } : 
					 hit_a ? { 1'b0, old_offset[11:3], 1'b0 } : { 1'b1, old_offset[11:3], 1'b0 };
						
	assign index_b = (~common_hit && bus_ack && ~buffer_working) ? { index_miss, 1'b1 } : 
					 hit_a ? { 1'b0, old_offset[11:3], 1'b1 } : { 1'b1, old_offset[11:3], 1'b1 };

	assign index_tag_a = rst ? count :  (~common_hit && bus_stb && ~bus_ack && ~buffer_working) ? index_miss :  
						 ~( flush_tf | hold_tf ) ? { 1'b0, offset[11:3] } : { 1'b0, old_offset[11:3] };
						 
						 
	assign index_tag_b = ~( flush_tf | hold_tf ) ? { 1'b1, offset[11:3] } : { 1'b1, old_offset[11:3] };



	assign write_a_0 = ((hit_a && old_we) || (old_ack && old_we)) && 
						((old_offset[2] == 1'b0 && old_size[1]) ||
						 (old_offset[2:1] == 2'b00 && old_size[1:0] == 2'b01) || 
						 (old_offset[2:0] == 3'b000 && old_size[1:0] == 2'b00));



	assign write_a_1 = ((hit_a && old_we) || (old_ack && old_we)) && 
						((old_offset[2] == 1'b0 && old_size[1]) ||
						 (old_offset[2:1] == 2'b00 && old_size[1:0] == 2'b01) || 
						 (old_offset[2:0] == 3'b001 && old_size[1:0] == 2'b00));



	assign write_a_2 = ((hit_a && old_we) || (old_ack && old_we)) && 
						((old_offset[2] == 1'b0 && old_size[1]) ||
						 (old_offset[2:1] == 2'b01 && old_size[1:0] == 2'b01) || 
						 (old_offset[2:0] == 3'b010 && old_size[1:0] == 2'b00));


	assign write_a_3 = ((hit_a && old_we) || (old_ack && old_we)) && 
						((old_offset[2] == 1'b0 && old_size[1]) ||
						 (old_offset[2:1] == 2'b01 && old_size[1:0] == 2'b01) || 
						 (old_offset[2:0] == 3'b011 && old_size[1:0] == 2'b00));


	assign write_b_0 = ((hit_b && old_we) || (old_ack && old_we)) && 
						((old_offset[2] == 1'b1 && old_size[1]) ||
						 (old_offset[2:1] == 2'b10 && old_size[1:0] == 2'b01) || 
						 (old_offset[2:0] == 3'b100 && old_size[1:0] == 2'b00));


	assign write_b_1 = ((hit_b && old_we) || (old_ack && old_we)) && 
						((old_offset[2] == 1'b1 && old_size[1]) ||
						 (old_offset[2:1] == 2'b10 && old_size[1:0] == 2'b01) || 
						 (old_offset[2:0] == 3'b101 && old_size[1:0] == 2'b00));


	assign write_b_2 = ((hit_b && old_we) || (old_ack && old_we)) && 
						((old_offset[2] == 1'b1 && old_size[1]) ||
						 (old_offset[2:1] == 2'b11 && old_size[1:0] == 2'b01) || 
						 (old_offset[2:0] == 3'b110 && old_size[1:0] == 2'b00));


	assign write_b_3 = ((hit_b && old_we) || (old_ack && old_we)) && 
						((old_offset[2] == 1'b1 && old_size[1]) ||
						 (old_offset[2:1] == 2'b11 && old_size[1:0] == 2'b01) || 
						 (old_offset[2:0] == 3'b111 && old_size[1:0] == 2'b00));

	/* write nur wenn cachemiss oder flush_df low */


	assign we_a_0 = ((~common_hit & ~buffer_working & (bus_ack | buffer_hit)) | (write_a_0 & ~flush_data));
	assign we_a_1 = ((~common_hit & ~buffer_working & (bus_ack | buffer_hit)) | (write_a_1 & ~flush_data));
	assign we_a_2 = ((~common_hit & ~buffer_working & (bus_ack | buffer_hit)) | (write_a_2 & ~flush_data));
	assign we_a_3 = ((~common_hit & ~buffer_working & (bus_ack | buffer_hit)) | (write_a_3 & ~flush_data));

	assign we_b_0 = ((~common_hit & ~buffer_working & (bus_ack | buffer_hit)) | (write_b_0 & ~flush_data));
	assign we_b_1 = ((~common_hit & ~buffer_working & (bus_ack | buffer_hit)) | (write_b_1 & ~flush_data));
	assign we_b_2 = ((~common_hit & ~buffer_working & (bus_ack | buffer_hit)) | (write_b_2 & ~flush_data));
	assign we_b_3 = ((~common_hit & ~buffer_working & (bus_ack | buffer_hit)) | (write_b_3 & ~flush_data));



	assign mux_data = hit_a ? { 32'hx, word } : { word, 32'hx };
	assign word = old_size[1] ? old_din : halfword;

	assign halfword = old_size[0] ? old_offset[1] ? { old_din[15:0], 16'hx } : { 16'hx, old_din[15:0] } : 
			/* size[0] == 0	*/  old_offset[1] ? { byte, 16'hx } 	 : { 16'hx, byte };

	assign byte = old_offset[0] ? { old_din[7:0], 8'hx } : { 8'hx, old_din[7:0] };



	/* bei cache-miss bus_din Leitung von memctrl auf data_in legen;
		sonst richtiges datum von cpu an richtige stelle in data_in */
	assign data_in = (~common_hit && bus_ack && ~buffer_working) ? bus_din : (~common_hit && buffer_hit) ? bufferLine[found] : mux_data;


	wire buffer_full = (buffer_wr_idx == 3'b11) ? (buffer_rd_idx == 3'b00) : ((buffer_wr_idx+1) == buffer_rd_idx);
	wire buffer_empty = (buffer_wr_idx == buffer_rd_idx);
	wire [63:0]buffer_out;


	assign hit_a = ({1'b1,  tag_in[28:12]} == tag_a_match) && ~(bus_stb && ~buffer_working);
	assign hit_b = ({1'b1,  tag_in[28:12]} == tag_b_match) && ~(bus_stb && ~buffer_working);
	
	assign common_hit = hit_a || hit_b;
	assign hit = (rst || old_rst) ? 1'b0 : (we && buffer_full) ? 1'b0 : 
				 ((bus_ack || buffer_hit) && ~buffer_working && ~old_we) ? 1'b1 : common_hit;

	assign tag_data = rst ? {2'b0x, 16'hxxxx} : bus_timeout ? prev_tag : 
					  (~common_hit && buffer_hit) ? bufferAddr[found] : {1'b1, tag_in[28:12]};
	
	assign wr_tag = (bus_stb && ~bus_ack && ~buffer_working) || (~common_hit && buffer_hit) || rst;

	/* decide if a bufferline already exists */
	assign ad0 = bufferAddr[0];
	assign ad1 = bufferAddr[1];
	assign ad2 = bufferAddr[2];
	assign ad3 = bufferAddr[3];

	assign match[0] = ~common_hit ? (({tag_in[28:12], mux_offset[11:3] } == ad0) && bufferValid[0]) : 
								  (({ old_tag[28:12], mux_offset[11:3] } == ad0) && bufferValid[0]);
	assign match[1] = ~common_hit ? (({tag_in[28:12], mux_offset[11:3] } == ad1) && bufferValid[1]) : 
							      (({ old_tag[28:12], mux_offset[11:3] } == ad1) && bufferValid[1]);
	assign match[2] = ~common_hit ? (({tag_in[28:12], mux_offset[11:3] } == ad2) && bufferValid[2]) : 
							      (({ old_tag[28:12], mux_offset[11:3] } == ad2) && bufferValid[2]);
	assign match[3] = ~common_hit ? (({tag_in[28:12], mux_offset[11:3] } == ad3) && bufferValid[3]) : 
							      (({ old_tag[28:12], mux_offset[11:3] } == ad3) && bufferValid[3]);


							   
	assign buffer_hit = (match[0] | match[1] | match[2] | match[3]);

	assign found[0] = match[1] | match[3];
	assign found[1] = match[2] | match[3];	
	

	/* 3 Modi:
		- Lesen (hit & ~we)
		- Schreiben (hit & we)
		- Cache-Miss (~hit)		*/
		
	always @(posedge clk)begin
		if(~hold_tf)begin
			last_flush_tf <= flush_tf;
		end
		prev_tag <= lru[old_offset[11:3]] ? tag_a_match : tag_b_match;
		old_ack <= bus_ack;		
		old_hit <= hit;
		/* store old values for 2nd pipelins stage*/
		if(~( hold_tf | flush_tf ) & ~rst & (startup || hit))begin
			
			old_startup <= startup;
		
			old_size <= size;
			old_offset <= offset[11:0];
			old_we <= we;
			
			old_din <= din;
			old_rst <= rst;
	
			strobe_enable <= ~(hold_tf | flush_tf);
		end else begin
			if(bus_ack && ~common_hit && ~buffer_working && strobe_enable && ~buffer_hit) begin
				strobe_enable <= 1'b0;
			end
		end
		if(~flush_data | ~rst && (old_startup | old_hit))begin
			mux_size <= old_size;
			mux_offset <= old_offset[11:0];
			old_tag <= tag_in[28:12];
			buffer_we <= old_we;
		end


		if(wr_tag)begin
			tag[index_tag_a] <= tag_data; 
		end else begin
			tag_a_match <= tag[index_tag_a];
			tag_b_match <= tag[index_tag_b];
		end	
	end
	
	always @(posedge clk) begin
		if(rst)begin
			lru[count[8:0]] <= 0; /* only for simulation purposes */
			buffer_rd_idx <= 3'b0;		/* init writeBuffer */
			buffer_wr_idx <= 3'b0;
			buffer_wr_old <= 3'b11;
			bufferAddr[0] <= 26'b0;
			bufferAddr[3] <= 26'b1;
			bufferLine[0] <= 64'b0;
			bufferLine[3] <= 64'b1;

			/* reset valid bits */
			bufferValid[0] <= 1'b0;
			bufferValid[1] <= 1'b0;
			bufferValid[2] <= 1'b0;
			bufferValid[3] <= 1'b0;
			
			/* increment count */
			count <= (count == 10'd1023) ? 10'b0 : count + 1'b1;
			startup <= 1;
			
			bus_stb <= 1'b0;
			buffer_working <= 1'b0;

		end else begin
			if(~(hold_tf | flush_tf) & (startup || hit))begin
				startup <= 0;
			end
			if(~common_hit & ~buffer_working & strobe_enable & ~buffer_hit & ~flush_data)begin
				if(bus_ack)begin
					bus_stb <= 1'b0;
				end else begin
					bus_stb <= 1'b1;
					bus_we <= 1'b0;
					bus_addr <= { tag_in[28:12], old_offset[11:3] };
				end
			end else begin
				if(~flush_data)begin
					/* hit */
					if(hit_a)begin
						lru[offset[11:3]] <= 1'b0;
					end 
					if(hit_b)begin
						lru[offset[11:3]] <= 1'b1;
					end
				end
				/*writebuffer*/
				if(buffer_we && ~buffer_full && old_hit)begin
				
					if(buffer_hit) begin
						/* replace data in existing addr with new data */
						bufferLine[found] <= buffer_out;

					end else begin
						/* add new addr to buffer */
						if(~(bufferAddr[buffer_wr_old] == bufferAddr[buffer_wr_idx]) && 
						   ~(bufferLine[buffer_wr_old] == bufferLine[buffer_wr_idx]))begin
								bufferAddr[buffer_wr_idx] <= { old_tag[28:12], mux_offset[11:3] };
								bufferLine[buffer_wr_idx] <= buffer_out;
								bufferValid[buffer_wr_idx] <= 1'b1; 	/* set Valid Bit */
								buffer_wr_idx <= (buffer_wr_idx == 3'b11) ? 3'b00 : buffer_wr_idx + 1'b1;
								buffer_wr_old <= buffer_wr_idx;
						end
					end
				end
				if(~buffer_working)begin
					if(~buffer_empty)begin
						buffer_working <= 1'b1;
						bus_stb <= 1'b1;
						bus_we <= 1'b1;
						bufferValid[buffer_rd_idx] <= 1'b0;		/* reset Vald-Bit */
						bus_addr <= bufferAddr[buffer_rd_idx];
						bus_dout <= bufferLine[buffer_rd_idx];
					end
				end else begin
					if(bus_ack)begin
						buffer_working <= 1'b0;
						bus_stb <= 1'b0;
						buffer_rd_idx <= (buffer_rd_idx == 3'b11) ? 3'b00 : buffer_rd_idx + 1'b1;
					end
				end
				/* end buffer*/
			end
		end
	end
	


/* ------ 1st Byte ------ */
	always @(posedge clk) begin
		
		if(~rst)begin

			if(we_a_0)begin							/* write */
				mem_0[index_a] <= data_in[39:32];
				out_a_0 <= data_in[39:32];
			end	else begin
				out_a_0 <= mem_0[index_a];			/* read */
			end
		end
	end

	always @(posedge clk) begin
		
		if(~rst)begin

			if(we_b_0)begin							/* write */
				mem_0[index_b] <= data_in[7:0];
				out_b_0 <= data_in[7:0];
			end else begin
				out_b_0 <= mem_0[index_b];			/* read */
			end
		end
	end

/* ---------------------- */


/* ------ 2nd Byte ------ */
	always @(posedge clk) begin
		
		if(~rst)begin

			if(we_a_1)begin							/* write */
				mem_1[index_a] <= data_in[47:40];
				out_a_1 <= data_in[47:40];
			end else begin
				out_a_1 <= mem_1[index_a];			/* read */
			end
		end
	end

	always @(posedge clk) begin
		
		if(~rst)begin

			if(we_b_1)begin							/* write */
				mem_1[index_b] <= data_in[15:8];
				out_b_1 <= data_in[15:8];
			end else begin
				out_b_1 <= mem_1[index_b];			/* read */
			end
		end
	end

/* ---------------------- */


/* ------ 3rd Byte ------ */
	always @(posedge clk) begin
		
		if(~rst)begin

			if(we_a_2)begin							/* write */
				mem_2[index_a] <= data_in[55:48];
				out_a_2 <= data_in[55:48];
			end else begin
				out_a_2 <= mem_2[index_a];			/* read */
			end
		end
	end

	always @(posedge clk) begin
		
		if(~rst)begin

			if(we_b_2)begin							/* write */
				mem_2[index_b] <= data_in[23:16];
				out_b_2 <= data_in[23:16];
			end else begin
				out_b_2 <= mem_2[index_b];			/* read */
			end
		end
	end

/* ---------------------- */


/* ------ 4th Byte ------ */
	always @(posedge clk) begin
		
		if(~rst)begin

			if(we_a_3)begin							/* write */
				mem_3[index_a] <= data_in[63:56];
				out_a_3 <= data_in[63:56];
			end else begin
				out_a_3 <= mem_3[index_a];			/*read*/
			end
		end
	end

	always @(posedge clk) begin
		
		if(~rst)begin

			if(we_b_3)begin							/* write */
				mem_3[index_b] <= data_in[31:24];
				out_b_3 <= data_in[31:24];
			end else begin
				out_b_3 <= mem_3[index_b];			/* read */
			end
		end
	end

/* mux output for dout and bufferline*/
	
	assign dout_word_a ={ out_a_3, out_a_2, out_a_1, out_a_0 };
							  
	assign dout_word_b ={ out_b_3, out_b_2, out_b_1, out_b_0 };

	assign dout_hw_a = mux_offset[1] ? dout_word_a[31:16] : dout_word_a[15:0];
	assign dout_hw_b = mux_offset[1] ? dout_word_b[31:16] : dout_word_b[15:0];	
	
	assign dout_byte_a = mux_offset[0] ? dout_hw_a[15:8] : dout_hw_a[7:0];
	assign dout_byte_b = mux_offset[0] ? dout_hw_b[15:8] : dout_hw_b[7:0];	
	
	assign dout_ext_a = mux_size[0] ? (mux_size[2] ? {~`HALF_EXT, dout_hw_a} : {`HALF_EXT, dout_hw_a}) : 
												(mux_size[2]? {~`BYTE_EXT, dout_byte_a} : {`BYTE_EXT, dout_byte_a});
												
	assign dout_ext_b = mux_size[0] ? (mux_size[2] ? {~`HALF_EXT, dout_hw_b} : {`HALF_EXT, dout_hw_b}) : 
												(mux_size[2]? {~`BYTE_EXT, dout_byte_b} : {`BYTE_EXT, dout_byte_b});
												
	assign dout_a = mux_size[1] ? dout_word_a : dout_ext_a;
	assign dout_b = mux_size[1] ? dout_word_b : dout_ext_b;
	
	assign dout[31:0] = mux_offset[2] ? dout_b[31:0] : dout_a[31:0];
	
	assign buffer_out = { dout_word_a, dout_word_b };

endmodule
