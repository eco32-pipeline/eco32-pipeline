/*******************************/
/* Pipelined Instruction Cache */
/*******************************/

`timescale 1ns/10ps

module icache(clk, rst, hold_tf, flush_tf, hold_df, flush_df,
				tag_in, offset, hit, dout,
				bus_stb, bus_addr, bus_din, bus_ack, bus_timeout);
	
	input clk;
	input rst;
	/* hold an flush signals for 1st and 2nd stage */
	input hold_tf;
	input hold_df;
	input flush_tf;
	input flush_df;

	/* adress and output */
	input  [31:12] tag_in;
	input  [11:0]  offset;
	output [31:0]  dout;
	output         hit;

	/* bus communication with memctrl*/
	output reg        bus_stb;
	output reg [25:0] bus_addr;
	input      [63:0] bus_din;
	input             bus_ack;
	input             bus_timeout;


	reg [11:2]  old_offset;
	reg         old_rst;
	reg         mux_offset;
		
	reg [29:12] tag_a_match;
	reg [29:12] tag_b_match;
	
	/* wires/ regs for fluh/hold logic in 2nd stage/ dout */
	wire flush_data;
	reg  old_flush_tf;
	reg  last_hold_df;
	reg  last_flush_df;
	
	/* indices for mem and tagram */
	wire         hit_a; 
	wire         hit_b;
	wire [10:0]  index_a;
	wire [10:0]  index_b;
	wire [9:0]   index_tag_a;
	wire [9:0]   index_tag_b;
	wire [9:0]   index_miss;
	wire         wr_tag;
	wire [29:12] tag_data;
	reg  [29:12] prev_tag;

	/* word of the cacheline */
	wire [31:0] dout_a;
	wire [31:0] dout_b;

	/* cache memory and tag */
	reg [7:0]   mem_0 [0:2047];
	reg [7:0]   mem_1 [0:2047];
	reg [7:0]   mem_2 [0:2047];
	reg [7:0]   mem_3 [0:2047];

	reg [29:12] tag   [0:1023];

	reg 	    lru   [0:511];		/* last-recently-used-bit
  								 * if lru[xy] = 0 -> set_0 is least recently used
  							 	 * if lru[xy] = 1 -> set_1 is least recently used
  								 */

	reg [7:0] out_a_0, out_b_0, out_a_1, out_b_1, 
			  out_a_2, out_b_2, out_a_3, out_b_3;
	
	reg [7:0] last_out_a_0, last_out_b_0, last_out_a_1, last_out_b_1, 
			  last_out_a_2, last_out_b_2, last_out_a_3, last_out_b_3;

	/* regs for proper startup */
	reg [9:0] count = 10'b0;
	reg       startup;
	
	reg last_hit;


	/* decide wether to use normal data/ tag_idx or reset data/ tag_idx */

	assign index_tag_a = rst ? count : ( ~bus_ack && bus_stb ) ? index_miss : 
						               ( flush_tf | hold_tf )  ? { 1'b0, old_offset[11:3] } : { 1'b0, offset[11:3] };
	
	assign index_tag_b = rst ? count : { 1'b1, offset[11:3] };

	assign index_miss  = (lru[old_offset[11:3]] == 0) ? { 1'b1, old_offset[11:3] } : { 1'b0, old_offset[11:3] };

	assign index_a     = bus_ack ? { index_miss, 1'b0 } : hit_a ? { 1'b0, old_offset[11:3], 1'b0 } : 
						           { 1'b1, old_offset[11:3], 1'b0 };
						
	assign index_b     = bus_ack ? { index_miss, 1'b1 } : hit_a ? { 1'b0, old_offset[11:3], 1'b1 } : 
						           { 1'b1, old_offset[11:3], 1'b1 };


	assign flush_data = old_flush_tf | flush_df;
	

	assign hit_a = ({1'b1, tag_in[28:12]} == tag_a_match) && ~bus_stb;
	assign hit_b = ({1'b1, tag_in[28:12]} == tag_b_match) && ~bus_stb;
	assign hit   = (rst || old_rst) ? 1'b0 : bus_ack ? 1'b1 : (hit_a || hit_b);


	assign tag_data = rst ? {2'b0x, 12'hxxx} : bus_timeout ? prev_tag : {1'b1, tag_in[28:12]};	
	
	assign wr_tag   = (bus_stb && ~bus_ack) || rst;


	always @(posedge clk) begin
		prev_tag <= lru[old_offset] ? tag_a_match : tag_b_match;
		old_rst  <= rst;
		last_hit <= hit;
		if(~hold_tf)begin
			old_flush_tf <= flush_tf;
		end
		if(~hold_df)begin
			mux_offset <= old_offset[2];
		end
		
		/* tag fetch */
		if(wr_tag)begin
			tag[index_tag_a] <= tag_data; 
		end else begin
			tag_a_match <= tag[index_tag_a];
			tag_b_match <= tag[index_tag_b];	
		end	

	end
	
	always @ ( posedge clk ) begin
		if ( rst ) begin
			/* increment count */
			count <= (count == 10'd1023) ? 10'b0 : count + 1'b1;
			lru[count[8:0]] <= 0; /* only for simulation purposes */
			startup <= 1;
			bus_stb <= 1'b0;
		end else begin
			if(~hold_tf & (startup | hit))begin
				startup    <= 0;
				old_offset <= offset[11:2];
			end
			/* cache miss/ communication memctrl */
			if ( bus_ack | bus_timeout ) begin
				bus_stb <= 1'b0;
				lru[old_offset[11:3]] <= ~lru[old_offset[11:3]];
			end
			if ( ~flush_data ) begin
				if ( ~hit ) begin
					if(~startup & ~bus_ack )begin
						bus_stb  <= 1'b1;
						bus_addr <= { tag_in[28:12], old_offset[11:3] };
					end
				end else begin
					/* hit */
					if ( hit_a ) begin
						lru[old_offset[11:3]] <= 1'b0;
					end 
					if ( hit_b ) begin
						lru[old_offset[11:3]] <= 1'b1;
					end
				end
			end
		end
	end
	
	
/**********************/
/* 2nd Pipeline Stage */
/**********************/

	/* control 2nd stage */
	always @(posedge clk) begin
		last_hold_df  <=  hold_df;
		last_flush_df <= flush_data;

		if( ~hold_df )begin
			if( flush_df | ~hit) begin
				last_out_a_0 <= 8'h0;
				last_out_a_1 <= 8'h0;
				last_out_a_2 <= 8'h0;
				last_out_a_3 <= 8'h0;
				last_out_b_0 <= 8'h0;
				last_out_b_1 <= 8'h0;
				last_out_b_2 <= 8'h0;
				last_out_b_3 <= 8'h0;
			end else begin
				last_out_a_0 <= out_a_0;
				last_out_a_1 <= out_a_1;
				last_out_a_2 <= out_a_2;
				last_out_a_3 <= out_a_3;
				last_out_b_0 <= out_b_0;
				last_out_b_1 <= out_b_1;
				last_out_b_2 <= out_b_2;
				last_out_b_3 <= out_b_3;
			end
		end
	end


	/* ------ 1st Byte ------ */
	always @(posedge clk) begin
		
		if( bus_ack )begin
			mem_0[index_a] <= bus_din[39:32];
			out_a_0        <= bus_din[39:32];
		end else begin
			out_a_0        <= mem_0[index_a];
		end

	end

	always @(posedge clk) begin
		
		if( bus_ack )begin
			mem_0[index_b] <= bus_din[7:0];
			out_b_0        <= bus_din[7:0];
		end else begin
			out_b_0        <= mem_0[index_b];
		end
	end

/* ---------------------- */


/* ------ 2nd Byte ------ */
	always @(posedge clk) begin

		if( bus_ack )begin
			mem_1[index_a] <= bus_din[47:40];
			out_a_1        <= bus_din[47:40];
		end else begin
			out_a_1        <= mem_1[index_a];
		end
	end

	always @(posedge clk) begin

		if( bus_ack )begin
			mem_1[index_b] <= bus_din[15:8];
			out_b_1        <= bus_din[15:8];
		end else begin
			out_b_1        <= mem_1[index_b];
		end
	end

/* ---------------------- */


/* ------ 3rd Byte ------ */
	always @(posedge clk) begin

		if( bus_ack )begin
			mem_2[index_a] <= bus_din[55:48];
			out_a_2        <= bus_din[55:48];
		end else begin
			out_a_2        <= mem_2[index_a];
		end
	end

	always @(posedge clk) begin

		if( bus_ack )begin
			mem_2[index_b] <= bus_din[23:16];
			out_b_2        <= bus_din[23:16];
		end else begin
			out_b_2        <= mem_2[index_b];
		end
	end

/* ---------------------- */


/* ------ 4th Byte ------ */
	always @(posedge clk) begin
	
		if( bus_ack )begin
			mem_3[index_a] <= bus_din[63:56];
			out_a_3        <= bus_din[63:56];
		end else begin
			out_a_3        <= mem_3[index_a];
		end
	end

	always @(posedge clk) begin

		if( bus_ack )begin /* write*/
			mem_3[index_b] <= bus_din[31:24];
			out_b_3        <= bus_din[31:24];
		end else begin 
			out_b_3        <= mem_3[index_b];
		end
	end
/* ---------------------- */
	
	/* mux output */
	assign dout_a[31:0] = ( last_hold_df | ~last_hit ) ? { last_out_a_3, last_out_a_2, last_out_a_1, last_out_a_0 } : 
										 			 	 { out_a_3, out_a_2, out_a_1, out_a_0 };

	assign dout_b[31:0] = ( last_hold_df | ~last_hit ) ? { last_out_b_3, last_out_b_2, last_out_b_1, last_out_b_0 } : 
										 			 	 { out_b_3, out_b_2, out_b_1, out_b_0 };

	assign dout[31:0] = ( last_flush_df & ~last_hold_df ) ? 32'h0 : mux_offset ? dout_b[31:0] : dout_a[31:0];

endmodule
