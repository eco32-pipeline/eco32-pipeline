//
// eco32.v -- ECO32 toplevel description
//

`timescale 1ns/10ps

module eco32(clk_in, rst_in_n);
        input         clk_in;      // clock input
        input         rst_in_n;    // reset input

/*
        input      [xx:xx] <name>;
        output reg [xx:xx] <name>;

    wire [xx:xx] <name>;
    reg  [xx:xx] <name>;

    <modulname> <instanzname> (
        <Parameter>
    );
*/

    //clk_rst
    wire         clk;    // system clk
    wire         rst;    // system reset


    //mmu-cpu
    wire [2:0]   cpu_data_size;
    wire         mmu_data_en;
    wire         mmu_data_we;
    wire         mmu_data_hit;
    wire [31:0]  mmu_data_din;
    wire [31:0]  cpu_data_din;

    wire         mmu_instr_hit;
    wire [31:0]  cpu_instr_din;
    wire         mmu_instr_map;
    wire [2:0]   mmu_fnc;
    wire [31:0]  mmu_instr_virt;
    wire [31:0]  mmu_data_virt;

    wire         mmu_data_tlb_kmissed;
    wire         mmu_data_tlb_invalid;
    wire         mmu_data_tlb_umissed;
    wire         mmu_data_tlb_wrtprot;
    wire         mmu_data_tlb_badaddr;
    wire		 mmu_data_bus_timeout;

    wire         mmu_instr_tlb_kmissed;
    wire         mmu_instr_tlb_umissed;
    wire         mmu_instr_tlb_invalid;
    wire         mmu_instr_tlb_wrtprot;
    wire         mmu_instr_tlb_badaddr;
    wire		 mmu_instr_bus_timeout;

    wire         cpu_hold_tf;
    wire         cpu_hold_df;
    wire         cpu_flush_df;
    wire         cpu_hold_data_tf;
    wire         cpu_flush_data_tf;
    wire         cpu_flush_data_df;


    //memctrl-mmu
    wire [25:0]  mem_data_addr;
    wire [63:0]  mem_data_dout;
    wire [63:0]  mem_data_din;
    wire         mem_data_stb;
    wire         mem_data_we;
    wire         mem_data_ack;

    wire         mem_instr_stb;
    wire [63:0]  mem_instr_dout;
    wire [25:0]  mem_instr_addr;
    wire         mem_instr_ack;


    //sregs-mmu
    wire [31:0]  sregs_tlb_index;
    wire [31:0]  sregs_tlb_index_new;
    wire [31:0]  sregs_tlb_entry_hi;
    wire [31:0]  sregs_tlb_entry_hi_new;
    wire [31:0]  sregs_tlb_entry_lo;
    wire [31:0]  sregs_tlb_entry_lo_new;


    //cpu-ioctrl
    wire [15:0]  irq;


    //mmu-ioctrl
    wire         ioctrl_stb;
    wire         ioctrl_we;
    wire [31:0]  ioctrl_addr;
    wire [31:0]  ioctrl_data_in;
    wire [31:0]  ioctrl_data_out;
    wire         ioctrl_ack;
    wire         ioctrl_timeout;


    //ioctrl-general IO
    wire         io_we;
    wire [31:0]  io_dout;
    wire [31:0]  io_addr;


    //ioctrl-kbd
    wire         kbd_ack;
    wire [7:0]   kbd_dout;
    wire 	     kbd_irq;
    wire 	     kbd_stb;


    //ioctrl-rom
    wire         rom_stb;
    wire [31:0]  rom_dout;
    wire         rom_ack;


    //ioctrl-ser1
    wire         ser1_ack;
    wire [7:0]   ser1_dout;
    wire 	     ser1_irq_r;
    wire 	     ser1_irq_t;
    wire 	     ser1_stb;


    //ioctrl-ser2
    wire         ser2_ack;
    wire [7:0]   ser2_dout;
    wire 	     ser2_irq_r;
    wire 	     ser2_irq_t;
    wire 	     ser2_stb;


    //ioctrl-tmr1
    wire [31:0]  tmr1_dout;
    wire 	     tmr1_ack;
    wire 	     tmr1_irq;
    wire 	     tmr1_stb;


    //ioctrl-tmr2
    wire [31:0]  tmr2_dout;
    wire         tmr2_ack;
    wire         tmr2_irq;
    wire         tmr2_stb;


    //ioctrl-dsp
    wire         dsp_stb;
    wire [15:0]  dsp_dout;
    wire         dsp_ack;


    clk_rst clk_rst_1 (
        .clk_in(clk_in),
        .rst_in_n(rst_in_n),
        .clk(clk),
        .rst(rst)
    );

    mmu mmu_1 (
        .clk(clk), .rst(rst),
        .fnc(mmu_fnc),

        .hold_data_stage_1(cpu_hold_data_tf),
    	.flush_data_stage_1(cpu_flush_data_tf),
    	.flush_data_stage_2(cpu_flush_data_df),

        .data_size(cpu_data_size),
        .data_we(mmu_data_we),     .data_hit(mmu_data_hit),
        .data_din(mmu_data_din),   .data_dout(cpu_data_din),
        .data_virt(mmu_data_virt),

        .instr_hit(mmu_instr_hit),
        .instr_dout(cpu_instr_din),
        .instr_virt(mmu_instr_virt),

        .tlb_index(sregs_tlb_index),       .tlb_index_new(sregs_tlb_index_new),
        .tlb_entry_hi(sregs_tlb_entry_hi), .tlb_entry_hi_new(sregs_tlb_entry_hi_new),
        .tlb_entry_lo(sregs_tlb_entry_lo), .tlb_entry_lo_new(sregs_tlb_entry_lo_new),

        .instr_tlb_kmissed(mmu_instr_tlb_kmissed), .data_tlb_kmissed(mmu_data_tlb_kmissed),
        .instr_tlb_umissed(mmu_instr_tlb_umissed), .data_tlb_umissed(mmu_data_tlb_umissed),
        .instr_tlb_invalid(mmu_instr_tlb_invalid), .data_tlb_invalid(mmu_data_tlb_invalid),
        .instr_tlb_wrtprot(mmu_instr_tlb_wrtprot), .data_tlb_wrtprot(mmu_data_tlb_wrtprot),
        .instr_tlb_badaddr(mmu_instr_tlb_badaddr), .data_tlb_badaddr(mmu_data_tlb_badaddr),

        .data_bus_stb(mem_data_stb),   .data_bus_we(mem_data_we),
        .data_bus_addr(mem_data_addr), .data_bus_din(mem_data_dout),
        .data_bus_dout(mem_data_din),  .data_bus_ack(mem_data_ack),
        .data_bus_timeout(mmu_data_bus_timeout),

        .instr_bus_stb(mem_instr_stb),   .instr_bus_din(mem_instr_dout),
        .instr_bus_addr(mem_instr_addr), .instr_bus_ack(mem_instr_ack),
        .instr_bus_timeout(mmu_instr_bus_timeout),

        .hold_instr_stage_1(cpu_hold_tf),
        .flush_instr_stage_1(0),
        .hold_instr_stage_2(cpu_hold_df),
        .flush_instr_stage_2(cpu_flush_df),

		.io_stb(ioctrl_stb), .io_we(ioctrl_we), .io_addr(ioctrl_addr),
		.io_data_in(ioctrl_data_in), .io_data_out(ioctrl_data_out),
		.io_ack(ioctrl_ack), .io_timeout(ioctrl_timeout)
    );

    cpu cpu_1 (
        .clock(clk),
        .reset(rst),

        .mmu_cm_addr(mmu_instr_virt),
        .mmu_cm_dout(cpu_instr_din),
        .mmu_cm_hit(mmu_instr_hit),

        .mmu_cm_exc_tlb_badaddr(mmu_instr_tlb_badaddr),
        .mmu_cm_exc_tlb_kmissed(mmu_instr_tlb_kmissed),
        .mmu_cm_exc_tlb_umissed(mmu_instr_tlb_umissed),
        .mmu_cm_exc_tlb_invalid(mmu_instr_tlb_invalid),
        .mmu_cm_exc_bus_timeout(mmu_instr_bus_timeout),

        .mmu_dm_addr(mmu_data_virt),
        .mmu_dm_we(mmu_data_we),
        .mmu_dm_size(cpu_data_size),
        .mmu_dm_din(mmu_data_din),
        .mmu_dm_dout(cpu_data_din),
        .mmu_dm_hit(mmu_data_hit),

        .mmu_tlb_fnc(mmu_fnc),

        .mmu_dm_exc_tlb_badaddr(mmu_data_tlb_badaddr),
        .mmu_dm_exc_tlb_kmissed(mmu_data_tlb_kmissed),
        .mmu_dm_exc_tlb_umissed(mmu_data_tlb_umissed),
        .mmu_dm_exc_tlb_invalid(mmu_data_tlb_invalid),
        .mmu_dm_exc_tlb_wrtprot(mmu_data_tlb_wrtprot),
        .mmu_dm_exc_bus_timeout(mmu_data_bus_timeout),

        .hold_tf(cpu_hold_tf),
        .hold_df(cpu_hold_df),
        .flush_df(cpu_flush_df),
        .hold_data_tf(cpu_hold_data_tf),
        .flush_data_tf(cpu_flush_data_tf),
        .flush_data_df(cpu_flush_data_df),

        .sreg_1(sregs_tlb_index),
        .sreg_2(sregs_tlb_entry_hi),
        .sreg_3(sregs_tlb_entry_lo)
    );


    ramctrl ramctrl_1 (
        .clk(clk),
        .rst(rst),
        .inst_stb(mem_instr_stb),
        .inst_addr(mem_instr_addr),
        .inst_dout(mem_instr_dout),
        .inst_ack(mem_instr_ack),
		.inst_timeout(mmu_instr_bus_timeout),

        .data_stb(mem_data_stb),
        .data_we(mem_data_we),
        .data_addr(mem_data_addr),
        .data_din(mem_data_din),
        .data_dout(mem_data_dout),
        .data_ack(mem_data_ack),
        .data_timeout(mmu_data_bus_timeout)
    );

    ioctrl ioctrl_1 (
        .clk(clk),

        .stb(ioctrl_stb),           .we(ioctrl_we),
        .addr(ioctrl_addr),         .data_in(ioctrl_data_in),
        .data_out(ioctrl_data_out), .ack(ioctrl_ack),
        .bus_timeout(ioctrl_timeout),

        .irq(irq),

        .io_we(io_we), .io_dout(io_dout),
        .io_addr(io_addr),

        .kbd_ack(kbd_ack), .kbd_dout(kbd_dout),
        .kbd_irq(kbd_irq), .kbd_stb(kbd_stb),

        .rom_stb(rom_stb), .rom_dout(rom_dout),
        .rom_ack(rom_ack),

        .ser1_ack(ser1_ack),     .ser1_dout(ser1_dout),
        .ser1_irq_r(ser1_irq_r), .ser1_irq_t(ser1_irq_t),
        .ser1_stb(ser1_stb),

        .ser2_ack(ser2_ack),     .ser2_dout(ser2_dout),
        .ser2_irq_r(ser2_irq_r), .ser2_irq_t(ser2_irq_t),
        .ser2_stb(ser2_stb),

        .tmr2_dout(tmr2_dout), .tmr2_ack(tmr2_ack),
        .tmr2_irq(tmr2_irq),   .tmr2_stb(tmr2_stb),

        .tmr1_dout(tmr1_dout), .tmr1_ack(tmr1_ack),
        .tmr1_irq(tmr1_irq),   .tmr1_stb(tmr1_stb),


        .dsp_stb(dsp_stb), .dsp_ack(dsp_ack), .dsp_dout(dsp_dout)
    );


    kbd kbd_1 (
        .clk(clk), .rst(rst),
        .stb(kbd_stb),
        .we(io_we),
        .addr(io_addr[2]),
        .data_in(io_dout[7:0]),
        .data_out(kbd_dout[7:0]),
        .ack(kbd_ack),
        .irq(kbd_irq)
    );

    rom rom_1(
        .clk(clk),
        .rst(rst),
        .stb(rom_stb),
        .we(io_we),
        .addr(io_addr[15:2]),
        .data_out(rom_dout[31:0]),
        .ack(rom_ack)
    );

    tmr tmr_1(
        .clk(clk),
        .rst(rst),
        .stb(tmr1_stb),
        .we(io_we),
        .addr(io_addr[3:2]),
        .data_in(io_dout[31:0]),
        .data_out(tmr1_dout[31:0]),
        .ack(tmr1_ack),
        .irq(tmr1_irq)
    );

    tmr tmr_2(
        .clk(clk),
        .rst(rst),
        .stb(tmr2_stb),
        .we(io_we),
        .addr(io_addr[3:2]),
        .data_in(io_dout[31:0]),
        .data_out(tmr2_dout[31:0]),
        .ack(tmr2_ack),
        .irq(tmr2_irq)
    );

    dsp dsp_1(
        .clk(clk),
        .rst(rst),
        .stb(dsp_stb),
        .we(io_we),
        .addr(io_addr[13:2]),
        .data_in(io_dout[15:0]),
        .data_out(dsp_dout[15:0]),
        .ack(dsp_ack)
    );

    ser ser_1(
        .i(0),
        .clk(clk),
        .rst(rst),
        .stb(ser1_stb),
        .we(io_we),
        .addr(io_addr[3:2]),
        .data_in(io_dout[7:0]),
        .data_out(ser1_dout[7:0]),
        .ack(ser1_ack),
        .irq_r(ser1_irq_r),
        .irq_t(ser1_irq_t)
    );

    ser ser_2(
        .i(1),
        .clk(clk),
        .rst(rst),
        .stb(ser2_stb),
        .we(io_we),
        .addr(io_addr[3:2]),
        .data_in(io_dout[7:0]),
        .data_out(ser2_dout[7:0]),
        .ack(ser2_ack),
        .irq_r(ser2_irq_r),
        .irq_t(ser2_irq_t)
    );

endmodule

