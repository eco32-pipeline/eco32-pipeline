//
// rom.v -- parallel flash ROM interface
//          16K x 32 bit = 64 KB
//


`timescale 1ns/1ns


module rom(clk, rst,
           stb, we, addr,
           data_out, ack);
    input clk;
    input rst;
    input stb;
    input we;
    input [15:2] addr;
    output reg [31:0] data_out;
    output ack;

  reg [31:0] mem[0:16383];
  reg rd_done;

  initial begin
    #0  $readmemh("rom.dat", mem);
  end

  always @(posedge clk) begin
    if (stb) begin
      if (~we) begin
        // read cycle
        data_out <= mem[addr];
      end
    end
  end

  always @(posedge clk) begin
    if (rst) begin
      rd_done <= 0;
    end else begin
      if (stb & ~we) begin
        // a read needs two clock cycles
        rd_done <= ~rd_done;
      end
    end
  end

  assign ack = rd_done;

endmodule
