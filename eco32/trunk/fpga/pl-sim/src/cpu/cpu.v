//
// cpu.v -- the ECO32 CPU
//


`timescale 1ns/10ps


module cpu(clock, reset,
            mmu_cm_addr, mmu_cm_dout, mmu_cm_hit,
            mmu_cm_exc_tlb_badaddr, mmu_cm_exc_tlb_kmissed,
            mmu_cm_exc_tlb_umissed, mmu_cm_exc_tlb_invalid,
            mmu_cm_exc_bus_timeout,
            mmu_dm_addr, mmu_dm_we, mmu_dm_size,
            mmu_dm_din, mmu_dm_dout, mmu_dm_hit, mmu_tlb_fnc,
            mmu_dm_exc_tlb_badaddr, mmu_dm_exc_tlb_kmissed,
            mmu_dm_exc_tlb_umissed, mmu_dm_exc_tlb_invalid,
            mmu_dm_exc_tlb_wrtprot, mmu_dm_exc_bus_timeout,
            hold_tf, hold_df, flush_df,
            hold_data_tf, flush_data_tf, flush_data_df,
            sreg_1, sreg_2, sreg_3);
        input               clock;
        input               reset;

        output      [31:0]  mmu_cm_addr;
        input       [31:0]  mmu_cm_dout;
        input               mmu_cm_hit;
        input               mmu_cm_exc_tlb_badaddr;
        input               mmu_cm_exc_tlb_kmissed;
        input               mmu_cm_exc_tlb_umissed;
        input               mmu_cm_exc_tlb_invalid;
        input               mmu_cm_exc_bus_timeout;
        output      [31:0]  mmu_dm_addr;
        output              mmu_dm_we;
        output      [2:0]   mmu_dm_size;
        output      [31:0]  mmu_dm_din;
        input       [31:0]  mmu_dm_dout;
        input               mmu_dm_hit;
        output      [2:0]   mmu_tlb_fnc;
        input               mmu_dm_exc_tlb_badaddr;
        input               mmu_dm_exc_tlb_kmissed;
        input               mmu_dm_exc_tlb_umissed;
        input               mmu_dm_exc_tlb_invalid;
        input               mmu_dm_exc_tlb_wrtprot;
        input               mmu_dm_exc_bus_timeout;
        output              hold_tf;
        output              hold_df;
        output              flush_df;
        output              hold_data_tf;
        output              flush_data_tf;
        output              flush_data_df;
        output      [31:0]  sreg_1;
        output      [31:0]  sreg_2;
        output      [31:0]  sreg_3;

    wire [31:0] g_psw;
    wire [3:0]  g_psw_unused;
    wire        g_psw_v;
    wire        g_psw_umode_c;
    wire        g_psw_umode_p;
    wire        g_psw_umode_o;
    wire        g_psw_int_c;
    wire        g_psw_int_p;
    wire        g_psw_int_o;
    wire [4:0]  g_psw_eid;
    wire [15:0] g_psw_ien;

    wire        g_cm_miss;
    wire        g_dm_miss;
    wire        g_jmp_taken;
    wire        g_mul_active;

    wire        g_pc_hold;
    wire        g_if_hold;
    wire        g_if2_hold;
    wire        g_ex_hold;
    wire        g_id_hold;
    wire        g_mem_hold;
    wire        g_mem2_hold;
    wire        g_wb_hold;

    wire        g_if_flush;
    wire        g_if2_flush;
    wire        g_ex_flush;
    wire        g_id_flush;
    wire        g_mem_flush;
    wire        g_mem2_flush;
    wire        g_wb_flush;

    wire [31:0] g_pc_begin0;
    wire [31:0] g_pc_begin1;
    wire [31:0] g_pc_begin2;

    reg         g_started;

    reg         g_cm1_flushed;
    reg         g_cm2_flushed;

    // PC stage
    wire [31:0] pc_pc_cur;    // current value of the PC
    wire [31:0] pc_pc_inc;    // PC incremented by 4
    wire [31:0] pc_pc_jmp;    // jump target address
    wire [31:0] pc_pc_reg;    // reg value as jump target
    wire [1:0]  pc_pc_sel;    // next PC value selector
    wire [31:0] pc_pc_nxt;    // next value of the PC

    // PC/IF pipeline register
    reg  [31:0] if_pc_cur;    // current value of the PC
    reg  [31:0] if_pc_inc;    // next value of the PC

    // IF stage
    wire [31:0] if_cm_addr;   // code memory address
    wire        if_cm_exc_tlb_badaddr;
    wire        if_cm_exc_tlb_kmissed;
    wire        if_cm_exc_tlb_umissed;
    //--------------------
    wire        if_exc_cur;   // exception was thrown
    wire [4:0]  if_eid_cur;   // exception identifier

    // IF/IF2 pipeline register
    reg  [31:0] if2_pc_cur;   // current value of the PC
    reg  [31:0] if2_pc_inc;   // next value of the PC
    reg         if2_exc;      // exception was thrown
    reg  [4:0]  if2_eid;      // exception identifier

    // IF2 stage
    wire        if2_cm_hit;   // code memory cache hit
    wire        if2_cm_exc_tlb_invalid;
    wire        if2_cm_exc_bus_timeout;
    //--------------------
    wire        if2_exc_cur;  // exception was thrown
    wire [4:0]  if2_eid_cur;  // exception identifier

    // IF/ID pipeline register
    reg  [31:0] id_pc_cur;    // current value of the PC
    reg  [31:0] id_pc_inc;    // PC incremented by 4
    //--------------------
    reg         id_exc;       // exception was thrown
    reg [4:0]   id_eid;       // exception identifier

    // ID stage
    wire [31:0] id_cm_instr;  // the instruction to be executed
    //--------------------
    wire [31:0] id_instr;     // the instruction to be executed
    wire [5:0]  id_opcode;    // opcode of instruction
    wire [4:0]  id_reg1;      // register 1 part of instruction
    wire [4:0]  id_reg2;      // register 2 part of instruction
    wire [4:0]  id_reg3;      // register 3 part of instruction
    wire [25:0] id_imm;       // immediate part of instruction
    wire [4:0]  id_regs_src1; // source register number 1
    wire [4:0]  id_regs_src2; // source register number 2
    wire [4:0]  id_regs_dst;  // destination register number
    wire        id_regs_we;   // register file write enable
    wire [31:0] id_regs_din;  // data to be written to destination
    wire [31:0] id_imm_z16;   // immediate, unsigned
    wire [31:0] id_imm_s16;   // immediate, signed
    wire [31:0] id_imm_s16s2; // immediate, signed, for branches
    wire [31:0] id_imm_s26s2; // immediate, signed, for jumps
    wire [31:0] id_imm_z16s16;// immediate, high 16 bits
    wire [2:0]  id_sregs_dst; // special register destination register
    wire [2:0]  id_r_sregs_src;   // special register source register
    wire [2:0]  id_r_sregs_dst;   // special register destination register
    wire        id_r_sregs_we;    // special register write
    wire [31:0] id_r_sregs_din;   // data to be written to special register
    //--------------------
    wire        id_umode;     // required user mode
    wire [1:0]  id_jmp_typ;   // jump type of instruction
    wire [3:0]  id_brn_typ;   // branch type of instruction
    wire        id_off_sel;   // jump target offset selector
    wire [2:0]  id_op2_sel;   // ALU operand 2 selector
    wire [2:0]  id_alu_fnc;   // ALU function
    wire [1:0]  id_shf_fnc;   // Shift function
    wire [2:0]  id_mul_fnc;   // Mul/Div function
    wire        id_mul_en;    // Mul/Div enable
    wire        id_mul_st;    // Start Mul/Div operation
    wire [2:0]  id_tlb_fnc;   // MMU TLB function
    wire [1:0]  id_dst_sel;   // destination register selector
    wire        id_dm_en;     // data memory enable
    wire        id_dm_we;     // data memory write enable
    wire [2:0]  id_dm_size;   // data memory size
    wire [2:0]  id_wbd_sel;   // write-back data selector
    wire        id_rg_we;     // write-back register write
    wire        id_sregs_we;  // write-back special register write
    wire        id_trap;      // trap instruction flag
    wire        id_rfx;       // rfx instruction flag
    wire        id_ill;       // illegal instruction flag
    //--------------------
    wire        id_exc_cur;
    wire [4:0]  id_eid_cur;

    // ID/EX pipeline register
    wire [31:0] ex_data1;     // data read from source register 1
    wire [31:0] ex_data2;     // data read from source register 2
    //--------------------
    reg  [31:0] ex_pc_cur;    // current value of the PC
    reg  [31:0] ex_pc_inc;    // PC incremented by 4
    reg  [31:0] ex_imm_z16;   // immediate, unsigned
    reg  [31:0] ex_imm_s16;   // immediate, signed
    reg  [31:0] ex_imm_s16s2; // immediate, signed, for branches
    reg  [31:0] ex_imm_s26s2; // immediate, signed, for jumps
    reg  [31:0] ex_imm_z16s16;// immediate, high 16 bits
    reg  [4:0]  ex_reg2;      // register 2 part of instruction
    reg  [4:0]  ex_reg3;      // register 3 part of instruction
    reg         ex_mul_st;    // Start Mul/Div operation
    reg  [2:0]  ex_sregs_dst; // destination special register number
    //--------------------
    reg  [1:0]  ex_jmp_typ;   // jump type of instruction
    reg  [3:0]  ex_brn_typ;   // branch type of instruction
    reg         ex_off_sel;   // jump target offset selector
    reg  [2:0]  ex_op2_sel;   // ALU operand 2 selector
    reg  [2:0]  ex_alu_fnc;   // ALU function
    reg  [1:0]  ex_shf_fnc;   // Shift function
    reg  [2:0]  ex_mul_fnc;   // Mul/Div function
    reg         ex_mul_en;    // Mul/Div enable
    reg  [2:0]  ex_tlb_fnc;   // MMU TLB function
    reg  [1:0]  ex_dst_sel;   // destination register selector
    reg         ex_dm_en;     // data memory enable
    reg         ex_dm_we;     // data memory write enable
    reg  [2:0]  ex_dm_size;   // data memory size
    reg  [2:0]  ex_wbd_sel;   // write-back data selector
    reg         ex_rg_we;     // write-back register write
    reg         ex_sregs_we;  // write-back special register write
    reg         ex_rfx;       // rfx instruction flag
    //--------------------
    reg         ex_exc;       // exception was thrown
    reg  [4:0]  ex_eid;       // exception identifier

    // EX stage
    wire [31:0] ex_sregs_data;// data read from special register
    //--------------------
    wire [31:0] ex_pc_off;    // jump target offset
    wire [31:0] ex_pc_jmp;    // jump target address
    wire [31:0] ex_alu_op1;   // ALU operand 1
    wire [31:0] ex_alu_op2;   // ALU operand 2
    wire [31:0] ex_alu_res;   // ALU result
    wire        ex_alu_equ;   // ALU op1 == op2
    wire        ex_alu_ult;   // ALU op1 < op2, unsigned
    wire        ex_alu_slt;   // ALU op1 < op2, signed
    wire [31:0] ex_shf_res;   // Shift result
    wire [31:0] ex_mul_res;   // Mul/Div result
    wire        ex_mul_done;  // Mul/Div done indicator
    wire        ex_mul_error; // Mul/Div error indicator
    wire [4:0]  ex_regs_dst;  // destination register number
    //--------------------
    wire        ex_exc_cur;
    wire [4:0]  ex_eid_cur;

    // EX/MEM pipeline register
    reg  [31:0] mem_pc_cur;    // current value of the PC
    reg  [31:0] mem_pc_inc;    // PC incremented by 4
    reg  [31:0] mem_pc_jmp;    // jump target address
    reg  [31:0] mem_alu_res;   // ALU result
    reg         mem_alu_equ;   // ALU op1 == op2
    reg         mem_alu_ult;   // ALU op1 < op2, unsigned
    reg         mem_alu_slt;   // ALU op1 < op2, signed
    reg  [31:0] mem_shf_res;   // Shift result
    reg  [31:0] mem_mul_res;   // Mul/Div result
    reg  [2:0]  mem_tlb_fnc;   // MMU TLB functions
    reg  [31:0] mem_data2;     // data read from source register 2
    reg  [4:0]  mem_regs_dst;  // destination register number
    reg  [2:0]  mem_sregs_dst; // destination special register number
    //--------------------
    reg  [1:0]  mem_jmp_typ;   // jump type of instruction
    reg  [3:0]  mem_brn_typ;   // branch type of instruction
    reg         mem_dm_en;     // data memory enable
    reg         mem_dm_we;     // data memory write enable
    reg  [2:0]  mem_dm_size;   // data memory size
    reg  [2:0]  mem_wbd_sel;   // write-back data selector
    reg         mem_rg_we;     // write-back register write
    reg         mem_sregs_we;  // write-back special register write
    reg         mem_rfx;       // rfx instruction flag
    //--------------------
    reg         mem_exc;       // exception was thrown
    reg  [4:0]  mem_eid;       // exception identifier

    // MEM stage
    wire [31:0] mem_dm_addr;   // data memory address
    wire [31:0] mem_dm_din;    // data memory data in
    wire        mem_exc_cur;
    wire [4:0]  mem_eid_cur;
    wire        mem_dm_exc_tlb_badaddr;
    wire        mem_dm_exc_tlb_kmissed;
    wire        mem_dm_exc_tlb_umissed;

    // MEM/MEM2 pipeline register
    reg  [31:0] mem2_pc_cur;   // current value of the PC
    reg  [31:0] mem2_pc_inc;   // PC incremented by 4
    reg  [31:0] mem2_pc_jmp;   // jump target address
    reg  [31:0] mem2_alu_res;  // ALU result
    reg         mem2_alu_equ;  // ALU op1 == op2
    reg         mem2_alu_ult;  // ALU op1 < op2, unsigned
    reg         mem2_alu_slt;  // ALU op1 < op2, signed
    reg  [31:0] mem2_shf_res;  // Shift result
    reg  [31:0] mem2_mul_res;  // Mul/Div result
    reg  [4:0]  mem2_regs_dst; // destination register number
    reg  [2:0]  mem2_sregs_dst;// destination special register number
    //--------------------
    reg  [1:0]  mem2_jmp_typ;  // jump type of instruction
    reg  [3:0]  mem2_brn_typ;  // branch type of instruction
    reg         mem2_dm_en;    // data memory enable
    reg  [2:0]  mem2_wbd_sel;  // write-back data selector
    reg         mem2_rg_we;    // write-back register write
    reg         mem2_sregs_we; // write-back special register write
    reg         mem2_rfx;      // rfx instruction flag
    //--------------------
    reg         mem2_exc;      // exception was thrown
    reg  [4:0]  mem2_eid;      // exception identifier

    // MEM2 stage
    wire        mem2_branch;   // branch taken
    wire [1:0]  mem2_pc_sel;   // next PC value selector
    //--------------------
    wire        mem2_dm_hit;   // data memory cache hit
    wire        mem2_dm_exc_tlb_invalid;
    wire        mem2_dm_exc_tlb_wrtprot;
    wire        mem2_dm_exc_bus_timeout;
    //--------------------
    wire        mem2_exc_cur;
    wire [4:0]  mem2_eid_cur;

    // MEM2/WB pipeline register
    wire [31:0] wb_dm_dout;   // data memory data out
    //--------------------
    reg  [31:0] wb_pc_cur;    // current value of the PC
    reg  [31:0] wb_pc_inc;    // PC incremented by 4
    reg  [31:0] wb_alu_res;   // ALU result
    reg  [31:0] wb_shf_res;   // Shift result
    reg  [31:0] wb_mul_res;   // Mul/Div result
    reg  [4:0]  wb_regs_dst;  // destination register number
    reg  [2:0]  wb_sregs_dst; // destination special register number
    //--------------------
    reg  [2:0]  wb_wbd_sel;   // write-back data selector
    reg         wb_rg_we;     // write-back register write
    reg         wb_sregs_we;  // write-back special register write
    reg         wb_rfx;       // rfx instruction flag
    //--------------------
    reg         wb_exc;       // exception was thrown
    reg  [4:0]  wb_eid;       // exception identifier

    // WB stage
    wire [31:0] wb_data;      // write-back data
    wire        wb_exc_cur;
    wire [4:0]  wb_eid_cur;

    //------------------------------------------------------------

    assign mmu_cm_addr                  = if_cm_addr;
    assign id_cm_instr                  = mmu_cm_dout;
    assign if2_cm_hit                   = mmu_cm_hit;
    assign if_cm_exc_tlb_badaddr        = mmu_cm_exc_tlb_badaddr;
    assign if_cm_exc_tlb_kmissed        = mmu_cm_exc_tlb_kmissed;
    assign if_cm_exc_tlb_umissed        = mmu_cm_exc_tlb_umissed;
    assign if2_cm_exc_tlb_invalid       = mmu_cm_exc_tlb_invalid;
    assign if2_cm_exc_bus_timeout       = mmu_cm_exc_bus_timeout;
    assign mmu_dm_addr                  = mem_dm_addr;
    assign mmu_dm_we                    = mem_dm_we;
    assign mmu_dm_size                  = mem_dm_size;
    assign mmu_dm_din                   = mem_dm_din;
    assign wb_dm_dout                   = mmu_dm_dout;
    assign mem2_dm_hit                  = mmu_dm_hit;
    assign mmu_tlb_fnc                  = mem_tlb_fnc;
    assign mem_dm_exc_tlb_badaddr       = mmu_dm_exc_tlb_badaddr;
    assign mem_dm_exc_tlb_kmissed       = mmu_dm_exc_tlb_kmissed;
    assign mem_dm_exc_tlb_umissed       = mmu_dm_exc_tlb_umissed;
    assign mem2_dm_exc_tlb_invalid      = mmu_dm_exc_tlb_invalid;
    assign mem2_dm_exc_tlb_wrtprot      = mmu_dm_exc_tlb_wrtprot;
    assign mem2_dm_exc_bus_timeout      = mmu_dm_exc_bus_timeout;
    assign hold_tf                      = g_if2_hold;
    assign hold_df                      = g_id_hold;
    assign flush_df                     = if2_cm_hit && (g_id_flush || g_cm2_flushed);
    assign hold_data_tf                 = g_mem2_hold;
    assign flush_data_tf                = !mem_dm_en || g_mem2_flush;
    assign flush_data_df                = g_wb_flush;

    always @(posedge clock) begin
        if (g_if2_hold) begin
            g_cm1_flushed <= g_cm1_flushed || g_if2_flush;
        end else begin
            g_cm1_flushed <= !reset && g_if2_flush;
        end
    end
    always @(posedge clock) begin
        if (g_id_hold) begin
            g_cm2_flushed <= g_cm2_flushed || g_id_flush || g_cm1_flushed;
        end else begin
            g_cm2_flushed <= g_id_flush || g_cm1_flushed;
        end
    end

    assign g_psw_unused  = g_psw[31:28];
    assign g_psw_v       = g_psw[27];
    assign g_psw_umode_c = g_psw[26];
    assign g_psw_umode_p = g_psw[25];
    assign g_psw_umode_o = g_psw[24];
    assign g_psw_int_c   = g_psw[23];
    assign g_psw_int_p   = g_psw[22];
    assign g_psw_int_o   = g_psw[21];
    assign g_psw_eid     = g_psw[20:16];
    assign g_psw_ien     = g_psw[15:0];

    assign g_cm_miss     = !if2_cm_hit;
    assign g_dm_miss     = mem2_dm_en && !mem2_dm_hit;
    assign g_jmp_taken   = mem2_pc_sel != 2'b00;
    assign g_mul_active  = ex_mul_en && (ex_mul_st || !ex_mul_done);

    assign g_pc_hold = g_started
        && (g_cm_miss || g_mul_active || g_dm_miss);

    assign g_if_hold = g_started
        && (g_cm_miss || g_mul_active || g_dm_miss);

    assign g_if2_hold = g_started
        && (g_cm_miss || g_mul_active || g_dm_miss);

    assign g_id_hold = g_started
        && (g_mul_active || g_dm_miss);

    assign g_ex_hold = g_started
        && (g_mul_active || g_dm_miss);

    assign g_mem_hold = g_started
        && (g_dm_miss);

    assign g_mem2_hold = g_started
        && (g_dm_miss);

    assign g_wb_hold = 0;

    assign g_if_flush = reset
        || if_exc_cur || if2_exc_cur || id_exc_cur || ex_exc_cur || mem_exc_cur || mem2_exc_cur || wb_exc_cur
        || g_jmp_taken;

    assign g_if2_flush = reset
        || if_exc_cur || if2_exc_cur || id_exc_cur || ex_exc_cur || mem_exc_cur || mem2_exc_cur || wb_exc_cur
        || g_jmp_taken;

    assign g_id_flush = reset
        || if2_exc_cur || id_exc_cur || ex_exc_cur || mem_exc_cur || mem2_exc_cur || wb_exc_cur
        || g_jmp_taken;

    assign g_ex_flush = reset
        || id_exc_cur || ex_exc_cur || mem_exc_cur || mem2_exc_cur || wb_exc_cur
        || g_jmp_taken;

    assign g_mem_flush = reset
        || ex_exc_cur || mem_exc_cur || mem2_exc_cur || wb_exc_cur
        || g_jmp_taken;

    assign g_mem2_flush = reset
        || mem_exc_cur || mem2_exc_cur || wb_exc_cur
        || g_jmp_taken;

    assign g_wb_flush = reset
        || mem2_exc_cur || wb_exc_cur;

    always @(posedge clock) begin
        if (reset) begin
            g_started   <= 0;
        end else begin
            g_started   <= 1;
        end
    end

    assign g_pc_begin0 = g_psw_v ? 32'hC0000000 : 32'hE0000000;
    assign g_pc_begin1 = g_psw_v ? 32'hC0000004 : 32'hE0000004;
    assign g_pc_begin2 = g_psw_v ? 32'hC0000008 : 32'hE0000008;

    // PC stage
    assign pc_pc_inc = pc_pc_cur + 4;
    assign pc_pc_jmp = mem2_pc_jmp;
    assign pc_pc_reg = mem2_alu_res;
    assign pc_pc_sel = mem2_pc_sel;
    assign pc_pc_nxt =
        reset ? g_pc_begin1 :
        wb_exc_cur ? (wb_rfx ? wb_alu_res : g_pc_begin1) :
        pc_pc_sel == 2'b01 ? pc_pc_jmp :
        pc_pc_sel == 2'b10 ? pc_pc_reg :
        g_if_hold ? pc_pc_cur :
        pc_pc_sel == 2'b00 ? pc_pc_inc :
        32'hxxxxxxxx;

    pc pc_1(
        .clock(clock),
        .pc_nxt(pc_pc_nxt),
        .pc_cur(pc_pc_cur)
    );

    // PC/IF pipeline register
    always @(posedge clock) begin
        if (g_if_hold) begin
        end else if (reset) begin
            if_pc_cur   <= g_pc_begin0;
            if_pc_inc   <= g_pc_begin1;
        end else if (g_if_flush || g_pc_hold) begin
            if_pc_cur   <= pc_pc_cur;
            if_pc_inc   <= 'hx;
        end else begin
            if_pc_cur   <= pc_pc_cur;
            if_pc_inc   <= pc_pc_inc;
        end
    end

    // IF stage
    assign if_cm_addr = if_pc_cur;

    assign if_eid_cur = !g_started ? 0 :
        if_cm_exc_tlb_kmissed ? 'd21 :
        if_cm_exc_tlb_umissed ? 'd21 :
        if_cm_exc_tlb_badaddr ? 'd24 :
        (if_cm_addr[31] == 1 && g_psw_umode_c == 1) ? 'd25 : 0;
    assign if_exc_cur = (if_eid_cur != 0);

    // IF/IF2 pipeline register
    always @(posedge clock) begin
        if (g_if2_hold) begin
        end else if (g_if2_flush || g_if_hold) begin
            if2_pc_cur    <= if_pc_cur;
            if2_pc_inc    <= 'hx;
            if2_eid       <= if_eid_cur;
            if2_exc       <= if_exc_cur;
        end else begin
            if2_pc_cur    <= if_pc_cur;
            if2_pc_inc    <= if_pc_inc;
            if2_eid       <= 'hx;
            if2_exc       <= 0;
        end
    end

    assign if2_eid_cur = !g_started ? 0 :
        if2_exc ? if2_eid :
        if2_cm_exc_bus_timeout ? 'd16 :
        if2_cm_exc_tlb_invalid ? 'd23 : 0;
    assign if2_exc_cur = if2_exc || if2_eid_cur != 0;

    // IF2/ID pipeline register
    always @(posedge clock) begin
        if (g_id_hold) begin
        end else if (g_id_flush || g_if2_hold) begin
            id_pc_cur     <= if2_pc_cur;
            id_pc_inc     <= 'hx;
            id_eid        <= if2_eid_cur;
            id_exc        <= if2_exc_cur;
        end else begin
            id_pc_cur     <= if2_pc_cur;
            id_pc_inc     <= if2_pc_inc;
            id_eid        <= 'hx;
            id_exc        <= 0;
        end
    end

    // ID stage
    assign id_instr         = id_cm_instr;
    assign id_opcode        = id_instr[31:26];
    assign id_reg1          = (id_opcode == 6'h2F ? 5'b11110 : id_instr[25:21]); // rfx
    assign id_reg2          = id_instr[20:16];
    assign id_reg3          = id_instr[15:11];
    assign id_imm           = id_instr[25:0];
    assign id_regs_src1     = id_reg1;
    assign id_regs_src2     = id_reg2;

    assign id_regs_dst      = wb_exc_cur ? 5'b11110     : wb_regs_dst;
    assign id_regs_we       = wb_exc_cur ? 1 : (wb_rg_we && (wb_regs_dst != 0));
    assign id_regs_din      = wb_exc_cur ? wb_pc_cur    : wb_data;

    assign id_sregs_dst     = id_instr[2:0];
    assign id_r_sregs_src   = id_sregs_dst;
    assign id_r_sregs_dst   = wb_exc_cur ? 0            : wb_sregs_dst;
    assign id_r_sregs_we    = wb_exc_cur ? 1            : wb_sregs_we;
    assign id_r_sregs_din   = wb_exc_cur ?
        (wb_rfx ?
            {g_psw_unused, g_psw_v, g_psw_umode_p, g_psw_umode_o, g_psw_umode_o,
             g_psw_int_p, g_psw_int_o, g_psw_int_o, wb_eid_cur, g_psw_ien} :
            {g_psw_unused, g_psw_v, 1'b0, g_psw_umode_c, g_psw_umode_p,
             1'b0, g_psw_int_c, g_psw_int_p, wb_eid_cur, g_psw_ien})
        : wb_data;

    regs regs_1(
        .clock(clock),
        .hold(g_ex_hold),
        .src1(id_regs_src1),
        .src2(id_regs_src2),
        .dst(id_regs_dst),
        .dst_we(id_regs_we),
        .dst_data(id_regs_din),
        .src1_data(ex_data1),
        .src2_data(ex_data2)
    );
    sregs sregs_1(
        .clock(clock),
        .reset(reset),
        .hold(g_ex_hold),
        .src(id_r_sregs_src),
        .dst(id_r_sregs_dst),
        .we(id_r_sregs_we),
        .din(id_r_sregs_din),
        .dout(ex_sregs_data),
        .sreg_0(g_psw),
        .sreg_1(sreg_1),
        .sreg_2(sreg_2),
        .sreg_3(sreg_3)
    );
    sign_ext sign_ext_1(
        .din(id_imm),
        .z16(id_imm_z16),
        .s16(id_imm_s16),
        .s16s2(id_imm_s16s2),
        .s26s2(id_imm_s26s2),
        .z16s16(id_imm_z16s16)
    );
    mctrl mctrl_1(
        .opcode(id_opcode),
        .mode(id_umode),
        .jmp_typ(id_jmp_typ),
        .brn_typ(id_brn_typ),
        .off_sel(id_off_sel),
        .op2_sel(id_op2_sel),
        .alu_fnc(id_alu_fnc),
        .shf_fnc(id_shf_fnc),
        .mul_fnc(id_mul_fnc),
        .mul_en(id_mul_en),
        .mul_st(id_mul_st),
        .tlb_fnc(id_tlb_fnc),
        .dst_sel(id_dst_sel),
        .dm_en(id_dm_en),
        .dm_we(id_dm_we),
        .dm_size(id_dm_size),
        .wbd_sel(id_wbd_sel),
        .rg_we(id_rg_we),
        .srg_we(id_sregs_we),
        .trap(id_trap),
        .rfx(id_rfx),
        .ill(id_ill)
    );

    assign id_eid_cur = !g_started ? 0 :
        id_exc ? id_eid :
        id_ill ? 'd17 :
        id_trap ? 'd20 :
        (id_umode == 0 && g_psw_umode_c == 1) ? 'd18 : 0;
    assign id_exc_cur = id_exc || id_rfx || id_eid_cur != 0;

    // ID/EX pipeline register
    always @(posedge clock) begin
        if (g_ex_hold) begin
            ex_mul_st    <= 1'b0;
        end else if (g_ex_flush || g_id_hold) begin
            ex_pc_cur       <= id_pc_cur;
            ex_pc_inc       <= 'hx;
            ex_imm_z16      <= 'hx;
            ex_imm_s16      <= 'hx;
            ex_imm_s16s2    <= 'hx;
            ex_imm_s26s2    <= 'hx;
            ex_imm_z16s16   <= 'hx;
            ex_reg2         <= 'hx;
            ex_reg3         <= 'hx;
            ex_sregs_dst    <= 'hx;
            ex_eid          <= id_eid_cur;
            ex_exc          <= id_exc_cur;
            //--------------------
            ex_jmp_typ      <= 2'b00;
            ex_brn_typ      <= 'hx;
            ex_off_sel      <= 'hx;
            ex_op2_sel      <= 'hx;
            ex_alu_fnc      <= 'hx;
            ex_shf_fnc      <= 'hx;
            ex_mul_fnc      <= 'hx;
            ex_mul_en       <= 0;
            ex_mul_st       <= 'hx;
            ex_tlb_fnc      <= 0;
            ex_dst_sel      <= 'hx;
            ex_dm_size      <= 'hx;
            ex_wbd_sel      <= 'hx;
            ex_dm_en        <= 0;
            ex_dm_we        <= 0;
            ex_rg_we        <= 0;
            ex_sregs_we     <= 0;
            ex_rfx          <= 0;
        end else begin
            ex_pc_cur       <= id_pc_cur;
            ex_pc_inc       <= id_pc_inc;
            ex_imm_z16      <= id_imm_z16;
            ex_imm_s16      <= id_imm_s16;
            ex_imm_s16s2    <= id_imm_s16s2;
            ex_imm_s26s2    <= id_imm_s26s2;
            ex_imm_z16s16   <= id_imm_z16s16;
            ex_reg2         <= id_reg2;
            ex_reg3         <= id_reg3;
            ex_sregs_dst    <= id_sregs_dst;
            ex_eid          <= 'hx;
            ex_exc          <= 0;
            //--------------------
            ex_jmp_typ      <= id_jmp_typ;
            ex_brn_typ      <= id_brn_typ;
            ex_off_sel      <= id_off_sel;
            ex_op2_sel      <= id_op2_sel;
            ex_alu_fnc      <= id_alu_fnc;
            ex_shf_fnc      <= id_shf_fnc;
            ex_mul_fnc      <= id_mul_fnc;
            ex_mul_en       <= id_mul_en;
            ex_mul_st       <= id_mul_st;
            ex_tlb_fnc      <= id_tlb_fnc;
            ex_dst_sel      <= id_dst_sel;
            ex_dm_size      <= id_dm_size;
            ex_wbd_sel      <= id_wbd_sel;
            ex_dm_en        <= id_dm_en;
            ex_dm_we        <= id_dm_we;
            ex_rg_we        <= id_rg_we;
            ex_sregs_we     <= id_sregs_we;
            ex_rfx          <= id_rfx;
        end
    end

    // EX stage
    assign ex_pc_off =
        ex_off_sel == 1'b0 ? ex_imm_s16s2 : ex_imm_s26s2;
    assign ex_pc_jmp = ex_pc_inc + ex_pc_off;
    assign ex_alu_op1 = ex_data1;
    assign ex_alu_op2 =
        ex_op2_sel == 3'b000 ? ex_data2 :
        ex_op2_sel == 3'b001 ? ex_imm_z16 :
        ex_op2_sel == 3'b010 ? ex_imm_s16 :
        ex_op2_sel == 3'b011 ? ex_imm_z16s16 :
        ex_op2_sel == 3'b100 ? ex_sregs_data :
        32'hxxxxxxxx;
    alu alu_1(
        .a(ex_alu_op1),
        .b(ex_alu_op2),
        .fnc(ex_alu_fnc),
        .res(ex_alu_res),
        .equ(ex_alu_equ),
        .ult(ex_alu_ult),
        .slt(ex_alu_slt)
    );
    shift shift_1(
        .data_in(ex_alu_op1),
        .shamt(ex_alu_op2[4:0]),
        .fnc(ex_shf_fnc),
        .data_out(ex_shf_res)
    );
    muldiv muldiv_1(
        .clk(clock),
        .a(ex_alu_op1),
        .b(ex_alu_op2),
        .fnc(ex_mul_fnc),
        .start(ex_mul_st),
        .done(ex_mul_done),
        .error(ex_mul_error),
        .res(ex_mul_res)
    );

    assign ex_regs_dst =
        ex_dst_sel == 2'b00 ? ex_reg2 :
        ex_dst_sel == 2'b01 ? ex_reg3 :
        ex_dst_sel == 2'b10 ? 5'b11110 :
        ex_dst_sel == 2'b11 ? 5'b11111 :
        5'bxxxxx;

    assign ex_eid_cur = !g_started ? 0 :
        ex_exc ? ex_eid :
        (ex_mul_en && !ex_mul_st && ex_mul_error) ? 'd19 : 0;
    assign ex_exc_cur = ex_exc || ex_rfx || ex_eid_cur != 0;

    // EX/MEM pipeline register
    always @(posedge clock) begin
        if (g_mem_hold) begin
        end else if (g_mem_flush || g_ex_hold) begin
            mem_pc_cur      <= ex_pc_cur;
            mem_pc_inc      <= 'hx;
            mem_pc_jmp      <= 'hx;
            mem_alu_res     <= 'hx;
            mem_alu_equ     <= 'hx;
            mem_alu_ult     <= 'hx;
            mem_alu_slt     <= 'hx;
            mem_shf_res     <= 'hx;
            mem_mul_res     <= 'hx;
            mem_tlb_fnc     <= 0;
            mem_data2       <= 'hx;
            mem_regs_dst    <= 'hx;
            mem_sregs_dst   <= 'hx;
            mem_eid         <= ex_eid_cur;
            mem_exc         <= ex_exc_cur;
            //--------------------
            mem_jmp_typ     <= 2'b00;
            mem_brn_typ     <= 'hx;
            mem_dm_size     <= 'hx;
            mem_wbd_sel     <= 'hx;
            mem_dm_en       <= 0;
            mem_dm_we       <= 0;
            mem_rg_we       <= 0;
            mem_sregs_we    <= 0;
            mem_rfx         <= 0;
        end else begin
            mem_pc_cur      <= ex_pc_cur;
            mem_pc_inc      <= ex_pc_inc;
            mem_pc_jmp      <= ex_pc_jmp;
            mem_alu_res     <= ex_alu_res;
            mem_alu_equ     <= ex_alu_equ;
            mem_alu_ult     <= ex_alu_ult;
            mem_alu_slt     <= ex_alu_slt;
            mem_shf_res     <= ex_shf_res;
            mem_mul_res     <= ex_mul_res;
            mem_tlb_fnc     <= ex_tlb_fnc;
            mem_data2       <= ex_data2;
            mem_regs_dst    <= ex_regs_dst;
            mem_sregs_dst   <= ex_sregs_dst;
            mem_eid         <= 'hx;
            mem_exc         <= 0;
            //--------------------
            mem_jmp_typ     <= ex_jmp_typ;
            mem_brn_typ     <= ex_brn_typ;
            mem_dm_size     <= ex_dm_size;
            mem_wbd_sel     <= ex_wbd_sel;
            mem_dm_en       <= ex_dm_en;
            mem_dm_we       <= ex_dm_we;
            mem_rg_we       <= ex_rg_we;
            mem_sregs_we    <= ex_sregs_we;
            mem_rfx         <= ex_rfx;
        end
    end

    assign mem_dm_addr = mem_alu_res;
    assign mem_dm_din = mem_data2;

    assign mem_eid_cur = !g_started ? 0 :
        mem_exc ? mem_eid :
        !mem_dm_en ? 0 :
        mem_dm_exc_tlb_kmissed ? 'd21 :
        mem_dm_exc_tlb_umissed ? 'd21 :
        mem_dm_exc_tlb_badaddr ? 'd24 :
        (mem_dm_addr[31] == 1 && g_psw_umode_c == 1) ? 'd25 : 0;
    assign mem_exc_cur = mem_exc || mem_rfx || mem_eid_cur != 0;

    // MEM/MEM2 pipeline register
    always @(posedge clock) begin
        if (g_mem2_hold) begin
        end else if (g_mem2_flush || g_mem_hold) begin
            mem2_pc_cur     <= mem_pc_cur;
            mem2_pc_inc     <= 'hx;
            mem2_pc_jmp     <= 'hx;
            mem2_alu_res    <= 'hx;
            mem2_alu_equ    <= 'hx;
            mem2_alu_ult    <= 'hx;
            mem2_alu_slt    <= 'hx;
            mem2_shf_res    <= 'hx;
            mem2_mul_res    <= 'hx;
            mem2_eid        <= mem_eid_cur;
            mem2_exc        <= mem_exc_cur;
            //--------------------
            mem2_jmp_typ    <= 2'b00;
            mem2_brn_typ    <= 'hx;
            mem2_dm_en      <= 0;
            mem2_regs_dst   <= 'hx;
            mem2_sregs_dst  <= 'hx;
            mem2_wbd_sel    <= 'hx;
            mem2_rg_we      <= 0;
            mem2_sregs_we   <= 0;
            mem2_rfx        <= 0;
        end else begin
            mem2_pc_cur     <= mem_pc_cur;
            mem2_pc_inc     <= mem_pc_inc;
            mem2_pc_jmp     <= mem_pc_jmp;
            mem2_alu_res    <= mem_alu_res;
            mem2_alu_equ    <= mem_alu_equ;
            mem2_alu_ult    <= mem_alu_ult;
            mem2_alu_slt    <= mem_alu_slt;
            mem2_shf_res    <= mem_shf_res;
            mem2_mul_res    <= mem_mul_res;
            mem2_eid        <= 'hx;
            mem2_exc        <= 0;
            //--------------------
            mem2_jmp_typ    <= mem_jmp_typ;
            mem2_brn_typ    <= mem_brn_typ;
            mem2_dm_en      <= mem_dm_en;
            mem2_regs_dst   <= mem_regs_dst;
            mem2_sregs_dst  <= mem_sregs_dst;
            mem2_wbd_sel    <= mem_wbd_sel;
            mem2_rg_we      <= mem_rg_we;
            mem2_sregs_we   <= mem_sregs_we;
            mem2_rfx        <= mem_rfx;
        end
    end

    bctrl bctrl_1(
        .brn_typ(mem2_brn_typ),
        .alu_equ(mem2_alu_equ),
        .alu_ult(mem2_alu_ult),
        .alu_slt(mem2_alu_slt),
        .branch(mem2_branch)
    );
    jctrl jctrl_1(
        .jmp_typ(mem2_jmp_typ),
        .branch(mem2_branch),
        .pc_sel(mem2_pc_sel)
    );

    assign mem2_eid_cur = !g_started ? 0 :
        mem2_exc ? mem2_eid :
        !mem2_dm_en ? 0 :
        mem2_dm_exc_bus_timeout ? 'd16 :
        mem2_dm_exc_tlb_wrtprot ? 'd22 :
        mem2_dm_exc_tlb_invalid ? 'd23 : 0;
    assign mem2_exc_cur = mem2_exc || mem2_rfx || mem2_eid_cur != 0;

    // MEM2/WB pipeline register
    always @(posedge clock) begin
        if (g_wb_hold) begin
        end else if (g_wb_flush || g_mem2_hold) begin
            wb_pc_cur       <= mem2_pc_cur;
            wb_pc_inc       <= 'hx;
            wb_alu_res      <= 'hx;
            wb_shf_res      <= 'hx;
            wb_mul_res      <= 'hx;
            wb_eid          <= mem2_eid_cur;
            wb_exc          <= mem2_exc_cur;
            //--------------------
            wb_regs_dst     <= 'hx;
            wb_sregs_dst    <= 'hx;
            wb_wbd_sel      <= 'hx;
            wb_rg_we        <= 0;
            wb_sregs_we     <= 0;
            wb_rfx          <= 0;
        end else begin
            wb_pc_cur       <= mem2_pc_cur;
            wb_pc_inc       <= mem2_pc_inc;
            wb_alu_res      <= mem2_alu_res;
            wb_shf_res      <= mem2_shf_res;
            wb_mul_res      <= mem2_mul_res;
            wb_eid          <= 'hx;
            wb_exc          <= 0;
            //--------------------
            wb_regs_dst     <= mem2_regs_dst;
            wb_sregs_dst    <= mem2_sregs_dst;
            wb_wbd_sel      <= mem2_wbd_sel;
            wb_rg_we        <= mem2_rg_we;
            wb_sregs_we     <= mem2_sregs_we;
            wb_rfx          <= mem2_rfx;
        end
    end

    // WB stage
    assign wb_data =
        wb_wbd_sel == 3'b000 ? wb_alu_res :
        wb_wbd_sel == 3'b001 ? wb_dm_dout :
        wb_wbd_sel == 3'b010 ? wb_pc_cur :
        wb_wbd_sel == 3'b011 ? wb_pc_inc :
        wb_wbd_sel == 3'b100 ? wb_shf_res :
        wb_wbd_sel == 3'b101 ? wb_mul_res :
        32'hxxxxxxxx;

    assign wb_eid_cur = !g_started ? 0 :
        wb_exc ? wb_eid : 0;
    assign wb_exc_cur = wb_exc || wb_rfx || wb_eid_cur != 0;

endmodule


//--------------------------------------------------------------
// pc -- the program counter
//--------------------------------------------------------------


module pc(clock, pc_nxt, pc_cur);
        input clock;
        input [31:0] pc_nxt;
        output [31:0] pc_cur;

    reg [31:0] pc_val;

    always @(posedge clock) begin
        pc_val <= pc_nxt;
    end

    assign pc_cur = pc_val;

endmodule


//--------------------------------------------------------------
// sregs -- the special registers
//--------------------------------------------------------------


module sregs(clock, reset, hold,
            src, dst, we, din, dout,
            sreg_0, sreg_1, sreg_2, sreg_3, sreg_4, sreg_5);
        input clock;
        input reset;
        input hold;
        input [2:0] src;
        input [2:0] dst;
        input we;
        input [31:0] din;
        output reg [31:0] dout;
        output [31:0] sreg_0;
        output [31:0] sreg_1;
        output [31:0] sreg_2;
        output [31:0] sreg_3;
        output [31:0] sreg_4;
        output [31:0] sreg_5;

    reg [31:0] r[0:5];

    assign sreg_0 = r[0];
    assign sreg_1 = r[1];
    assign sreg_2 = r[2];
    assign sreg_3 = r[3];
    assign sreg_4 = r[4];
    assign sreg_5 = r[5];

    always @(posedge clock) begin

        if (reset) begin
            r[0] <= 0;
            r[1] <= 0;
            r[2] <= 0;
            r[3] <= 0;
            r[4] <= 0;
            r[5] <= 0;
        end begin
            if (we) begin
                r[dst] <= din;
            end

            if (!hold) begin
                if (we && dst == src) begin
                    dout <= din;
                end else begin
                    dout <= r[src];
                end
            end
        end
    end
endmodule

//--------------------------------------------------------------
// regs -- the register file
//--------------------------------------------------------------


module regs(clock, hold,
            src1, src2, dst,
            dst_we, dst_data,
            src1_data, src2_data);
        input clock;
        input hold;
        input [4:0] src1;
        input [4:0] src2;
        input [4:0] dst;
        input [31:0] dst_data;
        input dst_we;
        output [31:0] src1_data;
        output [31:0] src2_data;

    reg [31:0] r[0:31];

    reg         r_hold;
    reg [4:0]   r_src1;
    reg [4:0]   r_src2;
    reg [4:0]   r_dst;
    reg [31:0]  r_dst_data;
    reg         r_dst_we;
    reg [31:0]  r_out1;
    reg [31:0]  r_out2;

    always @(posedge clock) begin
        r_hold <= hold;

        r_dst       <= dst;
        r_dst_data  <= dst_data;
        r_dst_we    <= dst_we;

        if (dst_we) begin
            r[dst] <= dst_data;
        end

        if (!hold) begin
            r_src1      <= src1;
            r_src2      <= src2;
        end

        r_out1 <= r[hold ? r_src1 : src1];
        r_out2 <= r[hold ? r_src2 : src2];
    end

    assign src1_data = ((!r_hold) && (r_dst_we) && (r_dst == r_src1)) ?
        r_dst_data : r_out1;

    assign src2_data = ((!r_hold) && (r_dst_we) && (r_dst == r_src2)) ?
        r_dst_data : r_out2;

    initial begin
    #0  r[ 0] = 32'h00000000;
        r[ 1] = 32'hFC871E91;
        r[ 2] = 32'h028739D3;
        r[ 3] = 32'hF5922747;
        r[ 4] = 32'hBD07A409;
        r[ 5] = 32'h19138A48;
        r[ 6] = 32'hDC006E46;
        r[ 7] = 32'h4D865DD0;
        r[ 8] = 32'hAD8428D3;
        r[ 9] = 32'h2F6A9A37;
        r[10] = 32'h748E2C46;
        r[11] = 32'hACDF9BF4;
        r[12] = 32'h988B06E3;
        r[13] = 32'h55DF0177;
        r[14] = 32'h5F58C9F6;
        r[15] = 32'h6C607540;
        r[16] = 32'hEDBF4A2A;
        r[17] = 32'hF0AF1276;
        r[18] = 32'h2DFE7E5D;
        r[19] = 32'hBBE2133F;
        r[20] = 32'h1FE6A019;
        r[21] = 32'h0B91E180;
        r[22] = 32'hE7E56431;
        r[23] = 32'hF1414705;
        r[24] = 32'h7EC1FDA0;
        r[25] = 32'h84275A73;
        r[26] = 32'h2698ACAA;
        r[27] = 32'hDEB25B8D;
        r[28] = 32'hA4EA19AA;
        r[29] = 32'hDC137979;
        r[30] = 32'hF029CC3A;
        r[31] = 32'hBFD1A5E5;
    end

endmodule


//--------------------------------------------------------------
// sign_ext -- the sign extender
//--------------------------------------------------------------


module sign_ext(din, z16, s16, s16s2, s26s2, z16s16);
        input [25:0] din;      // 26 bits data input
        output [31:0] z16;     // zero-extend 16 bits
        output [31:0] s16;     // sign-extend 16 bits
        output [31:0] s16s2;   // sign-extend 16 bits, shift left 2
        output [31:0] s26s2;   // sign-extend 26 bits, shift left 2
        output [31:0] z16s16;  // zero-extend 16 bits, shift left 16

    wire [15:0] copy_sign_16;
    wire [3:0] copy_sign_26;

    assign copy_sign_16 =
    (din[15] == 1'b1) ? 16'hFFFF : 16'h0000;
    assign copy_sign_26 =
    (din[25] == 1'b1) ? 4'hF : 4'h0;

    assign z16 = { 16'h0000, din[15:0] };
    assign s16 = { copy_sign_16[15:0], din[15:0] };
    assign s16s2 = { copy_sign_16[13:0], din[15:0], 2'b00 };
    assign s26s2 = { copy_sign_26[3:0], din[25:0], 2'b00 };
    assign z16s16 = { din[15:0], 16'h0000 };

endmodule


//--------------------------------------------------------------
// bctrl -- the branch controller
//--------------------------------------------------------------


module bctrl(brn_typ, alu_equ, alu_ult, alu_slt, branch);
        input [3:0] brn_typ;
        input alu_equ;
        input alu_ult;
        input alu_slt;
        output branch;

    // brn_typ[3] : "negated"
    // brn_typ[2] : "signed"
    // brn_typ[1] : "less"
    // brn_typ[0] : "equal"

    reg brn;

    always @(*) begin
        case (brn_typ[2:0])
            3'b000:   // not used
                brn = 1'bx;
            3'b001:   // unsigned, equal
                brn = alu_equ;
            3'b010:   // unsigned, less than
                brn = alu_ult;
            3'b011:   // unsigned, less equal
                brn = alu_ult | alu_equ;
            3'b100:   // not used
                brn = 1'bx;
            3'b101:   // signed, equal
                brn = alu_equ;
            3'b110:   // signed, less than
                brn = alu_slt;
            3'b111:   // signed, less equal
                brn = alu_slt | alu_equ;
            default:  // not used
                brn = 1'bx;
        endcase
    end

    assign branch = brn ^ brn_typ[3];

endmodule


//--------------------------------------------------------------
// jctrl -- the jump controller
//--------------------------------------------------------------


module jctrl(jmp_typ, branch, pc_sel);
        input [1:0] jmp_typ;
        input branch;
        output [1:0] pc_sel;

    assign pc_sel[1] = jmp_typ[1] & ~jmp_typ[0];
    assign pc_sel[0] = (~jmp_typ[1] & jmp_typ[0]) |
                       (jmp_typ[0] & branch);

endmodule
