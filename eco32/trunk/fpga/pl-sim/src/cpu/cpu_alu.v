//
// cpu_misc.v -- the ECO32 CPU misc
//


`timescale 1ns/10ps


//--------------------------------------------------------------
// alu -- the arithmetic/logic unit
//--------------------------------------------------------------


module alu(a, b, fnc,
           res, equ, ult, slt);
    input  [31:0] a;
    input  [31:0] b;
    input  [2:0]  fnc;
    output [31:0] res;
    output        equ;
    output        ult;
    output        slt;

  wire [32:0] a1;
  wire [32:0] b1;
  reg [32:0] res1;

  assign a1 = { 1'b0, a };
  assign b1 = { 1'b0, b };

  always @(*) begin
    case (fnc)
      3'b000:  res1 = a1 + b1;
      3'b001:  res1 = a1 - b1;
      3'b010:  res1 = a1;
      3'b011:  res1 = b1;
      3'b100:  res1 = a1 & b1;
      3'b101:  res1 = a1 | b1;
      3'b110:  res1 = a1 ^ b1;
      3'b111:  res1 = a1 ~^ b1;
      default: res1 = 33'hxxxxxxxx;
    endcase
  end

  assign res = res1[31:0];
  assign equ = ~| res1[31:0];
  assign ult = res1[32];
  assign slt = res1[32] ^ a[31] ^ b[31];

endmodule


//--------------------------------------------------------------
// shift -- the shift unit
//--------------------------------------------------------------


module shift(data_in, shamt, fnc, data_out);
    input      [31:0] data_in;
    input      [4:0]  shamt;
    input      [1:0]  fnc;
    output     [31:0] data_out;

  assign data_out =
    fnc == 2'b00 ? data_in << shamt :
    fnc == 2'b01 ? data_in >> shamt :
    fnc == 2'b10 ? (data_in[31] == 1 ? ~(32'hFFFFFFFF >> shamt) | (data_in >> shamt) : data_in >> shamt) :
    32'hxxxxxxxx;

endmodule


//--------------------------------------------------------------
// muldiv -- the multiplier/divide unit
//--------------------------------------------------------------


module muldiv(clk, a, b, fnc, start, done, error, res);
    input             clk;
    input      [31:0] a;
    input      [31:0] b;
    input      [2:0]  fnc;
    input             start;
    output reg        done;
    output reg        error;
    output reg [31:0] res;

  // fnc = 000    op = undefined
  //       001         undefined
  //       010         mul
  //       011         mulu
  //       100         div
  //       101         divu
  //       110         rem
  //       111         remu

  reg div;
  reg rem;
  reg [5:0] count;
  reg a_neg;
  reg b_neg;
  reg [31:0] b_abs;
  reg [64:0] q;
  wire [64:1] s;
  wire [64:0] d;

  assign s[64:32] = q[64:32] + { 1'b0, b_abs };
  assign s[31: 1] = q[31: 1];
  assign d[64:32] = q[64:32] - { 1'b0, b_abs };
  assign d[31: 0] = q[31: 0];

  always @(posedge clk) begin
    if (start == 1) begin
      if (fnc[2] == 1 && (| b[31:0]) == 0) begin
        // division by zero
        done <= 1;
        error <= 1;
      end else begin
        // operands are ok
        done <= 0;
        error <= 0;
      end
      div <= fnc[2];
      rem <= fnc[1];
      count <= 6'd0;
      if (fnc[0] == 0 && a[31] == 1) begin
        // negate first operand
        a_neg <= 1;
        if (fnc[2] == 0) begin
          // initialize q for multiplication
          q[64:32] <= 33'b0;
          q[31: 0] <= ~a + 1;
        end else begin
          // initialize q for division and remainder
          q[64:33] <= 32'b0;
          q[32: 1] <= ~a + 1;
          q[ 0: 0] <= 1'b0;
        end
      end else begin
        // use first operand as is
        a_neg <= 0;
        if (fnc[2] == 0) begin
          // initialize q for multiplication
          q[64:32] <= 33'b0;
          q[31: 0] <= a;
        end else begin
          // initialize q for division and remainder
          q[64:33] <= 32'b0;
          q[32: 1] <= a;
          q[ 0: 0] <= 1'b0;
        end
      end
      if (fnc[0] == 0 && b[31] == 1) begin
        // negate second operand
        b_neg <= 1;
        b_abs <= ~b + 1;
      end else begin
        // use second operand as is
        b_neg <= 0;
        b_abs <= b;
      end
    end else begin
      if (done == 0) begin
        // algorithm not yet finished
        if (div == 0) begin
          //
          // multiplication
          //
          if (count == 6'd32) begin
            // last step
            done <= 1;
            if (a_neg == b_neg) begin
              res <= q[31:0];
            end else begin
              res <= ~q[31:0] + 1;
            end
          end else begin
            // all other steps
            count <= count + 1;
            if (q[0] == 1) begin
              q <= { 1'b0, s[64:1] };
            end else begin
              q <= { 1'b0, q[64:1] };
            end
          end
        end else begin
          //
          // division and remainder
          //
          if (count == 6'd32) begin
            // last step
            done <= 1;
            if (rem == 0) begin
              // result <= quotient
              if (a_neg == b_neg) begin
                res <= q[31:0];
              end else begin
                res <= ~q[31:0] + 1;
              end
            end else begin
              // result <= remainder
              if (a_neg == 0) begin
                res <= q[64:33];
              end else begin
                res <= ~q[64:33] + 1;
              end
            end
          end else begin
            // all other steps
            count <= count + 1;
            if (d[64] == 0) begin
              q <= { d[63:0], 1'b1 };
            end else begin
              q <= { q[63:0], 1'b0 };
            end
          end
        end
      end
    end
  end

endmodule
