//
// cpu_mctrl.v
//


`timescale 1ns/10ps


//--------------------------------------------------------------
// mctrl -- the main control signal generator
//--------------------------------------------------------------

`define MODE_IGN        1'bx
`define MODE_K          1'b0
`define MODE_U          1'b1

`define JMP_IGN         2'bxx
`define JMP_NONE        2'b00
`define JMP_IMM         2'b01
`define JMP_REG         2'b10
`define JMP_CMP         2'b11

`define BRN_IGN         4'bxxxx
`define BRN_EQ          4'b0001
`define BRN_NE          4'b1001
`define BRN_LE          4'b0111
`define BRN_LT          4'b0110
`define BRN_GE          4'b1110
`define BRN_GT          4'b1111
`define BRN_LEU         4'b0011
`define BRN_LTU         4'b0010
`define BRN_GEU         4'b1010
`define BRN_GTU         4'b1011

`define OFF_IGN         1'bx
`define OFF_16          1'b0
`define OFF_26          1'b1

`define OP2_IGN         3'bxxx
`define OP2_REG         3'b000
`define OP2_IMM_z16     3'b001
`define OP2_IMM_s16     3'b010
`define OP2_IMM_z16s16  3'b011
`define OP2_SREGS       3'b100

`define ALU_IGN         3'bxxx
`define ALU_ADD         3'b000
`define ALU_SUB         3'b001
`define ALU_A           3'b010
`define ALU_B           3'b011
`define ALU_AND         3'b100
`define ALU_OR          3'b101
`define ALU_XOR         3'b110
`define ALU_XNOR        3'b111

`define MUL_IGN         3'bxxx
`define MUL_MUL         3'b010
`define MUL_MULU        3'b011
`define MUL_DIV         3'b100
`define MUL_DIVU        3'b101
`define MUL_REM         3'b110
`define MUL_REMU        3'b111

`define MUL_ENABLE_IGN   1'bx
`define MUL_ENABLE_OFF   1'b0
`define MUL_ENABLE_ON    1'b1

`define MUL_START_IGN    1'bx
`define MUL_START_OFF    1'b0
`define MUL_START_ON     1'b1

`define SHF_IGN         2'bxx
`define SHF_SLL         2'b00
`define SHF_SLR         2'b01
`define SHF_SAR         2'b10

`define TLB_IGN         3'bxxx
`define TLB_NOOP        3'b000
`define TLB_MAP         3'b001
`define TLB_TBS         3'b010
`define TLB_TBWR        3'b011
`define TLB_TBRI        3'b100
`define TLB_TBWI        3'b101

`define DST_IGN         2'bxx
`define DST_IR2         2'b00
`define DST_IR3         2'b01
`define DST_R30         2'b10
`define DST_R31         2'b11

`define DM_IGN          1'bx
`define DM_OFF          1'b0
`define DM_ON           1'b1

`define DM_SIZE_IGN     3'bxxx
`define DM_SIZE_8       3'b000
`define DM_SIZE_16      3'b001
`define DM_SIZE_32      3'b010
`define DM_SIZE_s8      3'b100
`define DM_SIZE_s16     3'b101

`define WBD_IGN         3'bxxx
`define WBD_ALU         3'b000
`define WBD_DM          3'b001
`define WBD_PC          3'b010
`define WBD_NPC         3'b011
`define WBD_SHF         3'b100
`define WBD_MUL         3'b101

`define RG_IGN          1'bx
`define RG_OFF          1'b0
`define RG_ON           1'b1

`define SRG_IGN         1'bx
`define SRG_OFF         1'b0
`define SRG_ON          1'b1

`define ILL_INVALID     1'b1
`define ILL_VALID       1'b0

`define TRAP_OFF        1'b0
`define TRAP_ON         1'b1

`define RFX_OFF         1'b0
`define RFX_ON          1'b1

module mctrl(opcode,
            mode,
            jmp_typ,
            brn_typ,
            off_sel,
            op2_sel,
            alu_fnc,
            shf_fnc,
            mul_fnc,
            mul_en,
            mul_st,
            tlb_fnc,
            dst_sel,
            dm_en,
            dm_we,
            dm_size,
            wbd_sel,
            rg_we,
            srg_we,
            trap,
            rfx,
            ill);
        input      [5:0] opcode;
        output reg       mode;
        output reg [1:0] jmp_typ;
        output reg [3:0] brn_typ;
        output reg       off_sel;
        output reg [2:0] op2_sel;
        output reg [2:0] alu_fnc;
        output reg [1:0] shf_fnc;
        output reg [2:0] mul_fnc;
        output reg       mul_en;
        output reg       mul_st;
        output reg [2:0] tlb_fnc;
        output reg [1:0] dst_sel;
        output reg       dm_en;
        output reg       dm_we;
        output reg [2:0] dm_size;
        output reg [2:0] wbd_sel;
        output reg       rg_we;
        output reg       srg_we;
        output reg       trap;
        output reg       rfx;
        output reg       ill;

    always @(*) begin
        case (opcode)
            6'h00:  // add
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_ADD;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR3;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_ALU;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h01:  // addi
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_s16;
                    alu_fnc = `ALU_ADD;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_ALU;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h02:  // sub
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_SUB;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR3;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_ALU;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h03:  // subi
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_s16;
                    alu_fnc = `ALU_SUB;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_ALU;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h04:  // mul
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_MUL;
                    mul_en  = `MUL_ENABLE_ON;
                    mul_st  = `MUL_START_ON;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR3;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_MUL;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h05:  // muli
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_s16;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_MUL;
                    mul_en  = `MUL_ENABLE_ON;
                    mul_st  = `MUL_START_ON;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_MUL;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h06:  // mulu
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_MULU;
                    mul_en  = `MUL_ENABLE_ON;
                    mul_st  = `MUL_START_ON;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR3;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_MUL;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h07:  // mului
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_z16;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_MULU;
                    mul_en  = `MUL_ENABLE_ON;
                    mul_st  = `MUL_START_ON;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_MUL;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h08:  // div
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_DIV;
                    mul_en  = `MUL_ENABLE_ON;
                    mul_st  = `MUL_START_ON;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR3;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_MUL;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h09:  // divi
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_s16;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_DIV;
                    mul_en  = `MUL_ENABLE_ON;
                    mul_st  = `MUL_START_ON;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_MUL;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h0A:  // divu
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_DIVU;
                    mul_en  = `MUL_ENABLE_ON;
                    mul_st  = `MUL_START_ON;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR3;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_MUL;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h0B:  // divui
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_z16;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_DIVU;
                    mul_en  = `MUL_ENABLE_ON;
                    mul_st  = `MUL_START_ON;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_MUL;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h0C:  // rem
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_REM;
                    mul_en  = `MUL_ENABLE_ON;
                    mul_st  = `MUL_START_ON;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR3;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_MUL;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h0D:  // remi
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_s16;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_REM;
                    mul_en  = `MUL_ENABLE_ON;
                    mul_st  = `MUL_START_ON;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_MUL;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h0E:  // remu
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_REMU;
                    mul_en  = `MUL_ENABLE_ON;
                    mul_st  = `MUL_START_ON;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR3;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_MUL;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h0F:  // remui
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_z16;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_REMU;
                    mul_en  = `MUL_ENABLE_ON;
                    mul_st  = `MUL_START_ON;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_MUL;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h10:  // and
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_AND;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR3;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_ALU;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h11:  // andi
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_z16;
                    alu_fnc = `ALU_AND;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_ALU;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h12:  // or
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_OR;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR3;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_ALU;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h13:  // ori
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_z16;
                    alu_fnc = `ALU_OR;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_ALU;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h14:  // xor
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_XOR;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR3;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_ALU;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h15:  // xori
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_z16;
                    alu_fnc = `ALU_XOR;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_ALU;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h16:  // xnor
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_XNOR;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR3;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_ALU;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h17:  // xnori
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_z16;
                    alu_fnc = `ALU_XNOR;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_ALU;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h18:  // sll
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_SLL;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR3;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_SHF;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h19:  // slli
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_z16;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_SLL;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_SHF;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h1A:  // slr
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_SLR;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR3;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_SHF;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h1B:  // slri
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_z16;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_SLR;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_SHF;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h1C:  // sar
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_SAR;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR3;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_SHF;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h1D:  // sari
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_z16;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_SAR;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_SHF;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h1F:  // ldhi
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_z16s16;
                    alu_fnc = `ALU_B;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_ALU;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h20:  // beq
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_CMP;
                    brn_typ = `BRN_EQ;
                    off_sel = `OFF_16;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_SUB;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h21:  // bne
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_CMP;
                    brn_typ = `BRN_NE;
                    off_sel = `OFF_16;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_SUB;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h22:  // ble
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_CMP;
                    brn_typ = `BRN_LE;
                    off_sel = `OFF_16;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_SUB;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h23:  // bleu
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_CMP;
                    brn_typ = `BRN_LEU;
                    off_sel = `OFF_16;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_SUB;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h24:  // blt
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_CMP;
                    brn_typ = `BRN_LT;
                    off_sel = `OFF_16;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_SUB;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h25:  // bltu
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_CMP;
                    brn_typ = `BRN_LTU;
                    off_sel = `OFF_16;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_SUB;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h26:  // bge
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_CMP;
                    brn_typ = `BRN_GE;
                    off_sel = `OFF_16;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_SUB;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h27:  // bgeu
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_CMP;
                    brn_typ = `BRN_GEU;
                    off_sel = `OFF_16;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_SUB;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h28:  // bgt
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_CMP;
                    brn_typ = `BRN_GT;
                    off_sel = `OFF_16;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_SUB;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h29:  // bgtu
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_CMP;
                    brn_typ = `BRN_GTU;
                    off_sel = `OFF_16;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_SUB;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h2A:  // j
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_IMM;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_26;
                    op2_sel = `OP2_IGN;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h2B:  // jr
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_REG;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IGN;
                    alu_fnc = `ALU_A;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h2C:  // jal
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_IMM;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_26;
                    op2_sel = `OP2_IGN;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_R31;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_NPC;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h2D:  // jalr
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_REG;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IGN;
                    alu_fnc = `ALU_A;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_R31;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_NPC;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h2E:  // trap
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IGN;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_IGN;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_IGN;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_IGN;
                    srg_we  = `SRG_IGN;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_ON;
                    rfx     = `RFX_OFF;
                end
            6'h2F:  // rfx
                begin
                    mode    = `MODE_K;
                    jmp_typ = `JMP_REG;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IGN;
                    alu_fnc = `ALU_A;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_ON;
                end
            6'h30:  // ldw
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_s16;
                    alu_fnc = `ALU_ADD;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_MAP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_ON;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_32;
                    wbd_sel = `WBD_DM;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h31:  // ldh
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_s16;
                    alu_fnc = `ALU_ADD;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_MAP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_ON;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_s16;
                    wbd_sel = `WBD_DM;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h32:  // ldhu
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_s16;
                    alu_fnc = `ALU_ADD;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_MAP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_ON;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_16;
                    wbd_sel = `WBD_DM;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h33:  // ldb
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_s16;
                    alu_fnc = `ALU_ADD;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_MAP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_ON;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_s8;
                    wbd_sel = `WBD_DM;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h34:  // ldbu
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_s16;
                    alu_fnc = `ALU_ADD;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_MAP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_ON;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_8;
                    wbd_sel = `WBD_DM;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h35:  // stw
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_s16;
                    alu_fnc = `ALU_ADD;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_MAP;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_ON;
                    dm_we   = `DM_ON;
                    dm_size = `DM_SIZE_32;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h36:  // sth
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_s16;
                    alu_fnc = `ALU_ADD;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_MAP;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_ON;
                    dm_we   = `DM_ON;
                    dm_size = `DM_SIZE_16;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h37:  // stb
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IMM_s16;
                    alu_fnc = `ALU_ADD;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_MAP;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_ON;
                    dm_we   = `DM_ON;
                    dm_size = `DM_SIZE_8;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h38:  // mvfs
                begin
                    mode    = `MODE_K;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_SREGS;
                    alu_fnc = `ALU_B;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR2;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_ALU;
                    rg_we   = `RG_ON;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h39:  // mvts
                begin
                    mode    = `MODE_K;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_REG;
                    alu_fnc = `ALU_B;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_NOOP;
                    dst_sel = `DST_IR3;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_ALU;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_ON;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h3A:  // tbs
                begin
                    mode    = `MODE_K;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IGN;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_TBS;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h3B:  // tbwr
                begin
                    mode    = `MODE_K;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IGN;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_TBWR;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h3C:  // tbri
                begin
                    mode    = `MODE_K;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IGN;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_TBRI;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            6'h3D:  // tbwi
                begin
                    mode    = `MODE_K;
                    jmp_typ = `JMP_NONE;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IGN;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_TBWI;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_OFF;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_OFF;
                    srg_we  = `SRG_OFF;
                    ill     = `ILL_VALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
            default:
                begin
                    mode    = `MODE_U;
                    jmp_typ = `JMP_IGN;
                    brn_typ = `BRN_IGN;
                    off_sel = `OFF_IGN;
                    op2_sel = `OP2_IGN;
                    alu_fnc = `ALU_IGN;
                    shf_fnc = `SHF_IGN;
                    mul_fnc = `MUL_IGN;
                    mul_en  = `MUL_ENABLE_OFF;
                    mul_st  = `MUL_START_IGN;
                    tlb_fnc = `TLB_IGN;
                    dst_sel = `DST_IGN;
                    dm_en   = `DM_OFF;
                    dm_we   = `DM_IGN;
                    dm_size = `DM_SIZE_IGN;
                    wbd_sel = `WBD_IGN;
                    rg_we   = `RG_IGN;
                    srg_we  = `SRG_IGN;
                    ill     = `ILL_INVALID;
                    trap    = `TRAP_OFF;
                    rfx     = `RFX_OFF;
                end
        endcase
    end
endmodule
