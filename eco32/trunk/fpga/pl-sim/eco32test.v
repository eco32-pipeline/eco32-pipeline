//
// eco32test.v -- test bench for ECO32
//


`include "src/eco32/eco32.v"
`include "src/clk_rst/clk_rst.v"
`include "src/cpu/cpu.v"
`include "src/cpu/cpu_mctrl.v"
`include "src/cpu/cpu_alu.v"
`include "src/mmu/mmu.v"
`include "src/mmu/data_cache.v"
`include "src/mmu/instr_cache.v"
`include "src/mmu/tlb.v"
`include "src/ramctrl/ramctrl.v"
`include "src/ioctrl/ioctrl.v"
`include "src/kbd/kbd.v"
`include "src/ser/ser.v"
`include "src/tmr/tmr.v"
`include "src/rom/rom.v"
`include "src/dsp/dsp.v"


`timescale 1ns/1ns


module eco32test;

  reg clk_in;			// clock, input, 50 MHz
  reg rst_in_n;			// reset, input, active low

  // simulation control
  initial begin
    #0          $dumpfile("dump.vcd");
                $dumpvars(0, eco32test);
                clk_in = 1;
                rst_in_n = 0;
    #145        rst_in_n = 1;
    #310000     $finish;
  end

// !!!!! HG
//    initial begin
//        #0    $readmemh("ram.dat", eco32_1.ramctrl_1.ram_1.mem);
//    end

  // clock generator
  always begin
    #10 clk_in = ~clk_in;	// 20 nsec cycle time
  end

  // create an instance of ECO32
  eco32 eco32_1(
    .clk_in(clk_in),
    .rst_in_n(rst_in_n)
  );

endmodule
