/*
 * load-icache.c -- generate icache load files from binary
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>


typedef unsigned int Word;


void error(char *fmt, ...) {
  va_list ap;

  va_start(ap, fmt);
  printf("error: ");
  vprintf(fmt, ap);
  printf("\n");
  va_end(ap);
  exit(1);
}


int main(int argc, char *argv[]) {
  FILE *binFile;
  Word word;
  Word addr;
  char *endptr;
  FILE *mem_0;
  FILE *mem_1;
  FILE *mem_2;
  FILE *mem_3;
  FILE *tag;
  int i;

  if (argc != 3) {
    printf("usage: %s <binary file> <load address>\n", argv[0]);
    return 1;
  }
  binFile = fopen(argv[1], "r");
  if (binFile == NULL) {
    error("cannot open binary file '%s'", argv[1]);
  }
  addr = strtoul(argv[2], &endptr, 16);
  if (*endptr != '\0') {
    error("cannot read load address");
  }
  mem_0 = fopen("mem_0.dat", "w");
  if (mem_0 == NULL) {
    error("cannot open output file 'mem_0.dat'");
  }
  mem_1 = fopen("mem_1.dat", "w");
  if (mem_1 == NULL) {
    error("cannot open output file 'mem_1.dat'");
  }
  mem_2 = fopen("mem_2.dat", "w");
  if (mem_2 == NULL) {
    error("cannot open output file 'mem_2.dat'");
  }
  mem_3 = fopen("mem_3.dat", "w");
  if (mem_3 == NULL) {
    error("cannot open output file 'mem_3.dat'");
  }
  tag = fopen("tag.dat", "w");
  if (tag == NULL) {
    error("cannot open output file 'tag.dat'");
  }
  i = 0;
  while (1) {
    if (fread(&word, sizeof(Word), 1, binFile) != 1) {
      break;
    }
    if (i >= 2048) {
      error("cache overflow");
    }
    printf("0x%08X\n", word);
    fprintf(mem_0, "%02X\n", (word >> 0) & 0xFF);
    fprintf(mem_1, "%02X\n", (word >> 8) & 0xFF);
    fprintf(mem_2, "%02X\n", (word >> 16) & 0xFF);
    fprintf(mem_3, "%02X\n", (word >> 24) & 0xFF);
    if (i & 1) {
      fprintf(tag, "%04X\n", addr >> 12 | 0x2000);
    }
    i++;
  }
  while (i < 2048) {
    fprintf(mem_0, "%02X\n", 0);
    fprintf(mem_1, "%02X\n", 0);
    fprintf(mem_2, "%02X\n", 0);
    fprintf(mem_3, "%02X\n", 0);
    if (i & 1) {
      fprintf(tag, "%04X\n", 0);
    }
    i++;
  }
  fclose(binFile);
  fclose(mem_0);
  fclose(mem_1);
  fclose(mem_2);
  fclose(mem_3);
  fclose(tag);
  return 0;
}
