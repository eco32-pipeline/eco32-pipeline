#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdarg.h>

void error(char *fmt, ...) {
  va_list ap;

  va_start(ap, fmt);
  printf("error: ");
  vprintf(fmt, ap);
  printf("\n");
  va_end(ap);
  exit(1);
}

int main(int argc, char **argv) {
    FILE *inFile;
    FILE *outFile;
    int padding;
    char *nop = "add\t$0, $0,";

    if (argc != 4) {
        printf("Usage: %s <padding> <inFile> <outFile>", argv[0]);
        return 1;
    }
    padding = strtol(argv[1], NULL, 10);
    inFile = fopen(argv[2], "r");
    if (inFile == NULL) {
        error("cannot open file '%s'", argv[2]);
    }
    outFile = fopen(argv[3], "w");
    if (outFile == NULL) {
        error("cannot open file '%s'", argv[3]);
    }

    char * line = NULL;
    size_t len = 0;
    ssize_t read;
    while ((read = getline(&line, &len, inFile)) != -1) {
            fprintf(outFile, "%s", line);
            if (strstr(line, ";<pad")) {
                int i;
                for (i = 0; i < padding; i++)
                    fprintf(outFile, "%s 0x%x\n", nop, i+0xbc00);
            }
       }

    fclose(inFile);
    fclose(outFile);

    return EXIT_SUCCESS;
}
