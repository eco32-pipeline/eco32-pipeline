#!/usr/bin/env python2

import sys

nop = "add\t$0, $0, $0"

if len(sys.argv) != 4 or sys.argv[1] == "--help":
    print "Usage: ./autopadding.py <padding> <inFile> <outFile>"
    sys.exit(0)

try: 
    pad = int(sys.argv[1])
except ValueError:
    pad = 10

try:
    infile = open(sys.argv[2], "r")
except IOError:
    print "Could not open file " + sys.argv[2]
    sys.exit(1)

try:
    outfile = open(sys.argv[3], "w")
except IOError:
    print "Could not open file " + sys.argv[3]
    sys.exit(1)

for line in infile:
    if ";<pad>" in line:
        outfile.write((nop+"\n")*pad)
    outfile.write(line)

infile.close()
outfile.close()
