//
// cache.v -- cache implementation
//


`timescale 1ns/10ps
`default_nettype none


module cache(clk, rst,
             addr, data, stall,
             mem_stb, mem_addr, mem_dout, mem_ack);
    input clk;
    input rst;
    // pipeline interface
    input [24:0] addr;
    output [31:0] data;
    output stall;
    // memory interface
    output mem_stb;
    output reg [24:2] mem_addr;
    input [31:0] mem_dout;
    input mem_ack;

  reg [31:0] data_mem[0:1023];
  reg [31:0] data_out;
  reg [13:0] tag_mem[0:1023];
  reg [13:0] tag_out;
  wire [9:0] index;
  wire [12:0] tag;
  wire miss;
  reg hold;

  //
  // Note: The MSB of tag_mem is used as 'valid' bit.
  //

  assign index[9:0] = mem_ack ? mem_addr[11:2] : addr[11:2];
  assign tag[12:0] = mem_ack ? mem_addr[24:12] : addr[24:12];

  always @(posedge clk) begin
    if (mem_ack) begin
      data_mem[index] <= mem_dout;
      data_out <= mem_dout;
    end else begin
      data_out <= data_mem[index];
    end
  end

  always @(posedge clk) begin
    if (mem_ack) begin
      tag_mem[index] <= { 1'b1, tag[12:0] };
      tag_out <= { 1'b1, tag[12:0] };
    end else begin
      tag_out <= tag_mem[index];
    end
  end

  assign miss = ~tag_out[13] | (tag_out[12:0] != tag[12:0]);

  assign data[31:0] = data_out[31:0];

  always @(posedge clk) begin
    if (rst) begin
      hold <= 0;
    end else begin
      if (~hold) begin
        if (miss) begin
          hold <= 1;
        end
      end else begin
        if (mem_ack) begin
          hold <= 0;
        end
      end
    end
  end

  assign stall = miss | hold;

  assign mem_stb = stall;

  always @(posedge clk) begin
    if (~stall) begin
      mem_addr[24:2] <= addr[24:2];
    end
  end

  integer i;
  initial begin
    // data easily correlated with address
    // all entries valid, all tags matching
    for (i = 0; i < 1024; i = i + 1) begin
      data_mem[i] = 32'h123AA000 + (i << 2);
      tag_mem[i] = { 1'b1, 13'h0030 };
    end
    // some tags are wrong and produce misses
    tag_mem [10'h008] = { 1'b1, 13'h0031 };
    data_mem[10'h008] = 32'hDEADBEEF;
    tag_mem [10'h010] = { 1'b1, 13'h0031 };
    data_mem[10'h010] = 32'hDEADBEEF;
    tag_mem [10'h012] = { 1'b1, 13'h0031 };
    data_mem[10'h012] = 32'hDEADBEEF;
    tag_mem [10'h018] = { 1'b1, 13'h0031 };
    data_mem[10'h018] = 32'hDEADBEEF;
    tag_mem [10'h019] = { 1'b1, 13'h0031 };
    data_mem[10'h019] = 32'hDEADBEEF;
    // some entries are invalid and produce misses
    tag_mem [10'h028] = { 1'b0, 13'h0030 };
    data_mem[10'h028] = 32'hDEADBEEF;
    tag_mem [10'h030] = { 1'b0, 13'h0030 };
    data_mem[10'h030] = 32'hDEADBEEF;
    tag_mem [10'h032] = { 1'b0, 13'h0030 };
    data_mem[10'h032] = 32'hDEADBEEF;
    tag_mem [10'h038] = { 1'b0, 13'h0030 };
    data_mem[10'h038] = 32'hDEADBEEF;
    tag_mem [10'h039] = { 1'b0, 13'h0030 };
    data_mem[10'h039] = 32'hDEADBEEF;
  end

endmodule
