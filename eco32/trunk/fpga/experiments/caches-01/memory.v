//
// memory.v -- simulate external memory
//             4M x 64 bit = 32 MB
//


`timescale 1ns/10ps
`default_nettype none


`define RD_CYCLES	4'h2		/* # cycles for read, min = 2 */


module memory(clk, rst,
              stb, we, addr,
              data_in, data_out, ack);
    input clk;
    input rst;
    input stb;
    input we;
    input [24:3] addr;
    input [63:0] data_in;
    output reg [63:0] data_out;
    output ack;

  reg [63:0] mem[0:4194303];
  reg [3:0] rd_counter;
  wire rd_done;

  always @(posedge clk) begin
    if (stb) begin
      if (we) begin
        // write cycle
        mem[addr] <= data_in;
      end else begin
        // read cycle
        data_out <= mem[addr];
      end
    end
  end

  always @(posedge clk) begin
    if (rst) begin
      rd_counter[3:0] <= 4'h0;
    end else begin
      if (rd_counter[3:0] == 4'h0) begin
        if (stb & ~we) begin
          // a read needs some clock cycles
          rd_counter[3:0] <= `RD_CYCLES - 1;
        end
      end else begin
        rd_counter[3:0] <= rd_counter[3:0] - 1;
      end
    end
  end

  assign rd_done = (rd_counter[3:0] == 4'h1) ? 1 : 0;
  assign ack = (stb & we) | rd_done;

  initial begin
    mem[22'h006004] = 64'h123AA020_123AA024;
    mem[22'h006008] = 64'h123AA040_123AA044;
    mem[22'h006009] = 64'h123AA048_123AA04C;
    mem[22'h00600C] = 64'h123AA060_123AA064;
    mem[22'h006014] = 64'h123AA0A0_123AA0A4;
    mem[22'h006018] = 64'h123AA0C0_123AA0C4;
    mem[22'h006019] = 64'h123AA0C8_123AA0CC;
    mem[22'h00601C] = 64'h123AA0E0_123AA0E4;
  end

endmodule
