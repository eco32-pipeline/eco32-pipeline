//
// addrsrc.v -- address source
//


`timescale 1ns/10ps
`default_nettype none


module addrsrc(clk, rst, stall, addr);
  input clk;
  input rst;
  input stall;
  output reg [24:0] addr;

  always @(posedge clk) begin
    if (rst) begin
      addr[24:0] <= 25'h0030000;
    end else begin
      if (~stall) begin
        if (addr[24:0] == 25'h0030FFC) begin
          addr[24:0] <= 25'h0030000;
        end else begin
          addr[24:0] <= addr[24:0] + 4;
        end
      end
    end
  end

endmodule
