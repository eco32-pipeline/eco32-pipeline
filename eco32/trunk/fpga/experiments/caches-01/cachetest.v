//
// cachetest.v -- test bench for cache
//


`include "addrsrc.v"
`include "cache.v"
`include "datasnk.v"
`include "memory.v"


`timescale 1ns/10ps
`default_nettype none


module cachetest;

  reg clk;			// system clock
  reg rst_in;			// reset, input
  reg rst;			// system reset

  wire stall;
  wire [24:0] addr;
  wire [31:0] data;
  wire [31:0] sink;

  wire mem_stb;
  wire [24:3] mem_addr;
  wire [63:0] mem_dout;
  wire mem_ack;

  // simulation control
  initial begin
    #0          $timeformat(-9, 1, " ns", 12);
                $dumpfile("dump.vcd");
                $dumpvars(0, cachetest);
                clk = 1;
                rst_in = 1;
    #23         rst_in = 0;
    #1000       $finish;
  end

  // clock generator
  always begin
    #5 clk = ~clk;		// 10 ns cycle time
  end

  // reset synchronizer
  always @(posedge clk) begin
    rst <= rst_in;
  end

  addrsrc addrsrc_1(
    .clk(clk),
    .rst(rst),
    .stall(stall),
    .addr(addr[24:0])
  );

  cache cache_1(
    .clk(clk),
    .rst(rst),
    .addr(addr[24:0]),
    .data(data[31:0]),
    .stall(stall),
    .mem_stb(mem_stb),
    .mem_addr(mem_addr[24:3]),
    .mem_dout(mem_dout[63:0]),
    .mem_ack(mem_ack)
  );

  datasnk datasnk_1(
    .clk(clk),
    .rst(rst),
    .stall(stall),
    .data(data[31:0]),
    .sink(sink[31:0])
  );

  memory memory_1(
    .clk(clk),
    .rst(rst),
    .stb(mem_stb),
    .we(1'b0),
    .addr(mem_addr[24:3]),
    .data_in(64'h0),
    .data_out(mem_dout[63:0]),
    .ack(mem_ack)
  );

endmodule
