//
// datasnk.v -- data sink
//


`timescale 1ns/10ps
`default_nettype none


module datasnk(clk, rst, stall, data, sink);
  input clk;
  input rst;
  input stall;
  input [31:0] data;
  output reg [31:0] sink;

  always @(posedge clk) begin
    if (rst) begin
      sink[31:0] <= 32'h0;
    end else begin
      if (~stall) begin
        sink[31:0] <= data[31:0];
      end
    end
  end

endmodule
