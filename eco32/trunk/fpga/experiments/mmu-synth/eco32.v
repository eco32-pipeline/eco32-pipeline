//
// eco32.v -- ECO32 top-level description
//


`default_nettype none


module eco32(clk_in, rst_inout_n, all_in, all_out
			);


    input  clk_in;
    input  rst_inout_n;
    input  [63:0] all_in;
    output [63:0] all_out;
    
    //in
	wire [31:0] io_data_out;
	wire io_ack;
	wire io_timeout;
	/*data_cache bus wires*/

	wire data_bus_ack;
	wire [63:0]data_bus_din;
	wire data_bus_timeout;

	/*instr_cache bus wires*/

	wire [63:0]instr_bus_din;
	wire instr_bus_ack;
	wire instr_bus_timeout;



	// data_cache
	wire [2:0] data_size;
	wire hold_data_stage_1;
	wire flush_data_stage_1;
	wire flush_data_stage_2;
	wire data_we;
	wire [31:0] data_din;
		/* hold / flush */
    wire hold_instr_stage_1;
    wire hold_instr_stage_2;
    wire flush_instr_stage_1;
    wire flush_instr_stage_2;
	
	// MMU
    wire [2:0] fnc;
	wire [31:0] instr_virt;
	wire [31:0] data_virt;
    wire [31:0] tlb_index;
    wire [31:0] tlb_entry_hi;
    wire [31:0] tlb_entry_lo;
    
    assign io_data_out = all_in[31:0];
	assign io_ack = all_in[8];
	assign io_timeout = all_in[9];
	/*data_cache bus assigns*/

	assign data_bus_ack = all_in[10];
	assign data_bus_din = all_in[63:0];
	assign data_bus_timeout = all_in[11];

	/*instr_cache bus assigns*/

	assign instr_bus_din = all_in[63:0];
	assign instr_bus_ack = all_in[12];
	assign instr_bus_timeout = all_in[13];



	// data_cache
	assign data_size = all_in[2:0];
	assign hold_data_stage_1 = all_in[0];
	assign flush_data_stage_1 = all_in[1];
	assign flush_data_stage_2 = all_in[2];
	assign data_we = all_in[3];
	assign data_din = all_in[31:0];
		/* hold / flush */
    assign hold_instr_stage_1 = all_in[4];
    assign hold_instr_stage_2 = all_in[5];
    assign flush_instr_stage_1 = all_in[6];
    assign flush_instr_stage_2 = all_in[7];
	
	// MMU
    assign fnc = all_in[2:0];
	assign instr_virt = all_in[31:0];
	assign data_virt = all_in[63:32];
    assign tlb_index = all_in[31:0];
    assign tlb_entry_hi = all_in[31:0];
    assign tlb_entry_lo = all_in[31:0];
    
    //out
    
    wire [31:0] tlb_entry_hi_new;
    wire [31:0] tlb_entry_lo_new;
    wire [31:0] tlb_index_new;
	/* Errors */
    wire instr_tlb_kmissed;
    wire instr_tlb_umissed;
    wire instr_tlb_invalid;
    wire instr_tlb_wrtprot;
    wire instr_tlb_badaddr;
    wire instr_bus_timeout_exc;

    wire data_tlb_kmissed;
    wire data_tlb_umissed;
    wire data_tlb_invalid;
    wire data_tlb_wrtprot;
    wire data_tlb_badaddr;
    wire data_bus_timeout_exc;

	/* I/O - Controller */
	wire io_stb;
	wire io_we;
	wire [31:0] io_addr;
	wire [31:0] io_data_in;
	wire data_bus_stb;
	wire data_bus_we;
	wire [25:0]data_bus_addr;
	wire [63:0]data_bus_dout;
	wire [25:0]instr_bus_addr;
	wire instr_bus_stb;
	wire [31:0] data_dout;
	wire instr_hit;
	wire [31:0] instr_dout;
	wire data_hit;

	assign all_out = tlb_entry_hi_new ^
     tlb_entry_lo_new ^
     tlb_index_new ^
     instr_tlb_kmissed ^
     instr_tlb_umissed ^
     instr_tlb_invalid ^
     instr_tlb_wrtprot ^
     instr_tlb_badaddr ^
     instr_bus_timeout_exc ^

     data_tlb_kmissed ^
     data_tlb_umissed ^
     data_tlb_invalid ^
     data_tlb_wrtprot ^
     data_tlb_badaddr ^
     data_bus_timeout_exc ^
	 io_stb ^
	 io_we ^
	 io_addr ^
	 io_data_in ^
	 data_bus_stb ^
	 data_bus_we ^
	 data_bus_addr ^
	 data_bus_dout ^
	 instr_bus_addr ^
	 instr_bus_stb ^
	 data_dout ^
	 instr_hit ^
	 instr_dout ^
	 data_hit;

  //--------------------------------------
  // module instances
  //--------------------------------------

mmu mmu_0(.clk(clk_in),
		.rst(rst_inout_n), 
		.data_size(data_size),
		.data_we(data_we),
		.data_hit(data_hit),
		.data_din(data_din),
		.data_dout(data_dout),
		.instr_hit(instr_hit),
		.instr_dout(instr_dout),
		.fnc(fnc),
		.instr_virt(instr_virt),
		.data_virt(data_virt),
		.tlb_index(tlb_index),
		.tlb_index_new(tlb_index_new),
		.tlb_entry_hi(tlb_entry_hi),
		.tlb_entry_hi_new(tlb_entry_hi_new),
		.tlb_entry_lo(tlb_entry_lo),
		.tlb_entry_lo_new(tlb_entry_lo_new),
		.instr_tlb_kmissed(instr_tlb_kmissed),
		.data_tlb_kmissed(data_tlb_kmissed),
		.instr_tlb_umissed(instr_tlb_umissed),
		.data_tlb_umissed(data_tlb_umissed),
		.instr_tlb_invalid(instr_tlb_invalid),
		.data_tlb_invalid(data_tlb_invalid),
		.instr_tlb_wrtprot(instr_tlb_wrtprot),
		.data_tlb_wrtprot(data_tlb_wrtprot),
		.instr_tlb_badaddr(instr_tlb_badaddr),
		.data_tlb_badaddr(data_tlb_badaddr),
		.instr_bus_timeout_exc(instr_bus_timeout_exc),
		.data_bus_timeout_exc(data_bus_timeout_exc),
		.hold_instr_stage_1(hold_instr_stage_1),
		.flush_instr_stage_1(flush_instr_stage_1),
		.hold_instr_stage_2(hold_instr_stage_2),
		.flush_instr_stage_2(flush_instr_stage_2),
		.hold_data_stage_1(hold_data_stage_1),
		.flush_data_stage_1(flush_data_stage_1),
		.flush_data_stage_2(flush_data_stage_2),
		.io_stb(io_stb),
		.io_we(io_we),
		.io_addr(io_addr),
		.io_data_in(io_data_in),
		.io_data_out(io_data_out),
		.io_ack(io_ack),
		.io_timeout(io_timeout),
		.data_bus_stb(data_bus_stb),
		.data_bus_we(data_bus_we),
		.data_bus_addr(data_bus_addr),
		.data_bus_din(data_bus_din),
		.data_bus_dout(data_bus_dout),
		.data_bus_ack(data_bus_ack),
		.data_bus_timeout(data_bus_timeout),
		.instr_bus_stb(instr_bus_stb),
		.instr_bus_din(instr_bus_din),
		.instr_bus_addr(instr_bus_addr),
		.instr_bus_ack(instr_bus_ack),
		.instr_bus_timeout(instr_bus_timeout)
		);
endmodule
