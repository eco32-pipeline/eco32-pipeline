//
// toplevel.v -- toplevel for test bench
//


`include "pipectrl.v"


`timescale 1ns/10ps
`default_nettype none


module toplevel;

  reg clk;			// system clock
  reg rst_in;			// reset, input
  reg rst;			// system reset

  // simulation control
  initial begin
    #0          $timeformat(-9, 1, " ns", 12);
                $dumpfile("dump.vcd");
                $dumpvars(0, toplevel);
                clk = 1;
                rst_in = 1;
    #53         rst_in = 0;
    #3000       $finish;
  end

  // clock generator
  always begin
    #5 clk = ~clk;		// 10 nsec cycle time
  end

  // reset synchronizer
  always @(posedge clk) begin
    rst <= rst_in;
  end

  pipectrl pipectrl_1(
    .clk(clk),
    .rst(rst)
  );

endmodule
