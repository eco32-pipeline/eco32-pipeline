//
// pipectrl.v -- pipeline control
//


`timescale 1ns/10ps
`default_nettype none


module pipectrl(clk, rst);
    input clk;
    input rst;

  reg inval_0;
  reg [15:0] data_0;
  wire stall_0;
  wire nodat_0;
  wire hold_0;
  wire [15:0] out_0;

  reg inval_1;
  reg [15:0] data_1;
  wire stall_1;
  wire nodat_1;
  wire hold_1;
  wire [15:0] out_1;

  reg inval_2;
  reg [15:0] data_2;
  wire stall_2;
  wire nodat_2;
  wire hold_2;
  wire [15:0] out_2;

  reg inval_3;
  reg [15:0] data_3;
  wire stall_3;
  wire nodat_3;
  wire hold_3;
  wire [15:0] out_3;

  //--------------------------------------------

  always @(posedge clk) begin
    if (rst) begin
      inval_0 <= 0;
      data_0[15:0] <= 8'h00;
    end else begin
      if (~stall_0) begin
        inval_0 <= 0;
        data_0[15:0] <= data_0[15:0] + 1;
      end
    end
  end

  //--------------------------------------------

  assign stall_0 = hold_0 | stall_1;
  assign nodat_0 = inval_0 | hold_0;

  assign hold_0 = 0;
  assign out_0 = nodat_0 ? 16'hDEAD : data_0;

  //--------------------------------------------

  always @(posedge clk) begin
    if (~stall_1) begin
      inval_1 <= nodat_0;
      data_1[15:0] <= out_0[15:0];
    end
  end

  //--------------------------------------------

  assign stall_1 = hold_1 | stall_2;
  assign nodat_1 = inval_1 | hold_1;

  assign hold_1 = 1;
  assign out_1 = nodat_1 ? 16'hDEAD : data_1;

  //--------------------------------------------

  always @(posedge clk) begin
    if (~stall_2) begin
      inval_2 <= nodat_1;
      data_2[15:0] <= out_1[15:0];
    end
  end

  //--------------------------------------------

  assign stall_2 = hold_2 | stall_3;
  assign nodat_2 = inval_2 | hold_2;

  assign hold_2 = 0;
  assign out_2 = nodat_2 ? 16'hDEAD : data_2;

  //--------------------------------------------

  always @(posedge clk) begin
    if (~stall_3) begin
      inval_3 <= nodat_2;
      data_3[15:0] <= out_2[15:0];
    end
  end

  //--------------------------------------------

  assign stall_3 = hold_3;
  assign nodat_3 = inval_3 | hold_3;

  assign hold_3 = 0;
  assign out_3 = nodat_3 ? 16'hDEAD : data_3;

  //--------------------------------------------

endmodule
