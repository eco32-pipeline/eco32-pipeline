//
// eco32.v -- ECO32 top-level description
//


`timescale 1ns/10ps
`default_nettype none


module eco32(clk_in,
             rst_inout_n,
             sdram_clk,
             sdram_fb,
             ssl);
    // clock and reset
    input clk_in;
    inout rst_inout_n;
    // SDRAM
    output sdram_clk;
    input sdram_fb;
    // 7 segment LED output
    output [6:0] ssl;

  // clk_rst
  wire clk_ok;
  wire clk2;
  wire clk;
  wire rst;
  wire init;
  // tlbtest
  wire [19:0] page;
  wire [15:0] frame;
  wire miss;
  wire [4:0] rw_index;
  wire [19:0] r_page;
  wire [15:0] r_frame;
  wire w_enable;
  wire [19:0] w_page;
  wire [15:0] w_frame;
  wire test_ended;
  wire test_error;
  reg [25:0] heartbeat2;
  reg [25:0] heartbeat;

  //
  // module instances
  //

  clk_rst clk_rst_1(
    .clk_in(clk_in),
    .rst_inout_n(rst_inout_n),
    .sdram_clk(sdram_clk),
    .sdram_fb(sdram_fb),
    .clk_ok(clk_ok),
    .clk2(clk2),
    .clk(clk),
    .rst(rst),
    .init(init)
  );

  tlbtest tlbtest_1(
    .clk(clk2),
    .rst(rst),
    .init(init),
    .page(page[19:0]),
    .frame(frame[15:0]),
    .rw_index(rw_index[4:0]),
    .w_enable(w_enable),
    .w_page(w_page[19:0]),
    .w_frame(w_frame[15:0]),
    .test_ended(test_ended),
    .test_error(test_error)
  );

  tlb tlb_1(
    .clk(clk2),
    .page(page[19:0]),
    .miss(miss),
    .found(),
    .frame(frame[15:0]),
    .rw_index(rw_index[4:0]),
    .r_page(r_page[19:0]),
    .r_frame(r_frame[15:0]),
    .w_enable(w_enable),
    .w_page(w_page[19:0]),
    .w_frame(w_frame[15:0])
  );

  always @(posedge clk2) begin
    heartbeat2 <= heartbeat2 + 1;
  end

  always @(posedge clk) begin
    heartbeat <= heartbeat + 1;
  end

  assign ssl[0] = heartbeat2[25];
  assign ssl[1] = clk_ok;
  assign ssl[2] = rst;
  assign ssl[3] = heartbeat[25];
  assign ssl[4] = test_ended;
  assign ssl[5] = test_error;
  assign ssl[6] = ((^r_page[19:0]) ^ (^r_frame[15:0])) |
                  r_frame[1] | r_frame[10] |
                  r_frame[12] | r_frame[15] | miss;

endmodule
