/*
 * mkinit.c -- generate initialization data for TLB
 */


#include <stdio.h>


int main(int argc, char *argv[]) {
  int i;
  int index[] = {
    5, 3, 24, 8, 28, 20, 31, 23, 1, 30, 9, 0, 2, 7, 13, 15
  };
  unsigned int page;
  unsigned int frame;

  for (i = 0; i < 16; i++) {
    page = ((i ^ 0x3) << 16) |
           ((i ^ 0xC) << 12) |
           ((i ^ 0x9) <<  8) |
           ((i ^ 0x6) <<  4) |
           ((i ^ 0x0) <<  0);
    frame = (page & 0xFFFF) ^
            (((page >> 16) & 0xF) << 12);
    printf("      6'd%d:\n", index[i]);
    printf("        begin\n");
    printf("          rw_index = 5'd%d;\n", index[i]);
    printf("          w_enable = 1;\n");
    printf("          w_page = 20'h%05X;\n", page);
    printf("          w_frame = 16'h%04X;\n", frame);
    printf("        end\n");
  }
  return 0;
}
