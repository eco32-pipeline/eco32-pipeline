//
// toplevel.v -- toplevel for test bench
//


`include "tlbtest.v"
`include "tlb.v"


`timescale 1ns/10ps
`default_nettype none


module toplevel;

  reg clk;			// system clock
  reg rst_in;			// reset, input
  reg rst_snc;			// reset, synchronized
  reg [5:0] rst_cnt;		// reset counter
  wire rst;			// system reset
  wire init;			// init pulse, early during rst

  wire [19:0] page;
  wire [15:0] frame;
  wire [4:0] rw_index;
  wire w_enable;
  wire [19:0] w_page;
  wire [15:0] w_frame;

  wire test_ended;
  wire test_error;

  // simulation control
  initial begin
    #0          $timeformat(-9, 1, " ns", 12);
                $dumpfile("dump.vcd");
                $dumpvars(0, toplevel);
                clk = 1;
                rst_in = 1;
    #23         rst_in = 0;
    #1730       $finish;
  end

  // clock generator
  always begin
    #5 clk = ~clk;		// 10 nsec cycle time
  end

  // reset synchronizer
  always @(posedge clk) begin
    rst_snc <= rst_in;
  end

  // reset counter
  always @(posedge clk) begin
    if (rst_snc) begin
      rst_cnt <= 6'd40;
    end else begin
      if (rst_cnt != 6'd0) begin
        rst_cnt <= rst_cnt - 1;
      end
    end
  end

  assign rst = (rst_cnt != 6'h00) ? 1 : 0;
  assign init = (rst_cnt == 6'd37) ? 1 : 0;

  tlbtest tlbtest_1(
    .clk(clk),
    .rst(rst),
    .init(init),
    .page(page[19:0]),
    .frame(frame[15:0]),
    .rw_index(rw_index[4:0]),
    .w_enable(w_enable),
    .w_page(w_page[19:0]),
    .w_frame(w_frame[15:0]),
    .test_ended(test_ended),
    .test_error(test_error)
  );

  tlb tlb_1(
    .clk(clk),
    .page(page[19:0]),
    .miss(),
    .found(),
    .frame(frame[15:0]),
    .rw_index(rw_index[4:0]),
    .r_page(),
    .r_frame(),
    .w_enable(w_enable),
    .w_page(w_page[19:0]),
    .w_frame(w_frame[15:0])
  );

endmodule
