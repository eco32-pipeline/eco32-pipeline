//
// tlb.v -- translation lookaside buffer
//


`timescale 1ns/10ps
`default_nettype none


module tlb(clk,
           page, miss, found, frame,
           rw_index, r_page, r_frame,
           w_enable, w_page, w_frame);
    input clk;
    input [19:0] page;
    output miss;
    output [4:0] found;
    output reg [15:0] frame;
    input [4:0] rw_index;
    output reg [19:0] r_page;
    output reg [15:0] r_frame;
    input w_enable;
    input [19:0] w_page;
    input [15:0] w_frame;

  reg [19:0] page_mem[0:31];
  reg [15:0] frame_mem[0:31];

  wire [19:0] p00, p01, p02, p03;
  wire [19:0] p04, p05, p06, p07;
  wire [19:0] p08, p09, p10, p11;
  wire [19:0] p12, p13, p14, p15;
  wire [19:0] p16, p17, p18, p19;
  wire [19:0] p20, p21, p22, p23;
  wire [19:0] p24, p25, p26, p27;
  wire [19:0] p28, p29, p30, p31;
  wire [31:0] match_comb;
  reg [31:0] match;

  assign p00 = page_mem[ 0];
  assign p01 = page_mem[ 1];
  assign p02 = page_mem[ 2];
  assign p03 = page_mem[ 3];
  assign p04 = page_mem[ 4];
  assign p05 = page_mem[ 5];
  assign p06 = page_mem[ 6];
  assign p07 = page_mem[ 7];
  assign p08 = page_mem[ 8];
  assign p09 = page_mem[ 9];
  assign p10 = page_mem[10];
  assign p11 = page_mem[11];
  assign p12 = page_mem[12];
  assign p13 = page_mem[13];
  assign p14 = page_mem[14];
  assign p15 = page_mem[15];
  assign p16 = page_mem[16];
  assign p17 = page_mem[17];
  assign p18 = page_mem[18];
  assign p19 = page_mem[19];
  assign p20 = page_mem[20];
  assign p21 = page_mem[21];
  assign p22 = page_mem[22];
  assign p23 = page_mem[23];
  assign p24 = page_mem[24];
  assign p25 = page_mem[25];
  assign p26 = page_mem[26];
  assign p27 = page_mem[27];
  assign p28 = page_mem[28];
  assign p29 = page_mem[29];
  assign p30 = page_mem[30];
  assign p31 = page_mem[31];

  assign match_comb[ 0] = (page == p00) ? 1 : 0;
  assign match_comb[ 1] = (page == p01) ? 1 : 0;
  assign match_comb[ 2] = (page == p02) ? 1 : 0;
  assign match_comb[ 3] = (page == p03) ? 1 : 0;
  assign match_comb[ 4] = (page == p04) ? 1 : 0;
  assign match_comb[ 5] = (page == p05) ? 1 : 0;
  assign match_comb[ 6] = (page == p06) ? 1 : 0;
  assign match_comb[ 7] = (page == p07) ? 1 : 0;
  assign match_comb[ 8] = (page == p08) ? 1 : 0;
  assign match_comb[ 9] = (page == p09) ? 1 : 0;
  assign match_comb[10] = (page == p10) ? 1 : 0;
  assign match_comb[11] = (page == p11) ? 1 : 0;
  assign match_comb[12] = (page == p12) ? 1 : 0;
  assign match_comb[13] = (page == p13) ? 1 : 0;
  assign match_comb[14] = (page == p14) ? 1 : 0;
  assign match_comb[15] = (page == p15) ? 1 : 0;
  assign match_comb[16] = (page == p16) ? 1 : 0;
  assign match_comb[17] = (page == p17) ? 1 : 0;
  assign match_comb[18] = (page == p18) ? 1 : 0;
  assign match_comb[19] = (page == p19) ? 1 : 0;
  assign match_comb[20] = (page == p20) ? 1 : 0;
  assign match_comb[21] = (page == p21) ? 1 : 0;
  assign match_comb[22] = (page == p22) ? 1 : 0;
  assign match_comb[23] = (page == p23) ? 1 : 0;
  assign match_comb[24] = (page == p24) ? 1 : 0;
  assign match_comb[25] = (page == p25) ? 1 : 0;
  assign match_comb[26] = (page == p26) ? 1 : 0;
  assign match_comb[27] = (page == p27) ? 1 : 0;
  assign match_comb[28] = (page == p28) ? 1 : 0;
  assign match_comb[29] = (page == p29) ? 1 : 0;
  assign match_comb[30] = (page == p30) ? 1 : 0;
  assign match_comb[31] = (page == p31) ? 1 : 0;

  always @(posedge clk) begin
    match[31:0] <= match_comb[31:0];
  end

  assign miss = ~(| match[31:0]);

  assign found[0] = match[ 1] | match[ 3] | match[ 5] | match[ 7] |
                    match[ 9] | match[11] | match[13] | match[15] |
                    match[17] | match[19] | match[21] | match[23] |
                    match[25] | match[27] | match[29] | match[31];
  assign found[1] = match[ 2] | match[ 3] | match[ 6] | match[ 7] |
                    match[10] | match[11] | match[14] | match[15] |
                    match[18] | match[19] | match[22] | match[23] |
                    match[26] | match[27] | match[30] | match[31];
  assign found[2] = match[ 4] | match[ 5] | match[ 6] | match[ 7] |
                    match[12] | match[13] | match[14] | match[15] |
                    match[20] | match[21] | match[22] | match[23] |
                    match[28] | match[29] | match[30] | match[31];
  assign found[3] = match[ 8] | match[ 9] | match[10] | match[11] |
                    match[12] | match[13] | match[14] | match[15] |
                    match[24] | match[25] | match[26] | match[27] |
                    match[28] | match[29] | match[30] | match[31];
  assign found[4] = match[16] | match[17] | match[18] | match[19] |
                    match[20] | match[21] | match[22] | match[23] |
                    match[24] | match[25] | match[26] | match[27] |
                    match[28] | match[29] | match[30] | match[31];

  always @(posedge clk) begin
    frame <= frame_mem[found];
  end

  always @(posedge clk) begin
    if (w_enable == 1) begin
      page_mem[rw_index] <= w_page;
      frame_mem[rw_index] <= w_frame;
    end else begin
      r_page <= page_mem[rw_index];
      r_frame <= frame_mem[rw_index];
    end
  end

endmodule
