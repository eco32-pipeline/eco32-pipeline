//
// tlbtest.v -- test bench for translation lookaside buffer
//


`timescale 1ns/10ps
`default_nettype none


module tlbtest(clk, rst, init,
               page, frame,
               rw_index, w_enable, w_page, w_frame,
               test_ended, test_error);
    input clk;
    input rst;
    input init;
    output [19:0] page;
    input [15:0] frame;
    output reg [4:0] rw_index;
    output reg w_enable;
    output reg [19:0] w_page;
    output reg [15:0] w_frame;
    output reg test_ended;
    output reg test_error;

  reg [19:0] del_page;
  wire [15:0] ref_frame;
  reg [15:0] del_frame;
  reg [6:0] test_count;

  addrsrc addrsrc_1(
    .clk(clk),
    .rst(rst),
    .page(page[19:0])
  );

  always @(posedge clk) begin
    del_page[19:0] <= page[19:0];
  end

  assign ref_frame[15:0] =
    del_page[15:0] ^ { del_page[19:16], 12'h0 };

  always @(posedge clk) begin
    del_frame[15:0] <= ref_frame[15:0];
  end

  always @(posedge clk) begin
    if (rst) begin
      test_count <= 0;
      test_ended <= 0;
      test_error <= 0;
    end else begin
      if (test_count != 7'h7F) begin
        test_count <= test_count + 1;
      end else begin
        test_ended <= 1;
      end
      if (frame[15:0] != del_frame[15:0]) begin
        test_error <= 1;
      end
    end
  end

  //
  // initialization
  //

  reg [5:0] init_count;

  always @(posedge clk) begin
    if (init) begin
      init_count <= 0;
    end else begin
      if (init_count != 6'h20) begin
        init_count <= init_count + 1;
      end
    end
  end

  always @(*) begin
    case (init_count)
      6'd5:
        begin
          rw_index = 5'd5;
          w_enable = 1;
          w_page = 20'h3C960;
          w_frame = 16'hF960;
        end
      6'd3:
        begin
          rw_index = 5'd3;
          w_enable = 1;
          w_page = 20'h2D871;
          w_frame = 16'hF871;
        end
      6'd24:
        begin
          rw_index = 5'd24;
          w_enable = 1;
          w_page = 20'h1EB42;
          w_frame = 16'hFB42;
        end
      6'd8:
        begin
          rw_index = 5'd8;
          w_enable = 1;
          w_page = 20'h0FA53;
          w_frame = 16'hFA53;
        end
      6'd28:
        begin
          rw_index = 5'd28;
          w_enable = 1;
          w_page = 20'h78D24;
          w_frame = 16'hFD24;
        end
      6'd20:
        begin
          rw_index = 5'd20;
          w_enable = 1;
          w_page = 20'h69C35;
          w_frame = 16'hFC35;
        end
      6'd31:
        begin
          rw_index = 5'd31;
          w_enable = 1;
          w_page = 20'h5AF06;
          w_frame = 16'hFF06;
        end
      6'd23:
        begin
          rw_index = 5'd23;
          w_enable = 1;
          w_page = 20'h4BE17;
          w_frame = 16'hFE17;
        end
      6'd1:
        begin
          rw_index = 5'd1;
          w_enable = 1;
          w_page = 20'hB41E8;
          w_frame = 16'hF1E8;
        end
      6'd30:
        begin
          rw_index = 5'd30;
          w_enable = 1;
          w_page = 20'hA50F9;
          w_frame = 16'hF0F9;
        end
      6'd9:
        begin
          rw_index = 5'd9;
          w_enable = 1;
          w_page = 20'h963CA;
          w_frame = 16'hF3CA;
        end
      6'd0:
        begin
          rw_index = 5'd0;
          w_enable = 1;
          w_page = 20'h872DB;
          w_frame = 16'hF2DB;
        end
      6'd2:
        begin
          rw_index = 5'd2;
          w_enable = 1;
          w_page = 20'hF05AC;
          w_frame = 16'hF5AC;
        end
      6'd7:
        begin
          rw_index = 5'd7;
          w_enable = 1;
          w_page = 20'hE14BD;
          w_frame = 16'hF4BD;
        end
      6'd13:
        begin
          rw_index = 5'd13;
          w_enable = 1;
          w_page = 20'hD278E;
          w_frame = 16'hF78E;
        end
      6'd15:
        begin
          rw_index = 5'd15;
          w_enable = 1;
          w_page = 20'hC369F;
          w_frame = 16'hF69F;
        end
      default:
        begin
          // fill other slots with default values
          // don't write if count == 6'h20
          rw_index = init_count[4:0];
          w_enable = ~init_count[5];
          w_page = 20'hDEAD0;
          w_frame = 16'h0000;
        end
    endcase
  end

endmodule


//
// address generator
//

module addrsrc(clk, rst, page);
  input clk;
  input rst;
  output [19:0] page;

  reg [3:0] add;

  always @(posedge clk) begin
    if (rst) begin
      add[3:0] <= 4'h0;
    end else begin
      add[3:0] <= add[3:0] + 1;
    end
  end

  assign page[19:0] =
    { add[3:0] ^ 4'h3, add[3:0] ^ 4'hC,
      add[3:0] ^ 4'h9, add[3:0] ^ 4'h6,
      add[3:0] };

endmodule
