/*
 * cachesim.c -- cache simulator
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <stdarg.h>


#define PHYS_ADDR_BITS	29


/**************************************************************/

/* boolean data type */


typedef int bool;

#define FALSE		0
#define TRUE		1


/**************************************************************/

/* global variables */


bool debug;


/**************************************************************/

/* error handling */


void error(char *fmt, ...) {
  va_list ap;

  va_start(ap, fmt);
  printf("error: ");
  vprintf(fmt, ap);
  printf("\n");
  va_end(ap);
  exit(1);
}


/**************************************************************/

/* memory allocation */


void *alloc(unsigned int size) {
  void *p;

  p = malloc(size);
  if (p == NULL) {
    error("alloc() got no memory");
  }
  return p;
}


/**************************************************************/

/* compute ld(n) for exact powers of 2 */


int ld(unsigned int n, char *what) {
  int i;

  if (n == 0) {
    error("ld(%s) - argument is 0", what);
  }
  for (i = 0; i < 32; i++) {
    if (n & (1 << i)) {
      break;
    }
  }
  if (n ^ (1 << i)) {
    error("ld(%s) - argument is not a power of 2", what);
  }
  return i;
}


/**************************************************************/

/* cache simulation */


#define CACHE_READ	0
#define CACHE_WRITE	1


typedef struct {
  bool valid;			/* is the line valid? */
  bool dirty;			/* is the line dirty? */
  unsigned int tag;		/* upper address bits */
  int age;			/* relative age of line in its set */
} CacheLine;

typedef CacheLine *CacheSet;	/* an array of cache lines */

typedef struct {
  /*
   * given cache parameters
   */
  int totalSize;		/* total size of the cache in bytes */
  int lineSize;			/* cache line size in bytes */
  int assoc;			/* n for n-way associative cache */
  bool wb;			/* 0: write-through, 1: write-back */
  /*
   * derived cache parameters
   */
  int lines;			/* total number of cache lines */
  int sets;			/* number of cache sets */
  int offsetBits;		/* the offset has this many bits */
  int indexBits;		/* the index has this many bits */
  int tagBits;			/* the tag has this many bits */
  int indexShift;		/* == offset bits */
  unsigned int indexMask;	/* mask for index bits (shifted to 0) */
  int tagShift;			/* == index bits + offset bits */
  unsigned int tagMask;		/* mask for tag bits (shifted to 0) */
  /*
   * cache management data
   */
  CacheSet *data;		/* an array of cache sets */
  /*
   * cache statistics
   */
  int accesses;			/* total number of accesses */
  int misses;			/* how many of these missed */
  int replacements;		/* # cache lines replaced */
  int demandFetches;		/* # cache lines fetched on demand */
  int copiesBack;		/* # cache lines copied back */
} Cache;


void cacheInit(Cache *cp, int s, int l, int a, bool wb) {
  int i;
  int j;

  cp->totalSize = s;
  cp->lineSize = l;
  cp->assoc = a;
  cp->wb = wb;
  printf("geometry:\n");
  printf("    total size      = %d bytes\n", cp->totalSize);
  printf("    line size       = %d bytes\n", cp->lineSize);
  printf("    assoc           = %d-way\n", cp->assoc);
  printf("    write policy    = write-%s\n", cp->wb ? "back" : "through");
  printf("lines and sets:\n");
  cp->lines = cp->totalSize / cp->lineSize;
  printf("    number of lines = %d\n", cp->lines);
  cp->sets = cp->lines / cp->assoc;
  printf("    number of sets  = %d\n", cp->sets);
  printf("address bits:\n");
  cp->offsetBits = ld(cp->lineSize, "line size");
  printf("    offset          = %d bits\n", cp->offsetBits);
  cp->indexBits = ld(cp->sets, "number of sets");
  printf("    index           = %d bits\n", cp->indexBits);
  cp->tagBits = PHYS_ADDR_BITS - cp->indexBits - cp->offsetBits;
  printf("    tag             = %d bits\n", cp->tagBits);
  printf("shifts and masks:\n");
  cp->indexShift = cp->offsetBits;
  printf("    index shift     = %d\n", cp->indexShift);
  cp->indexMask = (1 << cp->indexBits) - 1;
  printf("    index mask      = 0x%08X\n", cp->indexMask);
  cp->tagShift = cp->indexBits + cp->offsetBits;
  printf("    tag shift       = %d\n", cp->tagShift);
  cp->tagMask = (1 << cp->tagBits) - 1;
  printf("    tag mask        = 0x%08X\n", cp->tagMask);
  cp->data = alloc(cp->sets * sizeof(CacheSet));
  for (i = 0; i < cp->sets; i++) {
    cp->data[i] = alloc(cp->assoc * sizeof(CacheLine));
    for (j = 0; j < cp->assoc; j++) {
      cp->data[i][j].valid = FALSE;
      cp->data[i][j].dirty = FALSE;
      cp->data[i][j].tag = 0;
      cp->data[i][j].age = 0;
    }
  }
  cp->accesses = 0;
  cp->misses = 0;
  cp->replacements = 0;
  cp->demandFetches = 0;
  cp->copiesBack = 0;
}


void cacheAccess(Cache *cp, unsigned int addr, int access) {
  int index;
  unsigned int tag;
  int oldestAge;
  int oldestLine;
  int invalidLine;
  int i;
  CacheLine *lp;

  /* compute index and tag */
  cp->accesses++;
  index = (addr >> cp->indexShift) & cp->indexMask;
  tag = (addr >> cp->tagShift) & cp->tagMask;
  /* cache lookup */
  oldestAge = -1;
  invalidLine = cp->assoc;
  for (i = 0; i < cp->assoc; i++) {
    if (cp->data[index][i].valid &&
        cp->data[index][i].tag == tag) {
      break;
    }
    if (cp->data[index][i].valid &&
        cp->data[index][i].age > oldestAge) {
      oldestAge = cp->data[index][i].age;
      oldestLine = i;
    }
    if (!cp->data[index][i].valid) {
      invalidLine = i;
    }
  }
  /*
   * Here one of two possible conditions holds:
   * i < cp->assoc : cache hit
   *     i is the index of the matching cache line
   * i == cp->assoc : cache miss
   *     oldestLine is the index of the oldest valid cache line
   *     oldestAge is the age of this line (or -1, if no line is valid)
   *     invalidLine < cp->assoc : an invalid cache line is available
   *     invalidLine == cp->assoc : no invalid cache line is available
   */
  if (debug) {
    printf("       set = %d: ", index);
    if (i < cp->assoc) {
      printf("hit, line = %d, tag = 0x%08X\n", i, tag);
    } else {
      printf("miss, ");
      if (oldestAge > -1) {
        printf("oldest line = %d (age %d), ", oldestLine, oldestAge);
      } else {
        printf("all lines invalid, ");
      }
      if (invalidLine < cp->assoc) {
        printf("invalid line = %d\n", invalidLine);
      } else {
        printf("no invalid lines\n");
      }
    }
  }
  /* handle different cases */
  if (i < cp->assoc) {
    /* this is a cache hit */
    lp = &cp->data[index][i];
  } else {
    /* this is a cache miss */
    cp->misses++;
    if (invalidLine < cp->assoc) {
      /* there is a free cache line */
      lp = &cp->data[index][invalidLine];
      /* bump age values of all valid cache lines */
      lp->age = oldestAge + 1;
    } else {
      /* no free cache line, replace oldest cache line */
      cp->replacements++;
      lp = &cp->data[index][oldestLine];
      if (lp->dirty) {
        /* write back */
        /* note: dirty is never set for a write-through cache */
        cp->copiesBack++;
      }
    }
    /* fetch the requested cache line */
    cp->demandFetches++;
    lp->valid = TRUE;
    lp->dirty = FALSE;
    lp->tag = tag;
  }
  /* update age values */
  for (i = 0; i < cp->assoc; i++) {
    if (cp->data[index][i].valid &&
        cp->data[index][i].age < lp->age) {
      cp->data[index][i].age++;
    }
  }
  lp->age = 0;
  /* handle write */
  if (access == CACHE_WRITE) {
    if (cp->wb) {
      /* write-back */
      lp->dirty = TRUE;
    } else {
      /* write-through */
      cp->copiesBack++;
      lp->dirty = FALSE;
    }
  }
}


void cacheFlush(Cache *cp) {
  int index;
  int i;

  for (index = 0; index < cp->sets; index++) {
    for (i = 0; i < cp->assoc; i++) {
      if (cp->data[index][i].valid  &&
          cp->data[index][i].dirty) {
        /* write back */
        /* note: dirty is never set for a write-through cache */
        cp->copiesBack++;
      }
    }
  }
}


void cacheShow(Cache *cp) {
  float missRate;

  if (cp->accesses != 0) {
    missRate = (float) cp->misses / (float) cp->accesses;
  } else {
    missRate = 0.0;
  }
  printf("statistics:\n");
  printf("    accesses        = %d\n", cp->accesses);
  printf("    misses          = %d\n", cp->misses);
  printf("    miss rate       = %2.4f\n", missRate);
  printf("    hit rate        = %2.4f\n", 1.0 - missRate);
  printf("    replacements    = %d\n", cp->replacements);
  printf("    demand fetches  = %d cache lines\n", cp->demandFetches);
  printf("    copies back     = %d cache lines\n", cp->copiesBack);
}


/**************************************************************/

/* read an entry from the trace file */


#define TYPE_DATA_LOAD		0
#define TYPE_DATA_STORE		1
#define TYPE_INST_FETCH		2


typedef struct {
  int type;
  unsigned int addr;
} TraceEntry;


bool getTraceEntry(FILE *traceFile, TraceEntry *traceEntry) {
  char line[100];
  char *p1, *p2;
  char *endptr;

  if (fgets(line, 100, traceFile) == NULL) {
    return FALSE;
  }
  p1 = strtok(line, " \t\n");
  if (p1 == NULL) {
    error("cannot read trace entry type");
  }
  p2 = strtok(NULL, " \t\n");
  if (p2 == NULL) {
    error("cannot read trace entry address");
  }
  traceEntry->type = strtoul(p1, &endptr, 10);
  if (*endptr != '\0') {
    error("illegal trace entry type");
  }
  traceEntry->addr = strtoul(p2, &endptr, 16);
  if (*endptr != '\0') {
    error("illegal trace entry address");
  }
  return TRUE;
}


/**************************************************************/

/* main program */


/*
 * The cache geometry is defined by the triple (S, L, A).
 * S is the total cache size in bytes, L is the line size
 * in bytes, and A is the associativity of the cache.
 * The WB parameter defines the cache's write policy:
 * FALSE means "write-through", TRUE means "write-back".
 */

#define DEFAULT_S	4096
#define DEFAULT_L	8
#define DEFAULT_A	1
#define DEFAULT_WB	FALSE


void usage(char *myself) {
  printf("usage: %s [<options>] <trace file>\n", myself);
  printf("    <options> is some combination of:\n");
  printf("        -s <s>    total size of cache in bytes\n");
  printf("        -l <l>    line size in bytes\n");
  printf("        -a <a>    associativity\n");
  printf("        -w[t|b]   write-through or write-back\n");
  printf("        -d        enable debugging output\n");
  exit(1);
}


int main(int argc, char *argv[]) {
  int s;
  int l;
  int a;
  bool wb;
  char *traceFileName;
  int i;
  char *endptr;
  FILE *traceFile;
  TraceEntry traceEntry;
  Cache icache;
  Cache *cache;
  unsigned int addr;
  int mode;

  s = DEFAULT_S;
  l = DEFAULT_L;
  a = DEFAULT_A;
  wb = DEFAULT_WB;
  debug = FALSE;
  traceFileName = NULL;
  for (i = 1; i < argc; i++) {
    if (*argv[i] == '-') {
      if (strcmp(argv[i], "-s") == 0) {
        if (i == argc - 1) {
          error("total size is missing");
        }
        s = strtoul(argv[++i], &endptr, 0);
      } else
      if (strcmp(argv[i], "-l") == 0) {
        if (i == argc - 1) {
          error("line size is missing");
        }
        l = strtoul(argv[++i], &endptr, 0);
      } else
      if (strcmp(argv[i], "-a") == 0) {
        if (i == argc - 1) {
          error("associativity is missing");
        }
        a = strtoul(argv[++i], &endptr, 0);
      } else
      if (strcmp(argv[i], "-wt") == 0) {
        wb = FALSE;
      } else
      if (strcmp(argv[i], "-wb") == 0) {
        wb = TRUE;
      } else
      if (strcmp(argv[i], "-d") == 0) {
        debug = TRUE;
      } else {
        usage(argv[0]);
      }
    } else {
      if (traceFileName != NULL) {
        usage(argv[0]);
      }
      traceFileName = argv[i];
    }
  }
  if (traceFileName == NULL) {
    usage(argv[0]);
  }
  traceFile = fopen(traceFileName, "r");
  if (traceFile == NULL) {
    error("cannot open trace file '%s'", traceFileName);
  }
  printf("\n");
  printf("Instruction Cache\n");
  printf("-----------------\n");
  cacheInit(&icache, s, l, a, wb);
  while (getTraceEntry(traceFile, &traceEntry)) {
    cache = (traceEntry.type == TYPE_INST_FETCH) ? &icache : &icache;
    addr = traceEntry.addr;
    mode = (traceEntry.type == TYPE_DATA_STORE) ? CACHE_WRITE : CACHE_READ;
    if (debug) {
      printf("DEBUG: access(%s, 0x%08X, %s)\n",
             cache == &icache ? "icache" : "dcache",
             addr,
             mode == CACHE_READ ? "read" : "write");
    }
    cacheAccess(cache, addr, mode);
  }
  cacheFlush(&icache);
  printf("\n");
  printf("Instruction Cache\n");
  printf("-----------------\n");
  cacheShow(&icache);
  fclose(traceFile);
  return 0;
}
