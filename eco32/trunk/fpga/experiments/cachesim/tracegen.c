/*
 * tracegen.c -- trace generator
 */


#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define K		1024
#define NUM_ACCESSES	(64 * K)


unsigned int addr;
int count;


void next(void) {
  printf("2 %08x\n", addr << 2);
  addr++;
  count++;
  if (count == NUM_ACCESSES) {
    exit(0);
  }
}


void advance(int steps) {
  int i;

  for (i = 0; i < steps; i++) {
    next();
  }
}


void loop(int loopCount, int loopSize) {
  unsigned int loopEntry;
  int i;

  loopEntry = addr;
  for (i = 0; i < loopCount; i++) {
    addr = loopEntry;
    advance(loopSize);
  }
}


#define LINEAR		500
#define LOOP_COUNT	20
#define LOOP_SIZE	500


int main(int argc, char *argv[]) {
  count = 0;
  while (1) {
    addr = 0;
    advance(LINEAR);
    loop(LOOP_COUNT, LOOP_SIZE);
    advance(LINEAR);
    loop(LOOP_COUNT, LOOP_SIZE);
    advance(LINEAR);
  }
  return 0;
}
