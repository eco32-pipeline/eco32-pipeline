//
// addrsrc.v -- address source
//


`timescale 1ns/10ps
`default_nettype none


module addrsrc(clk, rst, stall, addr);
  input clk;
  input rst;
  input stall;
  output [24:0] addr;

  reg [3:0] page;
  reg [11:0] offset;

  always @(posedge clk) begin
    if (rst) begin
      page[3:0] <= 4'h0;
      offset[11:0] <= 12'h000;
    end else begin
      if (~stall) begin
        page[3:0] <= page[3:0] + 1;
        offset[11:0] <= offset[11:0] + 4;
      end
    end
  end

  assign addr[24:0] = 25'h0030000 + (page << 12) + offset;

endmodule
