//
// cache.v -- cache implementation
//


`timescale 1ns/10ps
`default_nettype none


module cache(clk, rst,
             addr, data, stall,
             mem_stb, mem_addr, mem_dout, mem_ack);
    input clk;
    input rst;
    // pipeline interface
    input [24:0] addr;
    output [31:0] data;
    output stall;
    // memory interface
    output mem_stb;
    output [24:2] mem_addr;
    input [31:0] mem_dout;
    input mem_ack;

  wire [24:0] st1_addr;
  wire [12:0] st1_tag;
  wire [9:0] st1_index;
  reg [24:0] st2_addr;
  wire [12:0] st2_tag;
  wire [9:0] st2_index;

  wire [9:0] tag_index;
  reg [13:0] tag_mem[0:1023];
  reg [13:0] tag_out;
  wire miss;

  wire [9:0] mem_index;
  reg [31:0] data_mem[0:1023];
  reg [31:0] data_out;

  //
  // 2-stage address pipeline
  //

  assign st1_addr[24:0] = addr[24:0];
  assign st1_tag[12:0] = st1_addr[24:12];
  assign st1_index[9:0] = st1_addr[11:2];

  always @(posedge clk) begin
    if (~stall) begin
      st2_addr[24:0] <= st1_addr[24:0];
    end
  end
  assign st2_tag[12:0] = st2_addr[24:12];
  assign st2_index[9:0] = st2_addr[11:2];

  //
  // tag memory access and cache miss detection
  // note: MSB of tag_mem used as 'valid' bit
  //

  assign tag_index[9:0] = ~mem_ack ? st1_index[9:0] : st2_index[9:0];

  always @(posedge clk) begin
    if (mem_ack) begin
      tag_mem[tag_index] <= { 1'b1, st2_tag[12:0] };
      tag_out <= { 1'b1, st2_tag[12:0] };
    end else begin
      tag_out <= tag_mem[tag_index];
    end
  end

  assign miss = ~tag_out[13] | (tag_out[12:0] != st2_tag[12:0]);

  //
  // data memory access
  //

  assign mem_index[9:0] = st2_index[9:0];

  always @(posedge clk) begin
    if (mem_ack) begin
      data_mem[mem_index] <= mem_dout;
      data_out <= mem_dout;
    end else begin
      data_out <= data_mem[mem_index];
    end
  end

  assign data[31:0] = data_out[31:0];

  //
  // main memory access
  //

  assign mem_stb = ~rst & miss;
  assign mem_addr[24:2] = st2_addr[24:2];

  assign stall = mem_stb;

  integer i;
  initial begin
    // all entries valid, all tags matching
    // data easily correlated with address
    for (i = 0; i < 1024; i = i + 1) begin
      tag_mem[i] = { 1'b1, 13'h0030 } + (i % 16);
      data_mem[i] = 32'h123AA000 + (i << 2);
    end
    // some tags are wrong and produce misses
    tag_mem [10'h008] = { 1'b1, 13'h0040 };
    data_mem[10'h008] = 32'hDEADBEEF;
    tag_mem [10'h010] = { 1'b1, 13'h0040 };
    data_mem[10'h010] = 32'hDEADBEEF;
    tag_mem [10'h012] = { 1'b1, 13'h0040 };
    data_mem[10'h012] = 32'hDEADBEEF;
    tag_mem [10'h018] = { 1'b1, 13'h0040 };
    data_mem[10'h018] = 32'hDEADBEEF;
    tag_mem [10'h019] = { 1'b1, 13'h0040 };
    data_mem[10'h019] = 32'hDEADBEEF;
    // some entries are invalid and produce misses
    tag_mem [10'h028] = { 1'b0, 13'h0038 };
    data_mem[10'h028] = 32'hDEADBEEF;
    tag_mem [10'h030] = { 1'b0, 13'h0030 };
    data_mem[10'h030] = 32'hDEADBEEF;
    tag_mem [10'h032] = { 1'b0, 13'h0032 };
    data_mem[10'h032] = 32'hDEADBEEF;
    tag_mem [10'h038] = { 1'b0, 13'h0038 };
    data_mem[10'h038] = 32'hDEADBEEF;
    tag_mem [10'h039] = { 1'b0, 13'h0039 };
    data_mem[10'h039] = 32'hDEADBEEF;
  end

endmodule
