//
// memory.v -- simulate external memory
//             8M x 32 bit = 32 MB
//


`timescale 1ns/10ps
`default_nettype none


`define RD_CYCLES	4'h2		/* # cycles for read, min = 2 */


module memory(clk, rst,
              stb, we, addr,
              data_in, data_out, ack);
    input clk;
    input rst;
    input stb;
    input we;
    input [24:2] addr;
    input [31:0] data_in;
    output reg [31:0] data_out;
    output ack;

  reg [31:0] mem[0:8388607];
  reg [3:0] rd_counter;
  wire rd_done;

  always @(posedge clk) begin
    if (stb) begin
      if (we) begin
        // write cycle
        mem[addr] <= data_in;
      end else begin
        // read cycle
        data_out <= mem[addr];
      end
    end
  end

  always @(posedge clk) begin
    if (rst) begin
      rd_counter[3:0] <= 4'h0;
    end else begin
      if (rd_counter[3:0] == 4'h0) begin
        if (stb & ~we) begin
          // a read needs some clock cycles
          rd_counter[3:0] <= `RD_CYCLES - 1;
        end
      end else begin
        rd_counter[3:0] <= rd_counter[3:0] - 1;
      end
    end
  end

  assign rd_done = (rd_counter[3:0] == 4'h1) ? 1 : 0;
  assign ack = (stb & we) | rd_done;

  initial begin
    mem[23'h00E008] = 32'h123AA020;
    mem[23'h00C010] = 32'h123AA040;
    mem[23'h00C812] = 32'h123AA048;
    mem[23'h00E018] = 32'h123AA060;
    mem[23'h00E419] = 32'h123AA064;
    mem[23'h00E028] = 32'h123AA0A0;
    mem[23'h00C030] = 32'h123AA0C0;
    mem[23'h00C832] = 32'h123AA0C8;
    mem[23'h00E038] = 32'h123AA0E0;
    mem[23'h00E439] = 32'h123AA0E4;
  end

endmodule
